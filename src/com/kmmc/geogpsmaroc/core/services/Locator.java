package com.kmmc.geogpsmaroc.core.services;

import com.kmmc.geogpsmaroc.framework.kernel.Kernel;

public class Locator {

    public static Object lookup(Class clazz) {
	return Kernel.getInstance().getImplementation(clazz);
    }

}
