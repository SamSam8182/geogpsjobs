package com.kmmc.geogpsmaroc.core.services.mail;

import java.io.UnsupportedEncodingException;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.kmmc.geogpsmaroc.core.services.log.Logger;


public class MailServiceThread extends Thread {
    private String subject;
    private String mailTo;
    private String content;
    private String mailUser;
    private String mailDisplayName;
    private String host;
    private String port;
    private String mailPassword;
    
    private Session session;

    public MailServiceThread(String subject, String mailUser, String mailTo, String content, Session session, String mailDisplayName, String host, String port, String mailPassword) {
        super();
        this.subject = subject;
        this.mailTo = mailTo;
        this.content = content;
        this.mailUser = mailUser;
        this.mailDisplayName = mailDisplayName;
        this.host = host;
        this.port = port;
        this.mailPassword = mailPassword;
        
        this.session = session;
    }

    public void run() {
    	 try {
    		 if(this.mailTo!=null && !this.mailTo.isEmpty()){
	             Message message = new MimeMessage(session);
	             message.setFrom(new InternetAddress(this.mailUser,this.mailDisplayName));
	             message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(this.mailTo));
	             message.setSubject(this.subject);
	             message.setText(this.content);

	             Transport transport = session.getTransport("smtp");
	             transport.connect(host,Integer.parseInt(port),mailUser,mailPassword);
	             //Send Mail won't work if we don't disable the antivirus
	             transport.sendMessage(message,message.getAllRecipients());
	             transport.close();
	             
	             Logger.info("Mail Sended To "+this.mailTo);
    		 }
         } catch (MessagingException e) {
             Logger.error("ERROR SENDING MAIL", e);
         } catch (UnsupportedEncodingException e) {
             Logger.error("ERROR SENDING MAIL", e);
		 }
    }
}
