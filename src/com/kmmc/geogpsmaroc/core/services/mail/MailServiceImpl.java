package com.kmmc.geogpsmaroc.core.services.mail;

import java.util.Properties;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import com.kmmc.geogpsmaroc.core.services.configuration.ConfigurationService;
import com.kmmc.geogpsmaroc.framework.kernel.Kernel;
import com.sun.mail.util.MailSSLSocketFactory;

/**
 * Impl�mentation du service d'envoi de la messagerie �l�ctronique.
 * @author EBE
 *
 */
public class MailServiceImpl extends Thread implements MailService {
    private String mailUser;
    private Session session;
    private String mailDisplayName;
    private String mailPort;
    private String mailHost;

    private String username, password;

    public void init() throws Exception {
        ConfigurationService config = (ConfigurationService) Kernel.getInstance().getImplementation(ConfigurationService.class);
        
        this.mailUser = config.getMailUser();
        this.username = config.getMailUser();
        this.password = config.getMailPassword();
        this.mailDisplayName = config.getMailUserDisplayName();
        this.mailHost = config.getMailServer();
        this.mailPort = config.getMailPort();
            
		Properties props = new Properties();
		props.put("mail.smtp.host", mailHost); 
		props.put("mail.smtp.port", mailPort); 
		//props.put("mail.debug", "true"); 
		props.put("mail.smtp.auth", "true"); 
		props.put("mail.smtp.starttls.enable","true"); 
		props.put("mail.smtp.ssl.enable", "true");
		props.put("mail.smtp.ssl.checkserveridentity","false");
		props.put("mail.smtp.ssl.trust", "*");
		
		this.session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
		        protected PasswordAuthentication getPasswordAuthentication() {
		                return new PasswordAuthentication(username, password);
		        }
			});
    }

    public void send(String subject, String mailTo, String content) {
    	//Logger.debug("Sending mail..." + content);
        
    	try {
        	new MailServiceThread(subject, mailUser, mailTo, content, this.session, this.mailDisplayName, this.mailHost, this.mailPort, this.password).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
