package com.kmmc.geogpsmaroc.core.services.mail;

import com.kmmc.geogpsmaroc.core.services.Service;

/**
 * Interface de gestion du service de messagerie electronique.
 * @author Administrateur
 *
 */
public interface MailService extends Service {

    public abstract void send(String subject, String mailTo, String content);

}