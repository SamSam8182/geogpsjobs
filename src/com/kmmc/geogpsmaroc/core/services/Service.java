/**
 * 
 */
package com.kmmc.geogpsmaroc.core.services;

/**
 * @author h.benmessaoud
 * 
 */
public interface Service {

    public void init() throws Exception;

}
