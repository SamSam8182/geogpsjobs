package com.kmmc.geogpsmaroc.core.services.messages;

import java.util.Locale;

import com.kmmc.geogpsmaroc.framework.kernel.Kernel;


public class MessagesServiceImpl implements MessagesService {
    public void init() throws Exception {
    }

    /*
         * (non-Javadoc)
         *
         * @see com.kmmc.geogpsmaroc.core.services.messages.MessagesService#getMessage(java.lang.String,
         *      java.lang.String[])
         */
    public synchronized String getMessage(String key, String[] params) {
        return Kernel.getInstance().getMessage(key, params, Locale.getDefault());
    }

    public String getMessage(String key) {
        return getMessage(key, null);
    }
    
    public String getMessageLocale(String key, String[] params, String locale) {
        return Kernel.getInstance().getMessage(key, params, new Locale(locale));
    }


    
    public synchronized String getMessageLocale(String key, String locale) {
        return getMessageLocale(key, null, locale);
    }
}
