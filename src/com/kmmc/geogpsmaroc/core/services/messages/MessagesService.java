package com.kmmc.geogpsmaroc.core.services.messages;

import com.kmmc.geogpsmaroc.core.services.Service;


public interface MessagesService extends Service {
    
    public abstract String getMessage(String key, String[] params);

    public abstract String getMessage(String key);
    
    public abstract String getMessageLocale(String key, String[] params, String locale);
    
    public abstract String getMessageLocale(String key, String locale);
    
}
