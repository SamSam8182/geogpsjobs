package com.kmmc.geogpsmaroc.core.services.configuration;

public interface XmlKeys {

	// Log
	public static String LOG_CONFIG_FILE = "log.logConfigFile";

	// Validation
	public static String VALIDATION_FILE = "validation.validationFile";
	public static String MESSAGES_FILE = "validation.messagesFile";

	// Mail
	public static String MAIL_PROTOCOL = "mail.mailProtocol";
	public static String MAIL_SERVER = "mail.mailServer";
	public static String MAIL_USER = "mail.mailUser";
	public static String MAIL_PASSWORD = "mail.mailPassword";
	public static String MAIL_PORT = "mail.mailPort";
	public static String MAIL_DISPLAY = "mail.mailDisplay";

	// Print
	public static String PRINT_TEMPLATES_FOLDER = "print.printTemplatesFolder";
	public static String PRINT_OUTPUT_FOLDER = "print.printOutputFolder";

	// Planification
	public static String JOBS_CONFIG_FILE = "planification.jobsConfigFile";

	// DataSource name
	public static String DATASOURCE_NAME = "database.dataSourceName";

	// Edi Shema folder
	public static String EDI_SCHEMA_PATH = "edi.schemaPath";
	// Edi Template folder
	public static String EDI_TEMPLATE_PATH = "edi.templatePath";

	// BeanMapper (Dozer) configuratio file path
	public static String BEAN_MAPPER_CONFIG_FILE = "beanMapper.configFilePath";

	
	// Proxy properties
	public static String PROXY_ADRESSE = "proxyProperties.proxyAdresse";
	public static String PROXY_PORT = "proxyProperties.proxyPort";

	
}
