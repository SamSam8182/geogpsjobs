package com.kmmc.geogpsmaroc.core.services.configuration;

import com.kmmc.geogpsmaroc.core.services.Service;

public interface ConfigurationService extends Service {

	public abstract String getLogConfigFile();

	public abstract String getValidationFile();

	public abstract String getMessagesFile();

	public abstract String getMailProtocol();

	public abstract String getMailServer();

	public abstract String getMailUser();

	public abstract String getMailPassword();

	public abstract String getMailPort();

	public abstract String getJobsConfigFile();

	public abstract String getBeanMapperConfigFilePath();

	public abstract String getProxyAdresse();

	public abstract String getProxyPort();

	public abstract String getMailUserDisplayName();
	
	
}
