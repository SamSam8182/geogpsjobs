package com.kmmc.geogpsmaroc.core.services.configuration;

import org.apache.commons.beanutils.DynaBean;

import com.kmmc.geogpsmaroc.framework.configuration.ConfigBeanLoader;

/**
 * Classe d'implementation du service d'initialisation de la configuration globale de l'application.
 * 
 * @author EBE
 * 
 */
public class ConfigurationServiceImpl implements XmlKeys, ConfigurationService {
	private static DynaBean configBean;
	private static String CONFIG_FILE_PATH = "config/global-config.xml";
	private static String BASE_PATH;

	public void init() throws Exception {
		ConfigBeanLoader config = new ConfigBeanLoader(CONFIG_FILE_PATH);
		configBean = config.getBean();

		String basePath = config.getBasePath();
		BASE_PATH = basePath.substring(0, basePath.length() - 17);
		System.out.println("BASE_PATH = [" + BASE_PATH + "]");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kmmc.geogpsmaroc.core.services.configuration.ConfigurationService#getLogFile()
	 */
	public String getLogConfigFile() {
		return BASE_PATH + (String) configBean.get(LOG_CONFIG_FILE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kmmc.geogpsmaroc.core.services.configuration.ConfigurationService#getValidationFile()
	 */
	public String getValidationFile() {
		return BASE_PATH + (String) configBean.get(VALIDATION_FILE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kmmc.geogpsmaroc.core.services.configuration.ConfigurationService#getMessagesFile()
	 */
	public String getMessagesFile() {
		return (String) configBean.get(MESSAGES_FILE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kmmc.geogpsmaroc.core.services.configuration.ConfigurationService#getMailProtocol()
	 */
	public String getMailProtocol() {
		return (String) configBean.get(MAIL_PROTOCOL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kmmc.geogpsmaroc.core.services.configuration.ConfigurationService#getMailServer()
	 */
	public String getMailServer() {
		return (String) configBean.get(MAIL_SERVER);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kmmc.geogpsmaroc.core.services.configuration.ConfigurationService#getMailUser()
	 */
	public String getMailUser() {
		return (String) configBean.get(MAIL_USER);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kmmc.geogpsmaroc.core.services.configuration.ConfigurationService#getMailPassword()
	 */
	public String getMailPassword() {
		return (String) configBean.get(MAIL_PASSWORD);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kmmc.geogpsmaroc.core.services.configuration.ConfigurationService#getMailPort()
	 */
	public String getMailPort() {
		return (String) configBean.get(MAIL_PORT);
	}

	public String getJobsConfigFile() {
		return BASE_PATH + (String) configBean.get(JOBS_CONFIG_FILE);
	}

	/*
	 * M�thode pour la recuperation de l'adresse du proxy
	 */
	public String getProxyAdresse() {
		return (String) configBean.get(PROXY_ADRESSE);
	}

	/*
	 * M�thode pour la recuperation du port du proxy
	 */
	public String getProxyPort() {
		return (String) configBean.get(PROXY_PORT);
	}
	public String getBeanMapperConfigFilePath() {
		return (String) configBean.get(BEAN_MAPPER_CONFIG_FILE);
	}
	
	/*
	 * Méthode pour la récupération du nom de l'émetteur
	 */
	public String getMailUserDisplayName(){
		return (String) configBean.get(MAIL_DISPLAY);
	}
}
