package com.kmmc.geogpsmaroc.core.services.persistance;

import com.kmmc.geogpsmaroc.core.services.Service;
import com.kmmc.geogpsmaroc.framework.persistance.dao.DAO;

public interface PersistanceService extends Service{

    public abstract DAO getDAO() throws Exception;

}