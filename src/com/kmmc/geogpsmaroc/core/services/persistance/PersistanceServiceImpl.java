package com.kmmc.geogpsmaroc.core.services.persistance;

import com.kmmc.geogpsmaroc.framework.kernel.Kernel;
import com.kmmc.geogpsmaroc.framework.persistance.dao.DAO;


public class PersistanceServiceImpl implements PersistanceService {
    /**
         * Initializes JPA using the given datasource.
         *
         * @throws Exception
         */
    public void init() throws Exception {
    }

    /*
         * (non-Javadoc)
         *
         * @see com.kmmc.geogpsmaroc.core.services.persistance.PersistanceService#getDAO()
         */
    public DAO getDAO() throws Exception {
        return (DAO) Kernel.getInstance().getImplementation(DAO.class);
    }
}
