package com.kmmc.geogpsmaroc.core.services.Planification;

import com.kmmc.geogpsmaroc.core.services.configuration.ConfigurationService;
import com.kmmc.geogpsmaroc.framework.kernel.Kernel;
import com.kmmc.geogpsmaroc.framework.planification.TaskScheduler;

/**
 * Implementation du service de gestions des batchs
 * @author Administrateur
 *
 */
public class PlanificationServiceImpl implements PlanificationService {
    private String configFile;

    /**
     * Methode d'initialisation du service de planification 
     */
    public void init() throws Exception {
        ConfigurationService config = (ConfigurationService) Kernel.getInstance().getImplementation(ConfigurationService.class);
        configFile = config.getJobsConfigFile();
        TaskScheduler scheduler = new TaskScheduler();
        scheduler.start(configFile);
    }
}
