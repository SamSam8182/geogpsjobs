package com.kmmc.geogpsmaroc.core.services.log;

import com.kmmc.geogpsmaroc.core.services.Locator;

/**
 * Classe statique d'acc�s au service de journalisation.
 * @author EBE
 *
 */
public class Logger {
    public static LogService logService;

     static {	
	logService = (LogService) Locator.lookup(LogService.class);
    }
    
    public static void debug(String msg){
	logService.debug(msg);
    }
    
    public static void info(String msg){
	logService.info(msg);
    }
    
    public static void fatal(String msg, Throwable e){
	logService.fatal(msg, e);	
    }
    
    public static void error(String msg, Throwable e){
	logService.error(msg, e);	
    }    

}
