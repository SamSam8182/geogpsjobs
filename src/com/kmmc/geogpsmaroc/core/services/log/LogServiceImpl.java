package com.kmmc.geogpsmaroc.core.services.log;

import com.kmmc.geogpsmaroc.core.services.configuration.ConfigurationService;
import com.kmmc.geogpsmaroc.framework.kernel.Kernel;
import com.kmmc.geogpsmaroc.framework.log.LogManager;
import com.kmmc.geogpsmaroc.framework.log.impls.Log4jLogger;


/**
 * Impl�mentation de gestion des fichiers de journalisation.
 * @author EBE
 *
 */
public class LogServiceImpl implements LogService {
    private static String LOGGER_NAME = "com.kmmc.geogpsmaroc";
    private LogManager logger;

    public void init() throws Exception {
        ConfigurationService config = (ConfigurationService) Kernel.getInstance().getImplementation(ConfigurationService.class);
        logger = new Log4jLogger(LOGGER_NAME, config.getLogConfigFile());
    }

    /* (non-Javadoc)
     * @see com.kmmc.geogpsmaroc.core.services.log.LogService#info(java.lang.String)
     */
    public void info(String msg) {
        logger.info(msg);
    }

    /* (non-Javadoc)
     * @see com.kmmc.geogpsmaroc.core.services.log.LogService#debug(java.lang.String)
     */
    public void debug(String msg) {
        logger.debug(msg);
    }

    /* (non-Javadoc)
     * @see com.kmmc.geogpsmaroc.core.services.log.LogService#error(java.lang.String, java.lang.Throwable)
     */
    public void error(String msg, Throwable e) {
        logger.error(msg, e);
    }

    /* (non-Javadoc)
     * @see com.kmmc.geogpsmaroc.core.services.log.LogService#fatal(java.lang.String, java.lang.Throwable)
     */
    public void fatal(String msg, Throwable e) {
        logger.fatal(msg, e);
    }
}
