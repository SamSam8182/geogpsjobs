package com.kmmc.geogpsmaroc.core.services.log;

import com.kmmc.geogpsmaroc.core.services.Service;

/**
 * Interface principale du service de journalisation.
 * @author EBE
 *
 */
public interface LogService  extends Service{

    public abstract void info(String msg);

    public abstract void debug(String msg);

    public abstract void error(String msg, Throwable e);

    public abstract void fatal(String msg, Throwable e);

}