package com.kmmc.geogpsmaroc.core.services.mapper;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

/**
 * <B>La copie d�objets Java simplifi�e et automatique :</B>
 * <p>
 * cette classe permet de copier r�cursivement les donn�es d�un objet vers un autre, g�n�ralement
 * des objets de type diff�rents, de mani�re automatique en utilisant la r�flexion Java et/ou de
 * mani�re personnalis�e avec un fichier de configuration XML.
 * </p>
 * <ul>
 * <li>Nom du Projet : GeoGpsJobs</li>
 * <li>Fichier : DozerMapper.java </li>
 * <li>Date : 15 nov. 10 </li>
 * </ul>
 * 
 * @author NAKMOUCH Achraf
 */
public final class BeanMapper extends DozerBeanMapper {

    private static Mapper instance;

    private static String MAPPER_CONFIG_FILE_PATH = "mapper/beanMapping.xml";

    private BeanMapper() {
    }

    /**
     * Cette m�thode retourne le 'Mapper Dozzer' en Singleton
     * 
     * @return Mapper
     * @author NAKMOUCH Achraf
     */
    @SuppressWarnings("unchecked")
    public static synchronized Mapper getInstance() {
        if (instance == null) {
            instance = new DozerBeanMapper(getConfigFile());
        }
        return instance;
    }

    /**
     * Cette m�thode retourne la liste des fichiers de mapping qu'utilise dozer
     * 
     * @return list
     * @author NAKMOUCH Achraf
     */
    @SuppressWarnings("unchecked")
    private static List getConfigFile() {
        List list = new ArrayList();
        list.add(MAPPER_CONFIG_FILE_PATH);
        return list;
    }

    /**
     * Cette m�thode permet de mapper une liste d'objets source dans une autre liste d'objets
     * destination
     * 
     * @param <T>
     * @param src
     * @param desc
     * @param clazz
     * @author NAKMOUCH Achraf
     */
    public static <T> void copyPropertiesList(List src, List<T> desc, Class<T> clazz) {
        for (Object obj : src) {
            T t = (T) BeanMapper.getInstance().map(obj, clazz);
            desc.add(t);
        }
    }
}
