package com.kmmc.geogpsmaroc.core.services.validation;

import java.util.Vector;

import com.kmmc.geogpsmaroc.core.services.configuration.ConfigurationService;
import com.kmmc.geogpsmaroc.framework.kernel.Kernel;
import com.kmmc.geogpsmaroc.framework.validation.SmartValidator;

/**
 * Impl�mentation du service de validation.
 * 
 * @author EBE
 */
public class ValidationServiceImpl implements ValidationService {
    private SmartValidator validator;

    public void init() throws Exception {
        ConfigurationService config = (ConfigurationService) Kernel.getInstance().getImplementation(ConfigurationService.class);
        validator = new SmartValidator(config.getValidationFile(), config.getMessagesFile());
    }

    /*
     * (non-Javadoc)
     * @see com.kmmc.geogpsmaroc.core.services.validation.ValidationService#execute(java.lang.Object)
     */
    public synchronized Vector<String> execute(Object bean) throws Exception {
        validator.applyToBean(bean);

        return validator.validate();
    }

    public synchronized Vector<String> execute(Object bean, String langue) throws Exception {
        validator.applyToBean(bean);

        return validator.validate(langue);
    }
}
