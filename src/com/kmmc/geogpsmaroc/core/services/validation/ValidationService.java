package com.kmmc.geogpsmaroc.core.services.validation;

import java.util.Vector;

import com.kmmc.geogpsmaroc.core.services.Service;


public interface ValidationService extends Service {

    public abstract Vector<String> execute(Object bean) throws Exception;

    public abstract Vector<String> execute(Object bean, String langue) throws Exception;

}