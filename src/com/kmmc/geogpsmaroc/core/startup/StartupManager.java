package com.kmmc.geogpsmaroc.core.startup;

import java.lang.reflect.Method;

import com.kmmc.geogpsmaroc.framework.configuration.ConfigLoader;
import com.kmmc.geogpsmaroc.framework.kernel.Kernel;
import com.kmmc.geogpsmaroc.framework.reflect.ReflectUtils;


/**
 * module g�n�rique de chargement de class de
 * startup depuis un fichier de config
 * @author h.benmessaoud
 *
 */
public class StartupManager {
    
    // Fichier de configuration de l'application
    private static String CONFIG_FILE_PATH = "config/startup-config.xml";
    // Nom de la m�thode d'initialisation de chaque service 
    private static String INIT_METHOD_NAME = "init";
    //Noeud coorespondant � l'entit� class du fichier de config
    private static String STARTUP_CLASS = "startup.class";
    //Noeud correspodant au nom complet du service dans le fichier de config
    private static String FULL_NAME = "fullname";

    public static void lunch() throws Exception {
        ConfigLoader config = new ConfigLoader(CONFIG_FILE_PATH);
        System.out.println("[StartupManager]  Starting services...");

        /*
         * Parcours de la liste des services � initialiser et appel � la methode "INIT_METHOD_NAME" par r�fl�xion 
         */
        for (int i = 0; i < config.getChildsCount(STARTUP_CLASS); i++) {
            String className = config.getValue(STARTUP_CLASS + "(" + i + ")." + FULL_NAME);
            System.out.println("Initializing Service class [" + className + "]");
            Method initMethod = ReflectUtils.getMethod(Class.forName(className), INIT_METHOD_NAME);
            // Obtention aupr�s de Spring de l'impl�mentation corr�spondant au service
            Object instance = Kernel.getInstance().getImplementation(Class.forName(className));
            //Invocation de la methode d'initialisation
            initMethod.invoke(instance, new Object[] {  });
            System.out.println("Service class [" + className + "] initialized");
        }

        System.out.println("[StartupManager] Services started");
    }
}
