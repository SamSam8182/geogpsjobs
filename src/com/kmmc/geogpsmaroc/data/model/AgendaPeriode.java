package com.kmmc.geogpsmaroc.data.model;

import java.sql.Time;

//Table agenda_periode
public class AgendaPeriode extends PersistentObject {
	
	private static final long serialVersionUID = 1L;
	
	// DATA
	private Integer jour;
	private Time heureDu;
	private Time heureAu;
	private Long idAgendaTravail;
	
	//BUSINESS
	private Long idHistoric;

	public Integer getJour() {
		return jour;
	}

	public void setJour(Integer jour) {
		this.jour = jour;
	}

	public Long getIdAgendaTravail() {
		return idAgendaTravail;
	}

	public void setIdAgendaTravail(Long idAgendaTravail) {
		this.idAgendaTravail = idAgendaTravail;
	}

	public Time getHeureDu() {
		return heureDu;
	}

	public void setHeureDu(Time heureDu) {
		this.heureDu = heureDu;
	}

	public Time getHeureAu() {
		return heureAu;
	}

	public void setHeureAu(Time heureAu) {
		this.heureAu = heureAu;
	}

	public Long getIdHistoric() {
		return idHistoric;
	}

	public void setIdHistoric(Long idHistoric) {
		this.idHistoric = idHistoric;
	}

}