package com.kmmc.geogpsmaroc.data.model;

//Table Alerte
public class ZoneAlert extends BaseAlert {

	private static final long serialVersionUID = 1L;

	public ZoneAlert(Long idHistoric) {
        this.idHistorique = idHistoric;
        super.setInprogressAlert(false);
    }

	public ZoneAlert() {
	}
}