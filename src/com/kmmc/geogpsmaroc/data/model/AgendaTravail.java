package com.kmmc.geogpsmaroc.data.model;

//Table agenda_travail
public class AgendaTravail extends PersistentObject {
	
	private static final long serialVersionUID = 1L;
	
	// DATA
	private String nom;
	private Long idTempsAlerte;
	private Long idCompte;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Long getIdTempsAlerte() {
		return idTempsAlerte;
	}

	public void setIdTempsAlerte(Long idTempsAlerte) {
		this.idTempsAlerte = idTempsAlerte;
	}

	public Long getIdCompte() {
		return idCompte;
	}

	public void setIdCompte(Long idCompte) {
		this.idCompte = idCompte;
	}

}