package com.kmmc.geogpsmaroc.data.model;

//Table Alerte
public class AgendaAlert extends BaseAlert {
	
	private static final long serialVersionUID = 1L;
	
	
    public AgendaAlert() {
	}
    public AgendaAlert(Long idHistoric) {
    	this.idHistorique = idHistoric;
    }
    

	public int getLiterConsumed() {
		return carburant;
	}

	public void setLiterConsumed(int literConsumed) {
		this.carburant = literConsumed;
	}

}