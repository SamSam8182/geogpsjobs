package com.kmmc.geogpsmaroc.data.model;

import java.util.Date;

//Table Alerte
public class BaseAlert extends PersistentObject {
		
	private static final long serialVersionUID = 1L;
	
	// DATA
	protected Long idVehicule;
	protected Long idChauffeur;
	protected Integer vitesseAlerte;
	protected Integer vitesseMax;
	protected Date datetimeFrom;
	protected Date datetimeTo;
	protected Double montantAmende ;
	protected Boolean payer ;
	protected Boolean traiter ;
	protected Long idPersonneTraite;
	protected Date tempsTraitement;
	protected String observation;
	protected String name ;
	protected String trancheHoraire;
	protected String remarque;
	protected Double distance;
	protected Integer carburant;
	protected String typeAlerte ;
	protected Long idHistorique ;
	protected Long idCompte  ;
	
	
	// BUSINESS
	protected boolean inprogressAlert;
	protected String mailSubject;
	protected String mailText;
	public Device device;
	protected Double counterFrom;
	protected Double counterTo;
	protected Double fuelFrom;
	
	public Date getDatetimeFrom() {
		return datetimeFrom;
	}
	public void setDatetimeFrom(Date datetimeFrom) {
		this.datetimeFrom = datetimeFrom;
	}
	public Date getDatetimeTo() {
		return datetimeTo;
	}
	public void setDatetimeTo(Date datetimeTo) {
		this.datetimeTo = datetimeTo;
	}
	public boolean isInprogressAlert() {
		return inprogressAlert;
	}
	public void setInprogressAlert(boolean inprogressAlert) {
		this.inprogressAlert = inprogressAlert;
	}
	public String getMailSubject() {
		return mailSubject;
	}
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	public String getMailText() {
		return mailText;
	}
	public void setMailText(String mailText) {
		this.mailText = mailText;
	}
	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}
	public Long getIdVehicule() {
		return idVehicule;
	}
	public void setIdVehicule(Long idVehicule) {
		this.idVehicule = idVehicule;
	}
	public Long getIdChauffeur() {
		return idChauffeur;
	}
	public void setIdChauffeur(Long idChauffeur) {
		this.idChauffeur = idChauffeur;
	}
	public Integer getVitesseAlerte() {
		return vitesseAlerte;
	}
	public void setVitesseAlerte(Integer vitesseAlerte) {
		this.vitesseAlerte = vitesseAlerte;
	}
	public Integer getVitesseMax() {
		return vitesseMax;
	}
	public void setVitesseMax(Integer vitesseMax) {
		this.vitesseMax = vitesseMax;
	}
	public Double getMontantAmende() {
		return montantAmende;
	}
	public void setMontantAmende(Double montantAmende) {
		this.montantAmende = montantAmende;
	}
	public Boolean getPayer() {
		return payer;
	}
	public void setPayer(Boolean payer) {
		this.payer = payer;
	}
	public Boolean getTraiter() {
		return traiter;
	}
	public void setTraiter(Boolean traiter) {
		this.traiter = traiter;
	}
	public Long getIdPersonneTraite() {
		return idPersonneTraite;
	}
	public void setIdPersonneTraite(Long idPersonneTraite) {
		this.idPersonneTraite = idPersonneTraite;
	}
	public Date getTempsTraitement() {
		return tempsTraitement;
	}
	public void setTempsTraitement(Date tempsTraitement) {
		this.tempsTraitement = tempsTraitement;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTrancheHoraire() {
		return trancheHoraire;
	}
	public void setTrancheHoraire(String trancheHoraire) {
		this.trancheHoraire = trancheHoraire;
	}
	public String getRemarque() {
		return remarque;
	}
	public void setRemarque(String remarque) {
		this.remarque = remarque;
	}
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double d) {
		this.distance = d;
	}
	public Integer getCarburant() {
		return carburant;
	}
	public void setCarburant(Integer carburant) {
		this.carburant = carburant;
	}
	public String getTypeAlerte() {
		return typeAlerte;
	}
	public void setTypeAlerte(String typeAlerte) {
		this.typeAlerte = typeAlerte;
	}
	public Long getIdHistorique() {
		return idHistorique;
	}
	public void setIdHistorique(Long idHistorique) {
		this.idHistorique = idHistorique;
	}
	public Long getIdCompte() {
		return idCompte;
	}
	public void setIdCompte(Long idCompte) {
		this.idCompte = idCompte;
	}
	public Double getCounterFrom() {
		return counterFrom;
	}
	public void setCounterFrom(Double counterFrom) {
		this.counterFrom = counterFrom;
	}
	public Double getCounterTo() {
		return counterTo;
	}
	public void setCounterTo(Double counterTo) {
		this.counterTo = counterTo;
	}
	public void setFuelFrom(Double fuelFrom){
		this.fuelFrom = fuelFrom;
	}
	public Double getFuelFrom(){
		return this.fuelFrom;
	}
}