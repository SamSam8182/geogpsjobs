package com.kmmc.geogpsmaroc.data.model;

//Table Alerte
public class SpeedAlert extends BaseAlert {
	
	private static final long serialVersionUID = 1L;
	
	private double maximalSpeed;

	public double getMaximalSpeed() {
		return maximalSpeed;
	}
	public void setMaximalSpeed(double maximalSpeed) {
		this.maximalSpeed = maximalSpeed;
	}
}