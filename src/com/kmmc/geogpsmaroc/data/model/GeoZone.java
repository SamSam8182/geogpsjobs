package com.kmmc.geogpsmaroc.data.model;

//Table geozone
public class GeoZone extends PersistentObject {

	private static final long serialVersionUID = 1L;
	
	// DATA
	private String nom;
	private String description;
	private String latitudes;//latitudes splited by comma
	private String longitudes;//longitudes splited by comma
	private Long idZoneAlerte;
	private String shapes;//JSON
	private Long idCompte;

	// BUSINESS
	private Account account;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLatitudes() {
		return latitudes;
	}

	public void setLatitudes(String latitudes) {
		this.latitudes = latitudes;
	}

	public String getLongitudes() {
		return longitudes;
	}

	public void setLongitudes(String longitudes) {
		this.longitudes = longitudes;
	}

	public Long getIdZoneAlerte() {
		return idZoneAlerte;
	}

	public void setIdZoneAlerte(Long idZoneAlerte) {
		this.idZoneAlerte = idZoneAlerte;
	}

	public String getShapes() {
		return shapes;
	}

	public void setShapes(String shapes) {
		this.shapes = shapes;
	}

	public Long getIdCompte() {
		return idCompte;
	}

	public void setIdCompte(Long idCompte) {
		this.idCompte = idCompte;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	
}