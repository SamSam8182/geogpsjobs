package com.kmmc.geogpsmaroc.data.model;

//Table groupes_vehicules
public class GroupesVehicules extends PersistentObject {
	
	private static final long serialVersionUID = 1L;
	
	// DATA
	private String nomGroupe;
	private String commentaire;
	private Long idCompte;

	public String getNomGroupe() {
		return nomGroupe;
	}

	public void setNomGroupe(String nomGroupe) {
		this.nomGroupe = nomGroupe;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Long getIdCompte() {
		return idCompte;
	}

	public void setIdCompte(Long idCompte) {
		this.idCompte = idCompte;
	}

}