package com.kmmc.geogpsmaroc.data.model;

/**
 * Classe représentant les objets persistants.
 */
public class PersistentObject implements java.io.Serializable {
    /**
     * Commentaire pour <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = 1L;
    
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
