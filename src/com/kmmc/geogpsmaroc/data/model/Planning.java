package com.kmmc.geogpsmaroc.data.model;

import java.util.Date;

import com.kmmc.geogpsmaroc.business.helpers.DateHelper;

//Table Historique_agenda_periode
public class Planning extends PersistentObject{
	
	private static final long serialVersionUID = 1L;
	
	private String day;
	private Date timeFrom;
	private Date timeTo;
	
	private int dayIdx;
	public POIElement poiElement;
	public Device device;
	
	public Planning(String day, int dayIdx, Date timeFrom, Date timeTo) {
		super();
		this.day = day;
		this.dayIdx = dayIdx;
		this.timeFrom = timeFrom;
		this.timeTo = timeTo;
	}
	public Planning(String day, int dayIdx, int timeFrom, int timeTo) {
		super();
		this.day = day;
		this.dayIdx = dayIdx;
		this.timeFrom = DateHelper.getDate(timeFrom);
		this.timeTo = DateHelper.getDate(timeTo);
	}
	public Planning(String day, int dayIdx, Date timeFrom, Date timeTo,
			POIElement poiElement, Device device) {
		super();
		this.day = day;
		this.dayIdx = dayIdx;
		this.timeFrom = timeFrom;
		this.timeTo = timeTo;
		this.poiElement = poiElement;
		this.device = device;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public int getDayIdx() {
		return dayIdx;
	}

	public void setDayIdx(int dayIdx) {
		this.dayIdx = dayIdx;
	}

	public Date getTimeFrom() {
		return timeFrom;
	}

	public void setTimeFrom(Date timeFrom) {
		this.timeFrom = timeFrom;
	}

	public Date getTimeTo() {
		return timeTo;
	}

	public void setTimeTo(Date timeTo) {
		this.timeTo = timeTo;
	}

	public POIElement getPoiElement() {
		return poiElement;
	}

	public void setPoiElement(POIElement poiElement) {
		this.poiElement = poiElement;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}
}