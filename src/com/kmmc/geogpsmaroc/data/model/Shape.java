package com.kmmc.geogpsmaroc.data.model;

//Table d'heritage, aucune table physique
public class Shape extends PersistentObject {
	
	private static final long serialVersionUID = 1L;
	
	public Device device;

	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}
}