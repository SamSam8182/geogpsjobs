package com.kmmc.geogpsmaroc.data.model;

//Aucune table physique, sert à simplifier l'utilisation de coordonnées GPS
public class LatLng {
	private double latitude;
	private double longitude;
	public Polygon polygon;
	public Circle circle;

    public LatLng(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public double getX(){
        return this.longitude;
    }
    
    public double getY(){
        return this.latitude;
    }

    public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public Polygon getPolygon() {
		return polygon;
	}

	public void setPolygon(Polygon polygon) {
		this.polygon = polygon;
	}

	public Circle getCircle() {
		return circle;
	}

	public void setCircle(Circle circle) {
		this.circle = circle;
	}

	public boolean isNull(){
        return (this.latitude==0 && this.longitude==0);
    }
}