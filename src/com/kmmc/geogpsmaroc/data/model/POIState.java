/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmmc.geogpsmaroc.data.model;

/**
 * @author dell
 */
//Aucune table physique, sert à garder les états des poi
public class POIState {
    private int from;
    private int to;
    private String name;
    private String date;
    private boolean attended;
    private boolean alerted;
    
	public POIState(int from, int to, String name, String date, boolean attended,
			boolean alerted) {
		super();
		this.from = from;
		this.to = to;
		this.name = name;
		this.setDate(date);
		this.attended = attended;
		this.alerted = alerted;
	}
	
	public int getFrom() {
		return from;
	}
	public void setFrom(int from) {
		this.from = from;
	}
	public int getTo() {
		return to;
	}
	public void setTo(int to) {
		this.to = to;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isAttended() {
		return attended;
	}
	public void setAttended(boolean attended) {
		this.attended = attended;
	}
	public boolean isAlerted() {
		return alerted;
	}
	public void setAlerted(boolean alerted) {
		this.alerted = alerted;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
