package com.kmmc.geogpsmaroc.data.model;

import java.util.Date;

import com.kmmc.geogpsmaroc.business.helpers.DateHelper;

//Table rapport, et peux être aussi la table rapport_etalonnage
public class Position extends PersistentObject {
	private static final long serialVersionUID = 1L;
	
	// DATA
	private Long idVehiculeAppareil;
	private Long idChauffeur;
	private Date date;
	private Date heure;
	private Double latitude;
	private Double longitude;
	private Double altitude;
	private Double speed;
	private Double compteur;
	private Double compteurBoitier;
	private Integer gsmLevel;
	private Double course;
	private String sensDeviation;
	private String etatMoteur;
	private Integer satellite;
	private Double temperature;
	private Double fuel;
	private Double carburantConso;
	private Double fuelCanbus;
	private Boolean engine;
	private Boolean treated;
	private Integer inputTrigger;
	private Integer powerSupply;

	// BUSINESS
	private Long deviceId;
	private Date time;
	private Boolean valid;
	private Device device;

	public Position() {
	}

	public Position(long deviceId, Date time, boolean valid, double latitude,
			double longitude, double altitude, double speed, double course) {

		setDeviceId(deviceId);
		setTime(time);
		setValid(valid);
		setLatitude(latitude);
		setLongitude(longitude);
		setAltitude(altitude);
		setSpeed(speed);
		setCourse(course);
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public Date getTime() {
		if(time == null)
		{
			this.time = DateHelper.getDate(DateHelper.getStringDate(this.date), DateHelper.getStringTime(this.heure));
		}
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public int getGsmLevel() {
		return gsmLevel;
	}

	public void setGsmLevel(int gsmLevel) {
		this.gsmLevel = gsmLevel;
	}

	public Double getFuelCanbus() {
		return fuelCanbus;
	}

	public void setFuelCanbus(Double fuelCanbus) {
		this.fuelCanbus = fuelCanbus;
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public Double getCourse() {
		return course;
	}

	public void setCourse(Double course) {
		this.course = course;
	}

	public int getSatellite() {
		return satellite;
	}

	public void setSatellite(int satellite) {
		this.satellite = satellite;
	}

	public Double getFuel() {
		return fuel;
	}

	public void setFuel(Double fuel) {
		this.fuel = fuel;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Long getIdVehiculeAppareil() {
		return idVehiculeAppareil;
	}

	public void setIdVehiculeAppareil(Long idVehiculeAppareil) {
		this.idVehiculeAppareil = idVehiculeAppareil;
	}

	public Long getIdChauffeur() {
		return idChauffeur;
	}

	public void setIdChauffeur(Long idChauffeur) {
		this.idChauffeur = idChauffeur;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getHeure() {
		return heure;
	}

	public void setHeure(Date heure) {
		this.heure = heure;
	}

	public Double getCompteur() {
		return compteur;
	}

	public void setCompteur(Double compteur) {
		this.compteur = compteur;
	}

	public Double getCompteurBoitier() {
		return compteurBoitier;
	}

	public void setCompteurBoitier(Double compteurBoitier) {
		this.compteurBoitier = compteurBoitier;
	}

	public String getSensDeviation() {
		return sensDeviation;
	}

	public void setSensDeviation(String sensDeviation) {
		this.sensDeviation = sensDeviation;
	}

	public String getEtatMoteur() {
		return etatMoteur;
	}

	public void setEtatMoteur(String etatMoteur) {
		this.etatMoteur = etatMoteur;
	}

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public Double getCarburantConso() {
		return carburantConso;
	}

	public void setCarburantConso(Double carburantConso) {
		this.carburantConso = carburantConso;
	}
	
	public Boolean getEngine() {
		return engine;
	}

	public void setEngine(Boolean engine) {
		this.engine = engine;
	}

	public Boolean getTreated() {
		return treated;
	}

	public void setTreated(Boolean treated) {
		this.treated = treated;
	}

	public Integer getInputTrigger() {
		return inputTrigger;
	}

	public void setInputTrigger(Integer inputTrigger) {
		this.inputTrigger = inputTrigger;
	}

	public Integer getPowerSupply() {
		return powerSupply;
	}

	public void setPowerSupply(Integer powerSupply) {
		this.powerSupply = powerSupply;
	}
}
