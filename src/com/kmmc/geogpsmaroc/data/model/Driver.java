package com.kmmc.geogpsmaroc.data.model;

//Table chauffeur
public class Driver extends PersistentObject {
	
	private static final long serialVersionUID = 1L;
	
	//DATA
	private String idChauffeur;
	private String nom ;
	private String prenom ;
	private String tel;
	private Boolean canceled; 
	private Long idCompte ;
	
	//Business
	public Device device;
	
	public Driver(String tel, String lastName, String firstName, Device device) {
		super();
		this.tel = tel;
		this.nom = lastName;
		this.prenom = firstName;
		this.device = device;
	}
	public Driver() {

	}

	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}
	public String getIdChauffeur() {
		return idChauffeur;
	}
	public void setIdChauffeur(String idChauffeur) {
		this.idChauffeur = idChauffeur;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Boolean getCanceled() {
		return canceled;
	}
	public void setCanceled(Boolean canceled) {
		this.canceled = canceled;
	}
	public Long getIdCompte() {
		return idCompte;
	}
	public void setIdCompte(Long idCompte) {
		this.idCompte = idCompte;
	}

}