package com.kmmc.geogpsmaroc.data.model;

import java.util.Date;

public class AlertProgress extends PersistentObject{

	private static final long serialVersionUID = 1L;

	private Long idVehicule;
	private Integer speedMax;
	private Date dateFrom;
	private Double fuelFrom;
	private Double counterFrom;
	private String type;
	
	public Long getIdVehicule() {
		return idVehicule;
	}
	public void setIdVehicule(Long idVehicule) {
		this.idVehicule = idVehicule;
	}
	public Integer getSpeedMax() {
		return speedMax;
	}
	public void setSpeedMax(Integer speedMax) {
		this.speedMax = speedMax;
	}
	public Date getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}
	public Double getFuelFrom() {
		return fuelFrom;
	}
	public void setFuelFrom(Double fuelFrom) {
		this.fuelFrom = fuelFrom;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getCounterFrom() {
		return counterFrom;
	}
	public void setCounterFrom(Double counterFrom) {
		this.counterFrom = counterFrom;
	}
}
