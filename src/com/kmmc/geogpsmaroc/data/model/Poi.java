package com.kmmc.geogpsmaroc.data.model;

//Table poi
public class Poi extends PersistentObject {
	
	private static final long serialVersionUID = 1L;
	
	// DATA
	private String details;
	private String nom;
	private String description;
	private Long idPoiAlerte;
	private Long idCompte;

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getIdPoiAlerte() {
		return idPoiAlerte;
	}

	public void setIdPoiAlerte(Long idPoiAlerte) {
		this.idPoiAlerte = idPoiAlerte;
	}

	public Long getIdCompte() {
		return idCompte;
	}

	public void setIdCompte(Long idCompte) {
		this.idCompte = idCompte;
	}
}