package com.kmmc.geogpsmaroc.data.model;

//Table module
public class Module extends PersistentObject {
	
	private static final long serialVersionUID = 1L;
	
	// DATA
	private String nom;
	private String methode;
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getMethode() {
		return methode;
	}
	public void setMethode(String methode) {
		this.methode = methode;
	}
	
}