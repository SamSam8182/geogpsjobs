package com.kmmc.geogpsmaroc.data.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.transform.ToListResultTransformer;

//Table Vehicule
public class Device extends PersistentObject {
	private static final long serialVersionUID = 1L;
	
	// DATA
	private String registration;
	private String internCode;
	private Long idGroupe;
	private Long idAgendaTravail;
	private Long idPiece;
	private String description;
	private Double compteurInitial;
	private double counter;
	private String type; 
	private String vehicleType;
	private Boolean canbus;
	private Double voltageNiveauMin;
	private Double maxVolt;
	private Double calibrationVolt;
	private Double calibrationLiter;
	private Double fuelCapacity;
	private Double alertSpeed;
	private String commentaire;
	private Long idZone;
	private Long idPoi;
	private Boolean alertEnabled;
	private Boolean registred;
	private Long lastActiveRapport;
	private Boolean disableFilter;
	private String trancheEtalonnage;
	private String SIM;
	private String formule;
	private Long idCompte;
	private Boolean isPowered;
	private Date lastArchive;
	private Date dateVehiculeAppareilChanged;

	// BUSINESS
	private Position previousPosition;
	private int accumulatedDistance;
	private List<Shape> zones;
	private List<POIElement> pois;
	private List<Planning> plannings;
	private int fuelLevelTolerated;
	private List<CalibrationElement> calibrations;
	private List<FuelRecord> fuelRecords;
	private List<BaseAlert> alertStates;
	private List<Piece> pieces;
	public Driver driver;
	public Account account;

	public Device(){
		alertStates = new ArrayList<BaseAlert>();
		alertStates.add(new POIAlert());
		alertStates.add(new ZoneAlert());
		alertStates.add(new AgendaAlert());
		alertStates.add(new SpeedAlert());
		alertStates.add(new FuelAlert());
		alertStates.add(new WireAlert());
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public double getCounter() {
		return counter;
	}

	public void setCounter(double counter) {
		this.counter = counter;
	}

	public boolean isCanbus() {
		return canbus;
	}

	public void setCanbus(boolean canbus) {
		this.canbus = canbus;
	}

	public boolean isDisableFilter() {
		return disableFilter;
	}

	public void setDisableFilter(boolean disableFilter) {
		this.disableFilter = disableFilter;
	}

	public Position getPreviousPosition() {
		return previousPosition;
	}

	public void setPreviousPosition(Position previousPosition) {
		this.previousPosition = previousPosition;
	}

	public int getAccumulatedDistance() {
		return accumulatedDistance;
	}

	public void setAccumulatedDistance(int accumulatedDistance) {
		this.accumulatedDistance = accumulatedDistance;
	}

	public List<Shape> getZones() {
		return zones;
	}

	public void setZones(List<Shape> zones) {
		this.zones = zones;
	}

	public List<POIElement> getPois() {
		return pois;
	}

	public void setPois(List<POIElement> pois) {
		this.pois = pois;
	}

	public List<Planning> getPlannings() {
		return plannings;
	}

	public void setPlannings(List<Planning> plannings) {
		this.plannings = plannings;
	}

	public int getFuelLevelTolerated() {
		if(fuelLevelTolerated!= 0)
			return fuelLevelTolerated;
		else
			return (int) ((fuelCapacity / 10 > 15) ? 15 : fuelCapacity / 10);
	}

	public void setFuelLevelTolerated(int fuelLevelTolerated) {
		this.fuelLevelTolerated = fuelLevelTolerated;
	}

	public boolean isAlertEnabled() {
		return alertEnabled;
	}

	public void setAlertEnabled(boolean alertEnabled) {
		this.alertEnabled = alertEnabled;
	}

	public List<CalibrationElement> getCalibrations() {
		return calibrations;
	}

	public void setCalibrations(List<CalibrationElement> calibrations) {
		this.calibrations = calibrations;
	}

	public List<FuelRecord> getFuelRecords() {
		return fuelRecords;
	}

	public void setFuelRecords(List<FuelRecord> fuelRecords) {
		this.fuelRecords = fuelRecords;
	}

	public List<BaseAlert> getAlertStates() {
		return alertStates;
	}

	public void setAlertStates(List<BaseAlert> alertStates) {
		this.alertStates = alertStates;
	}

	public List<Piece> getPieces() {
		return pieces;
	}

	public void setPieces(List<Piece> pieces) {
		this.pieces = pieces;
	}

	public String getInternCode() {
		return internCode;
	}

	public void setInternCode(String internCode) {
		this.internCode = internCode;
	}

	public boolean isRegistred() {
		return registred;
	}

	public void setRegistred(boolean registred) {
		this.registred = registred;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public void setAlert_state(BaseAlert base_alert, TypesAlert type) {
		for (BaseAlert base : alertStates) {
			switch (type) {
			case POI:
				if (base instanceof POIAlert) {
					((POIAlert) base).setIdChauffeur(((POIAlert) base_alert).getIdChauffeur());
					((POIAlert) base).setIdVehicule(((POIAlert) base_alert).getIdVehicule());
					((POIAlert) base).setTypeAlerte(((POIAlert) base_alert).getTypeAlerte());
					((POIAlert) base).setIdCompte(((POIAlert) base_alert).getIdCompte());
					((POIAlert) base).setTraiter(((POIAlert) base_alert).getTraiter());
				}
				break;
			case TIME:
				if (base instanceof AgendaAlert) {
					((AgendaAlert) base).setIdChauffeur(((AgendaAlert) base_alert).getIdChauffeur());
					((AgendaAlert) base).setIdVehicule(((AgendaAlert) base_alert).getIdVehicule());
					((AgendaAlert) base).setTypeAlerte(((AgendaAlert) base_alert).getTypeAlerte());
					((AgendaAlert) base).setIdCompte(((AgendaAlert) base_alert).getIdCompte());

					((AgendaAlert) base).setDatetimeFrom(((AgendaAlert) base_alert).getDatetimeFrom());
					((AgendaAlert) base).setInprogressAlert(((AgendaAlert) base_alert).isInprogressAlert());
					((AgendaAlert) base).setFuelFrom(((AgendaAlert) base_alert).getFuelFrom());
					((AgendaAlert) base).setCounterFrom(((AgendaAlert) base_alert).getCounterFrom());
					((AgendaAlert) base).setTraiter(((AgendaAlert) base_alert).getTraiter());
				}
				break;
			case ZONE:
				if (base instanceof ZoneAlert) {
					((ZoneAlert) base).setIdChauffeur(((ZoneAlert) base_alert).getIdChauffeur());
					((ZoneAlert) base).setIdVehicule(((ZoneAlert) base_alert).getIdVehicule());
					((ZoneAlert) base).setTypeAlerte(((ZoneAlert) base_alert).getTypeAlerte());
					((ZoneAlert) base).setIdCompte(((ZoneAlert) base_alert).getIdCompte());

					((ZoneAlert) base).setDatetimeFrom(((ZoneAlert) base_alert).getDatetimeFrom());
					((ZoneAlert) base).setInprogressAlert(((ZoneAlert) base_alert).isInprogressAlert());
					((ZoneAlert) base).setFuelFrom(((ZoneAlert) base_alert).getFuelFrom());
					((ZoneAlert) base).setCounterFrom(((ZoneAlert) base_alert).getCounterFrom());
					((ZoneAlert) base).setTraiter(((ZoneAlert) base_alert).getTraiter());
				}
				break;
			case SPEED:
				if (base instanceof SpeedAlert) {
					((SpeedAlert) base).setIdChauffeur(((SpeedAlert) base_alert).getIdChauffeur());
					((SpeedAlert) base).setIdVehicule(((SpeedAlert) base_alert).getIdVehicule());
					((SpeedAlert) base).setTypeAlerte(((SpeedAlert) base_alert).getTypeAlerte());
					((SpeedAlert) base).setVitesseAlerte(((SpeedAlert) base_alert).getVitesseAlerte());
					((SpeedAlert) base).setIdCompte(((SpeedAlert) base_alert).getIdCompte());

					((SpeedAlert) base).setDatetimeFrom(((SpeedAlert) base_alert).getDatetimeFrom());
					((SpeedAlert) base).setVitesseMax(((SpeedAlert) base_alert).getVitesseMax());
					((SpeedAlert) base).setInprogressAlert(((SpeedAlert) base_alert).isInprogressAlert());
					((SpeedAlert) base).setFuelFrom(((SpeedAlert) base_alert).getFuelFrom());
					((SpeedAlert) base).setCounterFrom(((SpeedAlert) base_alert).getCounterFrom());
					((SpeedAlert) base).setTraiter(((SpeedAlert) base_alert).getTraiter());
					
				}
				break;
			case GASOIL_CONSUMED: case GASOIL_ADDED:
				if (base instanceof FuelAlert) {
					((FuelAlert) base).setIdChauffeur(((FuelAlert) base_alert).getIdChauffeur());
					((FuelAlert) base).setIdVehicule(((FuelAlert) base_alert).getIdVehicule());
					((FuelAlert) base).setTypeAlerte(((FuelAlert) base_alert).getTypeAlerte());
					((FuelAlert) base).setTraiter(((FuelAlert) base_alert).getTraiter());
					((FuelAlert) base).setIdCompte(((FuelAlert) base_alert).getIdCompte());
				}
				break;
			case WIRE_OFF: case WIRE_ON:
				if (base instanceof WireAlert) {
					((WireAlert) base).setIdChauffeur(((WireAlert) base_alert).getIdChauffeur());
					((WireAlert) base).setIdVehicule(((WireAlert) base_alert).getIdVehicule());
					((WireAlert) base).setTypeAlerte(((WireAlert) base_alert).getTypeAlerte());
					((WireAlert) base).setTraiter(((WireAlert) base_alert).getTraiter());
					((WireAlert) base).setIdCompte(((WireAlert) base_alert).getIdCompte());
				}
				break;
			}
		}
	}

	public void removeAlert_state(TypesAlert type) {
		ArrayList<BaseAlert> cpyAlertStates = (ArrayList<BaseAlert>) ((ArrayList<BaseAlert>)alertStates).clone();
		for (BaseAlert base : cpyAlertStates) {
			switch (type) {
			case ZONE:
				if (base instanceof ZoneAlert)
					alertStates.remove(base);
				break;
			case POI:
				if (base instanceof POIAlert)
					alertStates.remove(base);
				break;
			case TIME:
				if (base instanceof AgendaAlert)
					alertStates.remove(base);
				break;
			case GASOIL_CONSUMED: case GASOIL_ADDED:
				if (base instanceof FuelAlert)
					alertStates.remove(base);
				break;
			case SPEED:
				if (base instanceof SpeedAlert)
					alertStates.remove(base);
				break;
			case WIRE_OFF: case WIRE_ON:
				if (base instanceof WireAlert)
					alertStates.remove(base);
				break;
			}
		}
	}

	public Piece getPiece(TypesPiece type) {
		for (Piece piece : pieces) {
			if (piece.getTypesPiece() == type)
				return piece;
		}

		return null;
	}

	public Long getIdGroupe() {
		return idGroupe;
	}

	public void setIdGroupe(Long idGroupe) {
		this.idGroupe = idGroupe;
	}

	public Long getIdAgendaTravail() {
		return idAgendaTravail;
	}

	public void setIdAgendaTravail(Long idAgendaTravail) {
		this.idAgendaTravail = idAgendaTravail;
	}

	public Long getIdPiece() {
		return idPiece;
	}

	public void setIdPiece(Long idPiece) {
		this.idPiece = idPiece;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getCompteurInitial() {
		return compteurInitial;
	}

	public void setCompteurInitial(Double compteurInitial) {
		this.compteurInitial = compteurInitial;
	}

	public Boolean getCanbus() {
		return canbus;
	}

	public void setCanbus(Boolean canbus) {
		this.canbus = canbus;
	}

	public Double getAlertSpeed() {
		return alertSpeed;
	}

	public void setAlertSpeed(Double vitesseAlerte) {
		this.alertSpeed = vitesseAlerte;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Long getIdZone() {
		return idZone;
	}

	public void setIdZone(Long idZone) {
		this.idZone = idZone;
	}

	public Long getIdPoi() {
		return idPoi;
	}

	public void setIdPoi(Long idPoi) {
		this.idPoi = idPoi;
	}

	public Long getLastActiveRapport() {
		return lastActiveRapport;
	}

	public void setLastActiveRapport(Long lastActiveRapport) {
		this.lastActiveRapport = lastActiveRapport;
	}

	public String getTrancheEtalonnage() {
		return trancheEtalonnage;
	}

	public void setTrancheEtalonnage(String trancheEtalonnage) {
		this.trancheEtalonnage = trancheEtalonnage;
	}

	public String getSIM() {
		return SIM;
	}

	public void setSIM(String sIM) {
		SIM = sIM;
	}

	public String getFormule() {
		return formule;
	}

	public void setFormule(String formule) {
		this.formule = formule;
	}

	public Double getVoltageNiveauMin() {
		return voltageNiveauMin;
	}

	public void setVoltageNiveauMin(Double voltageNiveauMin) {
		this.voltageNiveauMin = voltageNiveauMin;
	}

	public Double getMaxVolt() {
		return maxVolt;
	}

	public void setMaxVolt(Double maxVolt) {
		this.maxVolt = maxVolt;
	}

	public Double getCalibrationVolt() {
		return calibrationVolt;
	}

	public void setCalibrationVolt(Double calibrationVolt) {
		this.calibrationVolt = calibrationVolt;
	}

	public Double getFuelCapacity() {
		return fuelCapacity;
	}

	public void setFuelCapacity(Double fuelCapacity) {
		this.fuelCapacity = fuelCapacity;
	}

	public Double getCalibrationLiter() {
		return calibrationLiter;
	}

	public void setCalibrationLiter(Double calibrationLiter) {
		this.calibrationLiter = calibrationLiter;
	}

	public Long getIdCompte() {
		return idCompte;
	}

	public void setIdCompte(Long idCompte) {
		this.idCompte = idCompte;
	}

	
	public Boolean getIsPowered() {
		return isPowered;
	}

	
	public void setIsPowered(Boolean isPowered) {
		this.isPowered = isPowered;
	}

	public Date getLastArchive() {
		return lastArchive;
	}

	public void setLastArchive(Date lastArchive) {
		this.lastArchive = lastArchive;
	}

	public Date getDateVehiculeAppareilChanged() {
		return dateVehiculeAppareilChanged;
	}

	public void setDateVehiculeAppareilChanged(
			Date dateVehiculeAppareilChanged) {
		this.dateVehiculeAppareilChanged = dateVehiculeAppareilChanged;
	}
}