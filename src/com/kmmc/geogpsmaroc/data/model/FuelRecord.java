package com.kmmc.geogpsmaroc.data.model;

import java.util.Date;

//Aucune table physique, sert a faire des calculs de carburant dans le moteur
public class FuelRecord {
	private Date datetime;
	private Double counter;
	private Double fuelLevel;
	private Double fuelConsumed;
	public Device device;
	
	public FuelRecord(Date datetime, Double counter, Double fuelLevel, Double fuelConsumed){
		this.datetime = datetime;
		this.counter = counter;
		this.fuelLevel = fuelLevel;
		this.fuelConsumed = fuelConsumed;
	}
	
	public Date getDatetime() {
		return datetime;
	}
	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}
	public Double getCounter() {
		return counter;
	}
	public void setCounter(Double counter) {
		this.counter = counter;
	}
	public Double getFuelLevel() {
		return fuelLevel;
	}
	public void setFuelLevel(Double fuelLevel) {
		this.fuelLevel = fuelLevel;
	}
	public Double getFuelConsumed() {
		return fuelConsumed;
	}
	public void setFuelConsumed(Double fuelConsumed) {
		this.fuelConsumed = fuelConsumed;
	}
	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}

}