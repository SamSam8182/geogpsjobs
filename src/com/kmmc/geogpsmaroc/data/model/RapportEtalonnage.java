package com.kmmc.geogpsmaroc.data.model;

import java.util.Date;

//Table Vehicule sous format JSON dans le champ (etalonnage)
public class RapportEtalonnage extends PersistentObject{
	private static final long serialVersionUID = 1L;
	
	//DATA
	private Date date;
	private Date heure;
	private Double latitude;
	private Double longitude;
	private Integer vitesse;
	private Double compteur;
	private Double niveauCarburant;
	private Long idVehiculeAppareil;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getHeure() {
		return heure;
	}
	public void setHeure(Date heure) {
		this.heure = heure;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Integer getVitesse() {
		return vitesse;
	}
	public void setVitesse(Integer vitesse) {
		this.vitesse = vitesse;
	}
	public Double getCompteur() {
		return compteur;
	}
	public void setCompteur(Double compteur) {
		this.compteur = compteur;
	}
	public Double getNiveauCarburant() {
		return niveauCarburant;
	}
	public void setNiveauCarburant(Double niveauCarburant) {
		this.niveauCarburant = niveauCarburant;
	}
	public Long getIdVehiculeAppareil() {
		return idVehiculeAppareil;
	}
	public void setIdVehiculeAppareil(Long idVehiculeAppareil) {
		this.idVehiculeAppareil = idVehiculeAppareil;
	}
}