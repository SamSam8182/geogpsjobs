/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmmc.geogpsmaroc.data.model;

/**
 * @author dell
 */
public enum TypesPiece {
    ASSURANCE,
    VIDANGE,
    VISITE_TECHNIQUE,
    CARNET_METROLOGIQUE,
    CARNET_CIRCULATION,
    AUTORISATION_CIRCULATION;
}
