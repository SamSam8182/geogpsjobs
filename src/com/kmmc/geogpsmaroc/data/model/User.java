package com.kmmc.geogpsmaroc.data.model;

//Table Utilisateur
public class User extends PersistentObject {
	private static final long serialVersionUID = 1L;
	
	// DATA
	private String username;
	private String passcode;
	private String nom;
	private String prenom;
	private String email;
	private String commentaire;
	private Long idRole;
	private Long idCompte;

	// Business
	private boolean notify_alert;
	private boolean notify_piece;
	private boolean read_alert;
	private boolean read_piece;

	public boolean isNotify_alert() {
		return notify_alert;
	}

	public void setNotify_alert(boolean notify_alert) {
		this.notify_alert = notify_alert;
	}

	public boolean isNotify_piece() {
		return notify_piece;
	}

	public void setNotify_piece(boolean notify_piece) {
		this.notify_piece = notify_piece;
	}

	public boolean isRead_alert() {
		return read_alert;
	}

	public void setRead_alert(boolean read_alert) {
		this.read_alert = read_alert;
	}

	public boolean isRead_piece() {
		return read_piece;
	}

	public void setRead_piece(boolean read_piece) {
		this.read_piece = read_piece;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPasscode() {
		return passcode;
	}

	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Long getIdRole() {
		return idRole;
	}

	public void setIdRole(Long idRole) {
		this.idRole = idRole;
	}

	public Long getIdCompte() {
		return idCompte;
	}

	public void setIdCompte(Long idCompte) {
		this.idCompte = idCompte;
	}

}