/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmmc.geogpsmaroc.data.model;

/**
 * @author dell
 */
public enum TypesAlert {
    POI,
    ZONE,
    SPEED,
    TIME,
    GASOIL_CONSUMED,
    GASOIL_ADDED,
    WIRE_ON,
    WIRE_OFF;
}
