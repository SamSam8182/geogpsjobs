package com.kmmc.geogpsmaroc.data.model;

//Table ROLE
public class Role extends PersistentObject {
	private static final long serialVersionUID = 1L;
	
	// DATA
	private String nom;

	private Long idCompte;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Long getIdCompte() {
		return idCompte;
	}

	public void setIdCompte(Long idCompte) {
		this.idCompte = idCompte;
	}

}