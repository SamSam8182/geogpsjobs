package com.kmmc.geogpsmaroc.data.model;

//Table vehicule_utilisateur
public class VehiculeUtilisateur extends PersistentObject {

	private static final long serialVersionUID = 1L;
	
	//DATA
	private Long idUtilisateur;
	private Long idVehicule;
	
	public Long getIdUtilisateur() {
		return idUtilisateur;
	}
	public void setIdUtilisateur(Long idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	public Long getIdVehicule() {
		return idVehicule;
	}
	public void setIdVehicule(Long idVehicule) {
		this.idVehicule = idVehicule;
	}
    
}