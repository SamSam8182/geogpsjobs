package com.kmmc.geogpsmaroc.data.model;

//Table geozone sous format JSON colonne (shape)
public class Circle extends Shape {
	
	private static final long serialVersionUID = 1L;
	
	private LatLng center;
	private double radius;
	public POIElement poiElement;
	
	public LatLng getCenter() {
		return center;
	}
	public void setCenter(LatLng center) {
		this.center = center;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public POIElement getPoiElement() {
		return poiElement;
	}
	public void setPoiElement(POIElement poiElement) {
		this.poiElement = poiElement;
	}

}