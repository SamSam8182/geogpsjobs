package com.kmmc.geogpsmaroc.data.model;

import java.util.List;

//Table Historique_poi specifiquement la colonne details
public class POIElement extends PersistentObject{
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private String description;
	private Long idHistorique; 
	private String details;//JSON
	
	//Fields filled with JSON transformer
    private Circle zone;
    private boolean alertOnExit;
    private boolean haveToStop;
    private TypesPOI type;
    private List<Planning> plannings;
    public Device device;
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Circle getZone() {
		return zone;
	}
	public void setZone(Circle zone) {
		this.zone = zone;
	}
	public boolean isAlertOnExit() {
		return alertOnExit;
	}
	public void setAlertOnExit(boolean alertOnExit) {
		this.alertOnExit = alertOnExit;
	}
	public boolean isHaveToStop() {
		return haveToStop;
	}
	public void setHaveToStop(boolean haveToStop) {
		this.haveToStop = haveToStop;
	}
	public TypesPOI getType() {
		return type;
	}
	public void setType(TypesPOI type) {
		this.type = type;
	}
	public List<Planning> getPlannings() {
		return plannings;
	}
	public void setPlannings(List<Planning> plannings) {
		this.plannings = plannings;
	}
	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getIdHistorique() {
		return idHistorique;
	}
	public void setIdHistorique(Long idHistorique) {
		this.idHistorique = idHistorique;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
   
}