package com.kmmc.geogpsmaroc.data.model;

import java.util.Date;

//Table vehicule_utilisateur
public class VehiculeAppareil extends PersistentObject {
	
	private static final long serialVersionUID = 1L;
	
	//DATA
	private Long idAppareil;
	private Long idVehicule;
	private Date dateDebut;
	private Date dateFin;
	private String commentaire;
	
	public Long getIdAppareil() {
		return idAppareil;
	}
	public void setIdAppareil(Long idAppareil) {
		this.idAppareil = idAppareil;
	}
	public Long getIdVehicule() {
		return idVehicule;
	}
	public void setIdVehicule(Long idVehicule) {
		this.idVehicule = idVehicule;
	}
	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
    
}