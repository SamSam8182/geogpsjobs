package com.kmmc.geogpsmaroc.data.model;

//Table rapport_etalonnage
public class CalibrationElement extends PersistentObject {

	private static final long serialVersionUID = 1L;
	
	private int literMax;
	private int maxVolt;
	private int minVolt;
	private double rate;
	public Device device;

	public int getLiterMax() {
		return literMax;
	}

	public void setLiterMax(int literMax) {
		this.literMax = literMax;
	}

	public int getMaxVolt() {
		return maxVolt;
	}

	public void setMaxVolt(int maxVolt) {
		this.maxVolt = maxVolt;
	}

	public int getMinVolt() {
		return minVolt;
	}

	public void setMinVolt(int minVolt) {
		this.minVolt = minVolt;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public void setLiter_max(String _liter_max) {
        this.literMax = Integer.parseInt(_liter_max);
    }

	public void setMinMaxVolt(String[] _min_max_volt){
        this.minVolt = Integer.valueOf(_min_max_volt[1]);
        this.maxVolt = Integer.valueOf(_min_max_volt[0]);
    }

	public boolean contains(double _volt){
        return ((this.minVolt <= _volt) && (_volt <= this.maxVolt)) 
                || ((this.maxVolt <= _volt) && (_volt <= this.minVolt));
    }
}