package com.kmmc.geogpsmaroc.data.model;

import java.util.Date;

//Table Piece
public class Piece extends PersistentObject {
	private static final long serialVersionUID = 1L;
	
	// DATA
	private Long idVehicule;
	private String numero;
	private Date dateDebut;
	private Date dateFin;
	private Date dateAlerte;
	private Double montant;
	private Long idPersonneTraite;
	private Date tempsTraitement;
	private String societe;
	private String nCap;
	private String nom;
	private String prenom;
	private String adresse;
	private String defaultConstates;
	private String observation;
	private String numeroPolice;
	private String intermediaire;
	private String type;
	private Integer kmDebut;
	private Integer kmFin;
	private Integer kmAlert;
	private String vidangeEffectuee;
	private String typePiece;
	private Boolean alerted;

	// Business
	private boolean mailed;
	private String mailText;
	private String mailSubject;
	private TypesPiece typesPiece;
	public Device device;

	public Piece(){
		
	}
	public Piece(boolean mailed, int kmAlert, TypesPiece typesPiece) {
		this.mailed = mailed;
		this.kmAlert = kmAlert;
		this.typesPiece = typesPiece;
	}
	public Piece(boolean mailed, Date dateAlert, TypesPiece typesPiece) {
		this.mailed = mailed;
		this.dateAlerte = dateAlert;
		this.typesPiece = typesPiece;
	}

	public boolean isMailed() {
		return mailed;
	}

	public void setMailed(boolean mailed) {
		this.mailed = mailed;
	}

	public String getMailText() {
		return mailText;
	}

	public void setMailText(String mailText) {
		this.mailText = mailText;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public Integer getKmAlert() {
		return kmAlert;
	}

	public void setKmAlert(Integer kmAlert) {
		this.kmAlert = kmAlert;
	}

	public TypesPiece getTypesPiece() {
		return typesPiece;
	}

	public void setTypesPiece(TypesPiece typesPiece) {
		this.typesPiece = typesPiece;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Long getIdVehicule() {
		return idVehicule;
	}

	public void setIdVehicule(Long idVehicule) {
		this.idVehicule = idVehicule;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public Date getDateAlerte() {
		return dateAlerte;
	}

	public void setDateAlerte(Date dateAlerte) {
		this.dateAlerte = dateAlerte;
	}

	public Double getMontant() {
		return montant;
	}

	public void setMontant(Double montant) {
		this.montant = montant;
	}

	public Long getIdPersonneTraite() {
		return idPersonneTraite;
	}

	public void setIdPersonneTraite(Long idPersonneTraite) {
		this.idPersonneTraite = idPersonneTraite;
	}

	public Date getTempsTraitement() {
		return tempsTraitement;
	}

	public void setTempsTraitement(Date tempsTraitement) {
		this.tempsTraitement = tempsTraitement;
	}

	public String getSociete() {
		return societe;
	}

	public void setSociete(String societe) {
		this.societe = societe;
	}

	public String getnCap() {
		return nCap;
	}

	public void setnCap(String nCap) {
		this.nCap = nCap;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getDefaultConstates() {
		return defaultConstates;
	}

	public void setDefaultConstates(String defaultConstates) {
		this.defaultConstates = defaultConstates;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public String getNumeroPolice() {
		return numeroPolice;
	}

	public void setNumeroPolice(String numeroPolice) {
		this.numeroPolice = numeroPolice;
	}

	public String getIntermediaire() {
		return intermediaire;
	}

	public void setIntermediaire(String intermediaire) {
		this.intermediaire = intermediaire;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getKmDebut() {
		return kmDebut;
	}

	public void setKmDebut(Integer kmDebut) {
		this.kmDebut = kmDebut;
	}

	public Integer getKmFin() {
		return kmFin;
	}

	public void setKmFin(Integer kmFin) {
		this.kmFin = kmFin;
	}
	
	public String getVidangeEffectuee() {
		return vidangeEffectuee;
	}

	public void setVidangeEffectuee(String vidangeEffectuee) {
		this.vidangeEffectuee = vidangeEffectuee;
	}

	public String getTypePiece() {
		return typePiece;
	}

	public void setTypePiece(String typePiece) {
		this.typePiece = typePiece;
	}

	public Boolean getAlerted() {
		return alerted;
	}

	public void setAlerted(Boolean alerted) {
		this.alerted = alerted;
	}

}