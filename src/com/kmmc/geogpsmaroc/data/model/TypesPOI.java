package com.kmmc.geogpsmaroc.data.model;

public enum TypesPOI {
	ATTEINTE, 
    NON_ATTEINTE;
}