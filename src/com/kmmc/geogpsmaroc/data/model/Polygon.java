package com.kmmc.geogpsmaroc.data.model;

import java.util.List;

//Table geozone sous format JSON colonne (shape)
public class Polygon extends Shape {
	
	private static final long serialVersionUID = 1L;
	
	private List<LatLng> points;
    
    public Polygon(List<LatLng> points) {
		super();
		this.points = points;
	}

	public List<LatLng> getPoints() {
		return points;
	}
	public void setPoints(List<LatLng> points) {
		this.points = points;
	}
	public int size(){
        return this.points.size();
    }
}