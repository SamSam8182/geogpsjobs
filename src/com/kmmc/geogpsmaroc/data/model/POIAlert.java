package com.kmmc.geogpsmaroc.data.model;

import java.util.Date;
import java.util.List;

//Table Alerte
public class POIAlert extends BaseAlert {
        
	private static final long serialVersionUID = 1L;
	
	private List<POIState> poiStates;

    public POIAlert(Long idHistoric, List<POIState> poiStates) {
        idHistorique = idHistoric;
        this.poiStates = poiStates;
        super.setInprogressAlert(false);
    }

	public POIAlert() {
	}
	
	public List<POIState> getPoiStates() {
		return poiStates;
	}

	public void setPoiStates(List<POIState> poiStates) {
		this.poiStates = poiStates;
	}

	public POIState getPoiState(String date, int from, int to, String name){
		if(poiStates != null)
        for(POIState p : poiStates){
            if(p.getDate().equals(date) && p.getFrom() == from && p.getTo() == to && p.getName().equals(name))
                return p;
        }
        return null;
    }
	public POIState setPoiState(String date, int from, int to, String name, boolean attended, boolean alerted){
		if(poiStates != null)
	    for(POIState p : poiStates){
            if(p.getDate().equals(date) && p.getFrom() == from && p.getTo() == to && p.getName().equals(name)){
            	p.setAttended(attended);
                p.setAlerted(alerted);
            }
        }
        return null;
    }
}