package com.kmmc.geogpsmaroc.data.model;

//Table Appareil
public class Appareil extends PersistentObject {
	
	private static final long serialVersionUID = 1L;
	
	// DATA
	private Long idAppareil;
	private String description;
	private String commentaire ;
	private String type  ;
	private String typeAbonnement;
	private Integer port ;
	private String tableToUpdate;
	
	public Long getIdAppareil() {
		return idAppareil;
	}
	public void setIdAppareil(Long idAppareil) {
		this.idAppareil = idAppareil;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTypeAbonnement() {
		return typeAbonnement;
	}
	public void setTypeAbonnement(String typeAbonnement) {
		this.typeAbonnement = typeAbonnement;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	public String getTableToUpdate() {
		return tableToUpdate;
	}
	public void setTableToUpdate(String tableToUpdate) {
		this.tableToUpdate = tableToUpdate;
	}

}