package com.kmmc.geogpsmaroc.data.model;

public class DroitAcces extends PersistentObject {

	private static final long serialVersionUID = 1L;

	private Long idRole;
	private Long idModule;
	private String droit;
	
	public Long getIdRole() {
		return idRole;
	}
	public void setIdRole(Long idRole) {
		this.idRole = idRole;
	}
	public Long getIdModule() {
		return idModule;
	}
	public void setIdModule(Long idModule) {
		this.idModule = idModule;
	}
	public String getDroit() {
		return droit;
	}
	public void setDroit(String droit) {
		this.droit = droit;
	}
}
