package com.kmmc.geogpsmaroc.data.model;

//Table Alerte
public class FuelAlert extends BaseAlert {
	
	private static final long serialVersionUID = 1L;
	
	private int literConsumed;
	private int index;
	
	public int getLiterConsumed() {
		return literConsumed;
	}
	public void setLiterConsumed(int literConsumed) {
		this.literConsumed = literConsumed;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}

	
}