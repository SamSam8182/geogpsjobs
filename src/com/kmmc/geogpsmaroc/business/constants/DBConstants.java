package com.kmmc.geogpsmaroc.business.constants;

/**
 * Constants for the business.
 */
public interface DBConstants {
	public static final String TYPE_PIECE_VIDANGE="VIDANGE";
	public static final String TYPE_PIECE_ASSURANCE="ASSURANCE";
	public static final String TYPE_PIECE_VISITE_TECHNIQUE="VISITE_TECHNIQUE";
	public static final String TYPE_PIECE_CARNET_CIRCULATION="CARNET_CIRCULATION";
	public static final String TYPE_PIECE_CARNET_METROLOGIQUE="CARNET_METROLOGIQUE";
	public static final String TYPE_PIECE_AUTORISATION_CIRCULATION="AUTORISATION_CIRCULATION";

	public static final String TYPE_ALERTE_SPEED="SPEED";
	public static final String TYPE_ALERTE_ZONE="ZONE";
	public static final String TYPE_ALERTE_TIME="TIME";
}
