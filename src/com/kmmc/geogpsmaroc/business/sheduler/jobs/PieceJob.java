package com.kmmc.geogpsmaroc.business.sheduler.jobs;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import com.kmmc.geogpsmaroc.business.constants.DBConstants;
import com.kmmc.geogpsmaroc.business.helpers.DateHelper;
import com.kmmc.geogpsmaroc.business.helpers.MailHelper;
import com.kmmc.geogpsmaroc.business.managers.DeviceManager;
import com.kmmc.geogpsmaroc.business.managers.PieceManager;
import com.kmmc.geogpsmaroc.business.managers.UserManager;
import com.kmmc.geogpsmaroc.core.services.Locator;
import com.kmmc.geogpsmaroc.core.services.log.Logger;
import com.kmmc.geogpsmaroc.core.services.mail.MailService;
import com.kmmc.geogpsmaroc.data.model.Device;
import com.kmmc.geogpsmaroc.data.model.Piece;
import com.kmmc.geogpsmaroc.data.model.TypesPiece;
import com.kmmc.geogpsmaroc.data.model.User;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;

public class PieceJob  implements StatefulJob {

	public void execute(JobExecutionContext arg0) throws JobExecutionException {

		Logger.info("////----------------DEBUT JOB PIECE-----------------////");
		//================================================================================
	    // Instanciation des Managers, Handlers & Services
	    //================================================================================

		PieceManager pieceManager = (PieceManager) Locator
				.lookup(PieceManager.class);

		DeviceManager deviceManager = (DeviceManager) Locator
				.lookup(DeviceManager.class);

		UserManager userManager = (UserManager) Locator
				.lookup(UserManager.class);
		
		MailService mailService = (MailService) Locator
				.lookup(MailService.class);

		//================================================================================
	    // Traitements du JOB
	    //================================================================================

		try {
			//Récupération des alertes non signalées
			List<Piece> listPiece = pieceManager.getPieceUnalerted();
			
			Map<Long,Device> devices = new HashMap<Long,Device>();
			Map<Long,ArrayList<User>> users = new HashMap<Long,ArrayList<User>>();

			for(Piece piece : listPiece){
				Long deviceId = piece.getIdVehicule();
				Device device = null;
				ArrayList<User> listUser = new ArrayList<User>();
				
				//Récupération de l'objet Device s'il n'existe pas encore
				if((device = devices.get(deviceId)) == null){
					device = deviceManager.lookup(deviceId);
		            
					//Mettre à jour la liste de Device
					devices.put(deviceId,device);
				}

				//Récupération de la liste d'utilisateur du compte s'il n'existe pas encore
				Long accountId = device.getIdCompte();
				if((listUser = users.get(accountId)) == null){
					listUser = userManager.getUserByIdAccount(accountId);
					
					//Mettre à jour la liste de Users
					users.put(accountId,listUser);
				}
				
				//Affectation du Device au piece
				piece.setDevice(device);
				
				//Recherche d'éventuelle pièce à notifier 
				if(piece.getTypePiece().equals("VIDANGE")){
					if(piece.getDevice().getCounter() > piece.getKmAlert()){
						sendMailPiece(piece,listUser,mailService);
						piece.setAlerted(true);
						pieceManager.save(piece);
					}
				}else{
					Date date_alerte = piece.getDateAlerte();
					if(date_alerte != null && DateHelper.compareDatesWithNow(date_alerte)<0){
						sendMailPiece(piece,listUser,mailService);
						piece.setAlerted(true);
						pieceManager.save(piece);
					}
				}
			}

		} catch (DAOException e) {
			e.printStackTrace();
		}
		Logger.info("////----------------FIN JOB PIECE-----------------////");
	}

	private void sendMailPiece(Piece piece, ArrayList<User> listUser, MailService mailService) {
		//Cast manuel du type de la pièce
		TypesPiece aType = TypesPiece.ASSURANCE;
		if(piece.getTypePiece().equals(DBConstants.TYPE_PIECE_VIDANGE)){
			aType = TypesPiece.VIDANGE;
		}else if(piece.getTypePiece().equals(DBConstants.TYPE_PIECE_ASSURANCE)){
			aType = TypesPiece.ASSURANCE;
		}else if(piece.getTypePiece().equals(DBConstants.TYPE_PIECE_VISITE_TECHNIQUE)){
			aType = TypesPiece.VISITE_TECHNIQUE;
		}else if(piece.getTypePiece().equals(DBConstants.TYPE_PIECE_AUTORISATION_CIRCULATION)){
			aType = TypesPiece.AUTORISATION_CIRCULATION;
		}else if(piece.getTypePiece().equals(DBConstants.TYPE_PIECE_CARNET_METROLOGIQUE)){
			aType = TypesPiece.CARNET_METROLOGIQUE;
		}else if(piece.getTypePiece().equals(DBConstants.TYPE_PIECE_CARNET_CIRCULATION)){
			aType = TypesPiece.CARNET_CIRCULATION;
		}
		
		//Envoi d'alerte par mail pour tout les utilisateurs du compte, s'il souhaite reçevoir ce mail, et y on le droit
		for(User user : listUser){
			if(user.isNotify_piece() && user.isRead_piece())
				mailService.send(MailHelper.getSubjectPiece(aType),user.getEmail(),MailHelper.getMsgPiece(aType, piece, user));
		}
	}

}
