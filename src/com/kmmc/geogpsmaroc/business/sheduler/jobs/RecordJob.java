package com.kmmc.geogpsmaroc.business.sheduler.jobs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import com.kmmc.geogpsmaroc.business.handlers.AlertHandler;
import com.kmmc.geogpsmaroc.business.handlers.ArchiveHandler;
import com.kmmc.geogpsmaroc.business.helpers.DateHelper;
import com.kmmc.geogpsmaroc.business.helpers.FuelHelper;
import com.kmmc.geogpsmaroc.business.helpers.JSONHelper;
import com.kmmc.geogpsmaroc.business.jsonmodel.GeozoneJSON;
import com.kmmc.geogpsmaroc.business.jsonmodel.POIJSON;
import com.kmmc.geogpsmaroc.business.managers.AccountManager;
import com.kmmc.geogpsmaroc.business.managers.AlertManager;
import com.kmmc.geogpsmaroc.business.managers.AlertProgressManager;
import com.kmmc.geogpsmaroc.business.managers.DeviceManager;
import com.kmmc.geogpsmaroc.business.managers.DriverManager;
import com.kmmc.geogpsmaroc.business.managers.PositionManager;
import com.kmmc.geogpsmaroc.business.managers.UserManager;
import com.kmmc.geogpsmaroc.core.services.Locator;
import com.kmmc.geogpsmaroc.core.services.log.Logger;
import com.kmmc.geogpsmaroc.data.model.Account;
import com.kmmc.geogpsmaroc.data.model.AgendaAlert;
import com.kmmc.geogpsmaroc.data.model.AgendaPeriode;
import com.kmmc.geogpsmaroc.data.model.BaseAlert;
import com.kmmc.geogpsmaroc.data.model.Device;
import com.kmmc.geogpsmaroc.data.model.FuelAlert;
import com.kmmc.geogpsmaroc.data.model.FuelRecord;
import com.kmmc.geogpsmaroc.data.model.GeoZone;
import com.kmmc.geogpsmaroc.data.model.POIAlert;
import com.kmmc.geogpsmaroc.data.model.POIElement;
import com.kmmc.geogpsmaroc.data.model.Planning;
import com.kmmc.geogpsmaroc.data.model.Poi;
import com.kmmc.geogpsmaroc.data.model.Position;
import com.kmmc.geogpsmaroc.data.model.SpeedAlert;
import com.kmmc.geogpsmaroc.data.model.TypesAlert;
import com.kmmc.geogpsmaroc.data.model.User;
import com.kmmc.geogpsmaroc.data.model.WireAlert;
import com.kmmc.geogpsmaroc.data.model.ZoneAlert;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;

public class RecordJob implements StatefulJob {

	public void execute(JobExecutionContext arg0) throws JobExecutionException {

		Logger.info("////----------------DEBUT JOB RECORD-----------------////");
		//================================================================================
	    // Instanciation des Managers, Handlers & Services
	    //================================================================================

		PositionManager positionManager = (PositionManager) Locator
				.lookup(PositionManager.class);

		DeviceManager deviceManager = (DeviceManager) Locator
				.lookup(DeviceManager.class);
				
		AccountManager accountManager = (AccountManager) Locator
				.lookup(AccountManager.class);

		DriverManager driverManager = (DriverManager) Locator
				.lookup(DriverManager.class);

		UserManager userManager = (UserManager) Locator
				.lookup(UserManager.class);
		
		AlertManager alertManager = (AlertManager) Locator
				.lookup(AlertManager.class);

		AlertProgressManager alertProgressManager = (AlertProgressManager) Locator
				.lookup(AlertProgressManager.class);
		
		AlertHandler alertHandler = (AlertHandler) Locator
				.lookup(AlertHandler.class);
		
		ArchiveHandler archiveHandler = (ArchiveHandler) Locator
				.lookup(ArchiveHandler.class);

		//================================================================================
	    // Traitements du JOB
	    //================================================================================
		
		try {
			Map<Long,Device> devices = new HashMap<Long,Device>();
			Map<Long,Account> Accounts = new HashMap<Long,Account>();
			Map<Long,ArrayList<User>> users = new HashMap<Long,ArrayList<User>>();
			List<Position> listUntreatedPosition = positionManager.getPositionsUnTreated();
			Logger.info("////----------------JOB RECORD-----------------> Count positions : "+listUntreatedPosition.size());
			for(Position position : listUntreatedPosition){
				try{
					Long startTime= System.currentTimeMillis();
					Logger.debug("////----------------JOB INIT-----------------> Start Time : "+startTime);
					
					Long idVehiculeAppareil = position.getIdVehiculeAppareil();
					Device device = null;
					ArrayList<User> listUser = new ArrayList<User>();
					
					//Importation du Device, s'il n'existe pas déjà dans la liste de Device
					if((device = devices.get(idVehiculeAppareil)) == null){
						device = deviceManager.getDeviceByIdDeviceAppareil(position.getIdVehiculeAppareil());
						device.setDriver(driverManager.getDriverByDevice(device));

						//Calibration of this vehicle
						if(device.isAlertEnabled() && device.getTrancheEtalonnage() != null)
							device.setCalibrations(FuelHelper.buildCalibrationList(device.getTrancheEtalonnage()));
							
						//Objet d'alert depuis la base de donnée du device donnée
						Poi poi = alertManager.getPoiByDevice(device);
						GeoZone geozone = alertManager.getZoneByDevice(device);
						List<AgendaPeriode> agendas = alertManager.getAgendaPeriodeByDevice(device);
	
						//Construction de l'objet Shapes et liaison avec Device
						if(geozone != null){ 
		                    GeozoneJSON zoneJson =(GeozoneJSON) JSONHelper.getObject(GeozoneJSON.class, geozone.getShapes());
		                    zoneJson.setZone(device);
	
		                    ZoneAlert zone_alert = new ZoneAlert(geozone.getIdZoneAlerte());
		                    if(device.getDriver() != null)
		                    	zone_alert.setIdChauffeur(device.getDriver().getId());
		                    zone_alert.setIdVehicule(device.getId());
		                    zone_alert.setIdCompte(device.getIdCompte());
		                    zone_alert.setTypeAlerte("ZONE");
		                    zone_alert.setTraiter(false);
		                    device.setAlert_state(zone_alert, TypesAlert.ZONE);
						}else{
							device.removeAlert_state(TypesAlert.ZONE);
						}
						
	                    //Construction de l'objet Poi et liaison avec Device
	                    if(poi != null && poi.getDetails() != null){
	                        POIJSON[] poisJson =(POIJSON[]) JSONHelper.getObject(POIJSON[].class, poi.getDetails());
	                        List<POIElement> pois = new ArrayList<POIElement>();
	                        for(POIJSON poiJson : poisJson){
	                            POIElement poiElement = new POIElement();
	                            poiJson.getPOIElement(poiElement);
	                            pois.add(poiElement);
	                        }
	                        device.setPois(pois);
	
	                        POIAlert poi_alert = new POIAlert(poi.getIdPoiAlerte(),null);
		                    if(device.getDriver() != null)
		                    	poi_alert.setIdChauffeur(device.getDriver().getId());
		                    poi_alert.setIdVehicule(device.getId());
		                    poi_alert.setIdCompte(device.getIdCompte());
		                    poi_alert.setTypeAlerte("POI");
		                    poi_alert.setTraiter(false);
	                        device.setAlert_state(poi_alert, TypesAlert.POI);
	                    }else{
	                    	device.removeAlert_state(TypesAlert.POI);
	                    }
	                    
	                    //Construction de l'objet Agenda et liaison avec Device
	                    if(agendas != null && !agendas.isEmpty()){
		                    List<Planning> plannings = new ArrayList<Planning>();
		                    for(AgendaPeriode agenda : agendas)
		                    	plannings.add(new Planning(DateHelper.getDayOfWeek(agenda.getJour()), agenda.getJour(), DateHelper.getDateFromTime(agenda.getHeureDu()), DateHelper.getDateFromTime(agenda.getHeureAu())));
		                    device.setPlannings(plannings);
	
		                    AgendaAlert agenda_alert = new AgendaAlert(agendas.get(0).getIdHistoric());
		                    if(device.getDriver() != null)
		                    	agenda_alert.setIdChauffeur(device.getDriver().getId());
		                    agenda_alert.setIdVehicule(device.getId());
		                    agenda_alert.setIdCompte(device.getIdCompte());
		                    agenda_alert.setTypeAlerte("TIME");
		                    agenda_alert.setTraiter(false);
		                    device.setAlert_state(agenda_alert, TypesAlert.TIME);
	                    }else{
	                    	device.removeAlert_state(TypesAlert.TIME);
	                    }
	                    
	                    //Construction de l'objet Speed et liaison avec le device
	                    SpeedAlert speedAlert = new SpeedAlert();
	                    if(device.getDriver() != null)
	                    	speedAlert.setIdChauffeur(device.getDriver().getId());
	                    speedAlert.setIdVehicule(device.getId());
	                    speedAlert.setIdCompte(device.getIdCompte());
	                    speedAlert.setTypeAlerte("SPEED");
	                    speedAlert.setVitesseAlerte(device.getAlertSpeed().intValue());
	                    speedAlert.setTraiter(false);
	                    device.setAlert_state(speedAlert, TypesAlert.SPEED);
	                    
	                    //Construction de l'objet Fuel et liaison avec le device
	                    FuelAlert fuelAlert = new FuelAlert();
	                    if(device.getDriver() != null)
	                    	fuelAlert.setIdChauffeur(device.getDriver().getId());
	                    fuelAlert.setIdVehicule(device.getId());
	                    fuelAlert.setIdCompte(device.getIdCompte());
	                    fuelAlert.setTypeAlerte("GASOIL_CONSUMED");
	                    fuelAlert.setTraiter(false);
	                    device.setAlert_state(fuelAlert, TypesAlert.GASOIL_CONSUMED);
	                    
	                    //Construction de l'objet Wire et liaison avec le device
	                    WireAlert wireAlert = new WireAlert();
	                    if(device.getDriver() != null)
	                    	wireAlert.setIdChauffeur(device.getDriver().getId());
	                    wireAlert.setIdVehicule(device.getId());
	                    wireAlert.setIdCompte(device.getIdCompte());
	                    wireAlert.setTypeAlerte("WIRE_OFF");
	                    wireAlert.setTraiter(true);
	                    device.setAlert_state(wireAlert, TypesAlert.WIRE_OFF);
	                    
						//Récupérer les états d'alerte de ce véhicule
						Map<TypesAlert,BaseAlert> listAlertDevice = alertProgressManager.getAlertProgressByDevice(device.getId());
	                    if(!listAlertDevice.isEmpty()){
							for(Entry<TypesAlert,BaseAlert> entry : listAlertDevice.entrySet()){
		                    	BaseAlert baseAlt = entry.getValue();
		                    	
		                    	if(device.getDriver() != null)
			                    	baseAlt.setIdChauffeur(device.getDriver().getId());
		                    	
			                    baseAlt.setIdCompte(device.getIdCompte());
			                    
		                    	if(entry.getKey().equals(TypesAlert.SPEED))
		                    		baseAlt.setVitesseAlerte(device.getAlertSpeed().intValue());
		                    	
		                    	device.setAlert_state(baseAlt, entry.getKey());
		                    }
	                    }
	                    Account account=device.getAccount();
	                    if((account = Accounts.get(device.getId())) == null){
	                    	account=accountManager.lookup(device.getIdCompte());
	                    	device.setAccount(account);
	                    	Accounts.put(device.getId(), account);
	                    	
	                    }
	                    	
	                    //Mettre à jour la liste de Device
						devices.put(idVehiculeAppareil,device);
					}
	
					//Récupérer les utilisateurs du compte, s'il n'existe pas dans la liste d'utilisateur
					Long accountId = device.getIdCompte();
					if((listUser = users.get(accountId)) == null){
						listUser = userManager.getUserByIdAccount(accountId);
						
						//Mettre à jour la liste de Users
						users.put(accountId,listUser);
					}
					
					//Affectation du Device à la position
					position.setDevice(device);
					
					//Alimentation des Enregistrements de Carburant de type FuelRecord pour le traitement des alertes carburant
					if(device.isAlertEnabled()){
						if(position.getDevice().getFuelRecords() == null)
							position.getDevice().setFuelRecords(new ArrayList<FuelRecord>());
						position.getDevice().getFuelRecords().add(new FuelRecord(DateHelper.getDate(position.getDate(), position.getHeure()),position.getCompteur(),FuelHelper.convertFuelValue(device,position.getFuel()),position.getFuelCanbus()));
					}
					
					Long endTime=System.currentTimeMillis();
					Logger.debug("////----------------JOB INIT-----------------> End Time : "+endTime);
					Logger.debug("////----------------JOB INIT-----------------> Execution Time : "+(endTime-startTime));
				}catch(Exception e){
					Logger.error(e.getMessage(), e);
				}
			}
			
			//Analyse des alertes pour chaque position
			for(Position position : listUntreatedPosition){
				try{
					Long startTime= System.currentTimeMillis();
					Logger.debug("////----------------JOB ALERT-----------------> Start Time : "+startTime);
					alertHandler.verifyAlert(position,users.get(position.getDevice().getIdCompte()),alertManager,positionManager,alertProgressManager,deviceManager);
					Long endTime=System.currentTimeMillis();
					Logger.debug("////----------------JOB ALERT-----------------> End Time : "+endTime);
					Logger.debug("////----------------JOB ALERT-----------------> Execution Time : "+(endTime-startTime));
				}catch(Exception e){
					Logger.error(e.getMessage(), e);
				}
			}
			//Changement d'état de position
			for(Position position : listUntreatedPosition) {
				position.setTreated(true);
				positionManager.save(position);
			}
			
			//REDO ne doit pas changer l'état treated jusqu'à la fin d'archivage (problème de redandonce d'alerte) 
			//archiver les positions
			for(Position position : listUntreatedPosition){
				try{
					Long startTime= System.currentTimeMillis();
					Logger.debug("////----------------JOB ARCHIVE-----------------> Start Time : "+startTime);
					String rqt = positionManager.insertToString(position);
					
					Date dateQuota = DateHelper.subtractDays(new Date(),Integer.parseInt(""+position.getDevice().getAccount().getQuota()));
					Boolean archiverRqt = archiveHandler.archivePosition(position.getDevice().getIdCompte(), rqt);
					
					if(archiverRqt){
						if(archiveHandler.deleteOldPositions(dateQuota,position,deviceManager)){
							Device device = position.getDevice();
							device.setLastArchive(new Date());
							deviceManager.save(device);
						}	
					}
					
					Long endTime=System.currentTimeMillis();
					Logger.debug("////----------------JOB ARCHIVE-----------------> End Time : "+endTime);
					Logger.debug("////----------------JOB ARCHIVE-----------------> Execution Time : "+(endTime-startTime));
					
				}catch(Exception e){
					Logger.error(e.getMessage(), e);
				}
			}
			
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}

		Logger.info("////----------------FIN JOB RECORD-----------------////");
	}
	
}
