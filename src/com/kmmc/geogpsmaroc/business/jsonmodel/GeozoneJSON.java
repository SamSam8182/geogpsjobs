/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmmc.geogpsmaroc.business.jsonmodel;

import java.util.ArrayList;
import java.util.List;
import com.kmmc.geogpsmaroc.business.helpers.GeometryHelper;
import com.kmmc.geogpsmaroc.data.model.*;

/**
 *
 * @author dell
 */
public class GeozoneJSON {
    PolygonJSON[] polygon;
    CircleJSON[] circle;
    RectangleJSON[] rectangle;
    PolylineJSON[] polyline;
    
    public List<Shape> getShapes(){
        List<Shape> shapes = new ArrayList<Shape>();
        
        shapes.addAll(getPolygon());
        shapes.addAll(getCircle());
        
        return shapes;
    }
    
    public List<Polygon> getPolygon(){
        List<Polygon> _polygon = new ArrayList<Polygon>();

        if(polygon != null)
        for(PolygonJSON p : polygon){
            String[] lats = p.Lat.split(",");
            String[] lngs = p.Lon.split(",");
            List<LatLng> points = new ArrayList<LatLng>();
            
            for(int i = 0; i< lats.length ; i++)
                points.add(new LatLng(Double.parseDouble(lats[i]),Double.parseDouble(lngs[i])));
            
            _polygon.add(new Polygon(points));
        }
        
        if(polyline != null)
        for(PolylineJSON p : polyline){
            String[] lats = p.Lat.split(",");
            String[] lngs = p.Lon.split(",");
            List<LatLng> points = new ArrayList<LatLng>();
            
            for(int i = 0; i< lats.length ; i++)
                points.add(new LatLng(Double.parseDouble(lats[i]),Double.parseDouble(lngs[i])));
            
            _polygon.add(GeometryHelper.polylineToPolygon(new Polygon(points)));
        }

        if(rectangle != null)
        for(RectangleJSON r : rectangle)
            _polygon.add(GeometryHelper.rectangleToPolygon(r.LatLngNE, r.LatLngSW));
        
        return _polygon;
    }
    
    public List<Circle> getCircle(){
        List<Circle> _circle = new ArrayList<Circle>();
        
        if(circle != null)
        for(CircleJSON c : circle){
            Circle circleElt = new Circle();
            c.getCircle(circleElt);
            _circle.add(circleElt);
        }
        
        return _circle;
    }
    
    public void setZone(Device _device){
        _device.setZones(getShapes());
    }
    
    @Override
    public String toString(){
        String str = "Polygon : ";
        for(PolygonJSON e : polygon)
            str += e.toString();
        str += "\nCircle : ";
        for(CircleJSON c : circle)
            str += c.toString();
        str += "\nRectangle : ";
        for(RectangleJSON r : rectangle)
            str += r.toString();
        str += "\nPolyline : ";
        for(PolylineJSON p : polyline)
            str += p.toString();
        return str;
    }
}

class RectangleJSON {
    String LatLngNE;
    String LatLngSW;

    @Override
    public String toString(){
        return "\nCoords North Est : "+ LatLngNE + "\nCoords South West : " + LatLngSW;
    }
}

class PolylineJSON {
    String Lat;
    String Lon;

    @Override
    public String toString(){
        return "\nLatitude : "+ Lat + "\nLongitude :" + Lon;
    }
}

class PolygonJSON {
    String Lat;
    String Lon;

    @Override
    public String toString(){
        return "\nLatitude : "+ Lat + "\nLongitude :" + Lon;
    }
}