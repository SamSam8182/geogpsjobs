/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmmc.geogpsmaroc.business.jsonmodel;

import com.kmmc.geogpsmaroc.data.model.LatLng;
import com.kmmc.geogpsmaroc.data.model.Circle;

/**
 *
 * @author dell
 */
public class CircleJSON {
    double radius;
    String center;

    public double getRadius() {
        return radius;
    }

    public LatLng getCenter() {
        String[] latlng = center.split(",");
        return new LatLng(Double.parseDouble(latlng[0]),Double.parseDouble(latlng[1]));
    }

    public Circle getCircle(Circle _circle){
    	if(_circle == null)
    		_circle = new Circle();
        _circle.setCenter(getCenter());
        _circle.setRadius(getRadius());
        
        return _circle;
    }
    
    @Override
    public String toString(){
        return "\nRadius : "+ radius + "\nCenter : " + center;
    }
}
