/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmmc.geogpsmaroc.business.jsonmodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.kmmc.geogpsmaroc.data.model.POIElement;
import com.kmmc.geogpsmaroc.data.model.Planning;
import com.kmmc.geogpsmaroc.data.model.TypesPOI;

/**
 *
 * @author dell
 */
public class POIJSON {
    String name;
    CircleJSON circle;
    int vitesseAlerte;
    int alerteExit;
    int typeAlerte;
    PlanningJSON planning;

    public String getName() {
        return name;
    }

    public CircleJSON getCircle() {
        return circle;
    }

    public boolean getVitesseAlerte() {
        return (vitesseAlerte==1);
    }

    public boolean getAlerteExit() {
        return (alerteExit==1);
    }

    public TypesPOI getTypeAlerte() {
        return (typeAlerte==1)?TypesPOI.ATTEINTE:TypesPOI.NON_ATTEINTE;
    }

    public List<Planning> getPlanning(){
        List<Planning> _planning = new ArrayList<Planning>();
        
        if(planning != null){
	        for(Map.Entry<Pair<Integer,String>,FromTo[]> pl : planning.getPlannings().entrySet()){
	            for(FromTo fromto : pl.getValue()){
	                _planning.add(new Planning(pl.getKey().k,pl.getKey().t, fromto.du, fromto.au));
	            }  
	        }
        }
        
        return _planning;
    }
    
    public void getPOIElement(POIElement _poi){
        _poi.setName(getName());
        _poi.setZone(getCircle().getCircle(_poi.getZone()));
        _poi.setAlertOnExit(getAlerteExit());
        _poi.setHaveToStop(getVitesseAlerte());
        _poi.setType(getTypeAlerte());
        _poi.setPlannings(getPlanning());
    }
    
    @Override
    public String toString(){
        return "Name : "+name+" (vitesse : "+vitesseAlerte+", type : "+typeAlerte+", exit : "+alerteExit+")\nCircle : "+circle+"\nPlanning : "+planning;
    }
}
class Pair<T,K>{
    T t;
    K k;
    
    Pair(T t,K k){
        this.t = t;
        this.k = k;
    }
}
class PlanningJSON {
    FromTo[] lundi,mardi,mercredi,jeudi,vendredi,samedi,dimanche;

    public HashMap<Pair<Integer,String>,FromTo[]> getPlannings(){
        HashMap<Pair<Integer,String>,FromTo[]> plannings = new HashMap<Pair<Integer,String>,FromTo[]>();
        
        if(lundi != null){
            plannings.put(new Pair<Integer,String>(2,"Lundi"),lundi);
        }
        if(mardi != null){
            plannings.put(new Pair<Integer,String>(3,"Mardi"),mardi);
        }
        if(mercredi != null){
            plannings.put(new Pair<Integer,String>(4,"Mercredi"),mercredi);
        }
        if(jeudi != null){
            plannings.put(new Pair<Integer,String>(5,"Jeudi"),jeudi);
        }
        if(vendredi != null){
            plannings.put(new Pair<Integer,String>(6,"Vendredi"),vendredi);
        }
        if(samedi != null){
            plannings.put(new Pair<Integer,String>(7,"Samedi"),samedi);
        }
        if(dimanche != null){
            plannings.put(new Pair<Integer,String>(1,"Dimanche"),dimanche);
        }
        
        return plannings;
    }

    @Override
    public String toString(){
        String str = " Lundi : ";
        if(lundi != null)
            for(FromTo da : lundi)
                str+="\n  "+da;
        str += "\n Mardi : ";
        if(mardi != null)
            for(FromTo da : mardi)
                str+="\n  "+da;
        str += "\n Mercredi : ";
        if(mercredi != null)
            for(FromTo da : mercredi)
                str+="\n  "+da;
        str += "\n Jeudi : ";
        if(jeudi != null)
            for(FromTo da : jeudi)
                str+="\n  "+da;
        str += "\n Vendredi : ";
        if(vendredi != null)
            for(FromTo da : vendredi)
                str+="\n  "+da;
        str += "\n Samedi : ";
        if(samedi != null)
            for(FromTo da : samedi)
                str+="\n  "+da;
        str += "\n Dimanche : ";
        if(dimanche != null)
            for(FromTo da : dimanche)
                str+="\n  "+da;
        return str;
    }
}

class FromTo{
    int du;
    int au;

    @Override
    public String toString(){
        return "["+du+"-"+au+"]";
    }
}