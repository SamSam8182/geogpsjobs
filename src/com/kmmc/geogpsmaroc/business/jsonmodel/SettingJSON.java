/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmmc.geogpsmaroc.business.jsonmodel;

import com.kmmc.geogpsmaroc.data.model.User;


/**
 *
 * @author dell
 */
public class SettingJSON {
    EmailSetting email;
    ViewSetting affichage;

    public boolean getEmailAlerte() {
        return email.getAlerte();
    }
    public boolean getEmailPiece() {
        return email.getPiece();
    }

    public boolean getAffichageCaburant() {
        return affichage.getCarburant();
    }
    
    public boolean getAffichageVehicules() {
        return affichage.getVehicules();
    }
    
    public void setSettings(User _user){
        _user.setNotify_alert(getEmailAlerte());
        _user.setNotify_piece(getEmailPiece());
    }
    
    @Override
    public String toString(){
        return "Email : " + email + "\nAffichage : "+affichage;
    }
}

class EmailSetting{
    int alerte;
    int piece;

    public boolean getAlerte() {
        return (alerte==1);
    }

    public boolean getPiece() {
        return (piece==1);
    }
    
    @Override
    public String toString(){
        return "Alerte : " + alerte + "\nPiece : "+piece;
    }
}

class ViewSetting{
    int carburant;
    int vehicules;

    public boolean getCarburant() {
        return (carburant==1);
    }

    public boolean getVehicules() {
        return (vehicules==1);
    }
    
    @Override
    public String toString(){
        return "Carburant : " + carburant + "\nVehicules : "+vehicules;
    }
}