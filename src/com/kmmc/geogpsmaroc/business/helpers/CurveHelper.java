package com.kmmc.geogpsmaroc.business.helpers;

import java.util.ArrayList;
import java.util.List;

import com.kmmc.geogpsmaroc.data.model.FuelRecord;

/*
 * CurveHelper est un helper qui correspond aux calcule d'une courbe, du lissage de celle ci, et des suppression des ses erreurs.
 * Cette class est principalement utilis� par l'analyse des alertes carburant
 */
public class CurveHelper {

    public final static double DISTANCE_MIN_PIC_TOLERATED = 0.8;
    public final static int NUMBER_POINTS_LITTLE_PIC = 5;
    public final static int NUMBER_POINTS_SLEEK = 5;
        
	public static boolean deleteFakePic(List<FuelRecord> aFuel_records, double fuel_level_tolerated) {
            for (int i = 1; i < aFuel_records.size() - 1; i++) {
                if ((aFuel_records.get(i).getFuelLevel() > aFuel_records.get(i - 1).getFuelLevel()) && (aFuel_records.get(i - 1).getFuelLevel() >= aFuel_records.get(i + 1).getFuelLevel())) {
                    aFuel_records.get(i).setFuelLevel(aFuel_records.get(i - 1).getFuelLevel());
                } else if ((aFuel_records.get(i).getFuelLevel() > aFuel_records.get(i - 1).getFuelLevel()) && (aFuel_records.get(i).getFuelLevel() > aFuel_records.get(i + 1).getFuelLevel()) && (aFuel_records.get(i - 1).getFuelLevel() < aFuel_records.get(i + 1).getFuelLevel())) {
                    aFuel_records.get(i).setFuelLevel(aFuel_records.get(i + 1).getFuelLevel());
                } else if ((aFuel_records.get(i).getFuelLevel() < aFuel_records.get(i + 1).getFuelLevel()) && (aFuel_records.get(i - 1).getFuelLevel() >= aFuel_records.get(i + 1).getFuelLevel())) {
                    aFuel_records.get(i).setFuelLevel(aFuel_records.get(i + 1).getFuelLevel());
                } else if ((aFuel_records.get(i).getFuelLevel() < aFuel_records.get(i - 1).getFuelLevel()) && (aFuel_records.get(i).getFuelLevel() < aFuel_records.get(i + 1).getFuelLevel()) && (aFuel_records.get(i - 1).getFuelLevel() < aFuel_records.get(i + 1).getFuelLevel())) {
                    aFuel_records.get(i).setFuelLevel(aFuel_records.get(i - 1).getFuelLevel());
                }
            }
            return deleteLittleFakePic(aFuel_records, 0, aFuel_records.size(), fuel_level_tolerated);
	}

	public static boolean deleteLittleFakePic(List<FuelRecord> aFuel_records, int aIdx_from, int aIdx_to, double fuel_level_tolerated) {
            int toReturn = 0;
            ArrayList<Double> ecarts = new ArrayList<Double>();
            for (int i = aIdx_from; i < aIdx_to - 1; i++) {
                ecarts.add(Math.abs(aFuel_records.get(i).getFuelLevel() - aFuel_records.get(i + 1).getFuelLevel()));
            }
            for (int i = 0; i < ecarts.size(); i++) {
                if (ecarts.get(i) > 1) {
                    int debut = i;
                    int fin = searchEndOfPic(aFuel_records, i, aIdx_to, fuel_level_tolerated);
                    if ((fin >= 0) && (fin < aIdx_to - 1)) {
                        for (int j = debut; j < fin; j++) {
                            aFuel_records.get(j).setFuelLevel(aFuel_records.get(debut).getFuelLevel());
                        }
                        i = fin-1;
                    } else {
                        if (fin == -2) {
                            toReturn = fin;
                        }
                    }
                }
            }
            if (toReturn == -2) {
                return false;
            } else {
                return true;
            }
	}

	public static int searchEndOfPic(List<FuelRecord> aFuel_records, int aIdx, int aIdx_fin, double fuel_level_tolerated) {
            FuelRecord D = aFuel_records.get(aIdx);
            boolean sens = (aFuel_records.get(aIdx).getFuelLevel() < aFuel_records.get(aIdx + 1).getFuelLevel());
            aIdx++;
            int toReturn = -1;
            double diff = -1;
            boolean search = false;
            if (sens) {
                while ((aIdx < aIdx_fin - 1) && ((aFuel_records.get(aIdx).getCounter() - D.getCounter())<=DISTANCE_MIN_PIC_TOLERATED)) {
                    if ((!((aFuel_records.get(aIdx).getFuelLevel() < aFuel_records.get(aIdx + 1).getFuelLevel()) ^ sens)) || search){
                        if ((Math.abs(aFuel_records.get(aIdx).getFuelLevel() - D.getFuelLevel()) <= fuel_level_tolerated) && search) {
                            if (toReturn == -1 || (diff != -1 && diff > (Math.abs(aFuel_records.get(aIdx).getFuelLevel() - D.getFuelLevel())))) {
                                toReturn = aIdx;
                                diff = Math.abs(aFuel_records.get(aIdx).getFuelLevel() - D.getFuelLevel());
                            }
                        }
                        aIdx++;
                    } else {
                        if (sens) {
                            sens = false;
                            search = true;
                        }
                    }
                }
            } else {
                while ((aIdx < aIdx_fin - 1) && ((aFuel_records.get(aIdx).getCounter() - D.getCounter())<=DISTANCE_MIN_PIC_TOLERATED))  {
                    if ((!((aFuel_records.get(aIdx).getFuelLevel() > aFuel_records.get(aIdx + 1).getFuelLevel()) ^ sens)) || search) {
                        if((Math.abs(aFuel_records.get(aIdx).getFuelLevel() - D.getFuelLevel()) <= fuel_level_tolerated) && search) {
                            if (toReturn == -1 || (diff != -1 && diff > (Math.abs(aFuel_records.get(aIdx).getFuelLevel() - D.getFuelLevel())))) {
                                 toReturn = aIdx;
                                 diff = Math.abs(aFuel_records.get(aIdx).getFuelLevel() - D.getFuelLevel());
                             }
                         }
                         aIdx++;
                    } else {
                        if (!sens) {
                            sens = true;
                            search = true;
                        }
                    }
                }
            }
            if ((toReturn == -1) && (aIdx >= aIdx_fin - 1)) {
                toReturn = -2;
            }
            return toReturn;
	}

	public static void traitePoints(List<FuelRecord> aFuel_records, int aTolerance) {
			
			ArrayList<Integer> result = new ArrayList<Integer>();
            ArrayList<Double> ecarts = new ArrayList<Double>();
            for (int i = 0; i < aFuel_records.size() - 1; i++) {
                ecarts.add(Math.abs(aFuel_records.get(i).getFuelLevel() - aFuel_records.get(i + 1).getFuelLevel()));
                result.add(0);
            }
            result.add(0);
            for (int i = 0; i < ecarts.size(); i++) {
                if (ecarts.get(i) > aTolerance) {
                    int idx = analysePic(aFuel_records, i, NUMBER_POINTS_LITTLE_PIC, ecarts.size());
                    if (idx == -1) {
                        int j = i + 1;
                        if ((j < NUMBER_POINTS_LITTLE_PIC + i) && (j < ecarts.size())) {
                            result.set(j, 1);
                        }
                    } else {
                        i = idx;
                    }
                }
            }
            sleekCurve(aFuel_records, result);
	}

	public static int analysePic(List<FuelRecord> aFuel_records, int aIdx, int aNbr_pt_little_pic, int aSize) {
            double fst = aFuel_records.get(aIdx).getFuelLevel();
            double scd = aFuel_records.get(aIdx + 1).getFuelLevel();
            for (int j = aIdx + 2; (j < aIdx + aNbr_pt_little_pic) && (j < aSize); j++) {
                if (Math.abs(aFuel_records.get(j).getFuelLevel() - fst) < Math.abs(aFuel_records.get(j).getFuelLevel() - scd)) {
                    return j;
                }
            }
            return -1;
	}

	public static void sleekCurve(List<FuelRecord> aFuel_records, List<Integer> aDiagnostic) {
            for (int aIdx = 1; aIdx < aDiagnostic.size(); aIdx++) {
                double sum = aFuel_records.get(aIdx).getFuelLevel();
                int div = 1;
                int min = (aIdx >= NUMBER_POINTS_SLEEK) ? aIdx - NUMBER_POINTS_SLEEK : 0;
                int max = (aIdx + NUMBER_POINTS_SLEEK < aDiagnostic.size()) ? NUMBER_POINTS_SLEEK + aIdx : aDiagnostic.size();

                for (int i = aIdx - 1; i >= min; i--) {
                    if (aDiagnostic.get(i) != 1) {
                        div++;
                        sum += aFuel_records.get(i).getFuelLevel();
                    } else {
                        i = min - 1;
                    }
                }
                for (int i = aIdx + 1; i < max; i++) {
                    if (aDiagnostic.get(i) != 1) {
                        div++;
                        sum += aFuel_records.get(i).getFuelLevel();
                    } else {
                        i = max;
                    }
                }
                aFuel_records.get(aIdx).setFuelLevel(sum / div);
            }
	}
}