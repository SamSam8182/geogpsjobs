package com.kmmc.geogpsmaroc.business.helpers;

import com.google.gson.Gson;

/*
 * JSONHelper, est un helper qui retourne l'objet correspondant à un json donnée. Ce Helper utilise une library de google 'gson'.
 */
public class JSONHelper<T> {

	public static <T> Object getObject(Class<T> aClass,String aJson) {
            try {
                Gson gson = new Gson();
                return gson.fromJson(aJson, aClass);
            } catch (IllegalArgumentException ex) {
                return null;
            }
	}
}