package com.kmmc.geogpsmaroc.business.helpers;

import java.util.ArrayList;
import java.util.List;

import com.kmmc.geogpsmaroc.data.model.CalibrationElement;
import com.kmmc.geogpsmaroc.data.model.Device;

/*
 * FuelHelper, permet d'effectuer des calcules relative au carburant et l'�talonnage de v�hicules. 
 * Cette class est principalement utilis� par l'analyse des alertes carburant.
 */
public class FuelHelper {

	public static double calculateFuelValue(List<CalibrationElement> aCalibration_slice, double aFuel) {
            for( CalibrationElement slice : aCalibration_slice){
                if(slice.contains(aFuel)){
                    return (Math.abs(slice.getMaxVolt() - ((int) aFuel)) * slice.getRate()) + slice.getLiterMax();
                }
            }
            return -1;
	}

	public static List<CalibrationElement> buildCalibrationList(String aCalibration_slice) {
            List<CalibrationElement> slices = new ArrayList<CalibrationElement>();
            aCalibration_slice = aCalibration_slice.substring(1, aCalibration_slice.length() - 1);
            aCalibration_slice = aCalibration_slice.replaceAll("\"", "");
            String[] json_split = aCalibration_slice.split(",");
            
            for (int i = 0; i < json_split.length; i++) {
                String[] slice_str = json_split[i].split(":");
                String[] liter_slice = slice_str[0].split(";");
                String[] min_max_volt = liter_slice[1].split("-");
                
                CalibrationElement slice = new CalibrationElement();
                slice.setMinMaxVolt(min_max_volt);
                slice.setLiter_max(liter_slice[0]);
                slice.setRate(Double.valueOf(slice_str[1]));
                slices.add(slice);
            }
            return slices;
	}

	public static Double convertFuelValue(Device device, Double fuel) {
		if(!device.isCanbus() && ((device.getCalibrationVolt() != 0) || (device.getTrancheEtalonnage() != null))){
			if(device.getTrancheEtalonnage() != null)
			{
				fuel = calculateFuelValue(device.getCalibrations(), fuel);
				if(fuel == -1 && !device.getFuelRecords().isEmpty())
					fuel = device.getFuelRecords().get(device.getFuelRecords().size() - 1).getFuelLevel(); 
			}else{
				double taux = ((double) device.getCalibrationLiter())/((double) device.getCalibrationVolt());
				fuel = ((double) (device.getVoltageNiveauMin() - fuel)) * taux;
			}
		}else{
			if(device.isCanbus() && !device.getVehicleType().equalsIgnoreCase("voiture"))
				return (fuel*device.getFuelCapacity())/250;
		}
		return fuel;
	}
}