/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmmc.geogpsmaroc.business.helpers;

import java.util.ArrayList;
import com.kmmc.geogpsmaroc.data.model.*;

/*
 * Cette class permet les manipulations qui concerne l'objet 'POIElement'
 */
public class POIHelper {
    
    public static ArrayList<POIElement> getPOIElement(Device device, TypesPOI type,int day_idx){
        ArrayList<POIElement> plannings = new ArrayList<POIElement>();
        for(POIElement pe : device.getPois()){
            if(pe.getType().equals(type)){
                if(pe.getType().equals(TypesPOI.ATTEINTE) || !PlanningHelper.getPlannings(pe.getPlannings(), day_idx).isEmpty())
                    plannings.add(pe);
            }
        }
        return plannings;
    }
}
