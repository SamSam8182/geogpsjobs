package com.kmmc.geogpsmaroc.business.helpers;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import com.kmmc.geogpsmaroc.core.services.log.Logger;

/*
 * Cette class fournis toutes les méthodes utiles pour les conversions de dates et heures, les comparaisons et les modifications de celles-ci.
 */
public class DateHelper {
    public static Date getDate(String date, String heure){
        String[] dateSplit = date.split("-");
        String[] heureSplit = heure.split(":");
        
        Calendar cal = new GregorianCalendar();
        cal.set(Integer.parseInt(dateSplit[0]), Integer.parseInt(dateSplit[1])-1,
                    Integer.parseInt(dateSplit[2]), Integer.parseInt(heureSplit[0]),
                    Integer.parseInt(heureSplit[1]), Integer.parseInt(heureSplit[2]));
            
        return cal.getTime();
    }
    
    public static Date getDate(Date date, Date heure){
    	return getDate(getStringDate(date),getStringTime(heure));
    }

    public static String getDayOfWeek(int idx){
        switch(idx){
            case 1 :
                return "Dimanche";
            case 2 :
                return "Lundi";
            case 3 :
                return "Mardi";
            case 4 :
                return "Mercredi";
            case 5 :
                return "Jeudi";
            case 6 :
                return "Vendredi";
            case 7 :
                return "Samedi";
            default :
                return "";
        }
    }
    
	public static String getDay(Date date) {
            Calendar c = new GregorianCalendar();
            c.setTime(date);
            return c.getDisplayName(Calendar.DAY_OF_WEEK,Calendar.LONG, Locale.FRANCE);
	}

    //Si la date d1 est inférieur à d2, cela retournera une valeur négative, sinon l'inverse
    public static int compareDates(Date d1, Date d2){
        Calendar cal1 = new GregorianCalendar();
        cal1.setTime(d1);
        cal1.set(Calendar.HOUR, 0);
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.SECOND, 0);
        
        Calendar cal2 = new GregorianCalendar();
        cal2.setTime(d2);
        cal2.set(Calendar.HOUR, 0);
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.SECOND, 0);
        
        return cal1.compareTo(cal2);
    }
    public static Date subtractDays(Date d, int days)
	{
		days*=-1;
	    Calendar c = Calendar.getInstance();
	    c.setTime(d);
	    c.add(Calendar.DATE, days);
	    d.setTime( c.getTime().getTime() );
	    return d;
	}

    public static Date addDays(Date d, int days)
	{
	    Calendar c = Calendar.getInstance();
	    c.setTime(d);
	    c.add(Calendar.DATE, days);
	    d.setTime( c.getTime().getTime() );
	    return d;
	}
    //Si l'heure time est inférieur à hour, cela retournera une valeur négative, sinon l'inverse
	public static int compareHour(String time,int hour){
            int currentH = Integer.parseInt(time.split(":")[0]);
            return (currentH - hour);
	}
    
    public static Date getDate(int hour){
        Calendar cal = new GregorianCalendar();
        
        cal.set(Calendar.HOUR_OF_DAY,hour);
        cal.set(Calendar.MINUTE,0);
        cal.set(Calendar.SECOND,0);

        return cal.getTime();
    }

    //Si l'heure d1 est inférieur à d2, cela retournera une valeur négative, sinon l'inverse
    public static int compareTimes(Date d1, Date d2){
        int t1;
        int t2;

        t1 = (int) (d1.getTime() % (24*60*60*1000L));
        t2 = (int) (d2.getTime() % (24*60*60*1000L));
        
        return (t1 - t2);
    }
    
    public static int getDateIndex(Date date){
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK);
    }
    
    public static int getDateHour(Date date){
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(Calendar.HOUR_OF_DAY);
    }

    //Retourne l'heure et la date (String) de la date donnée
    public static String getStringDateTime(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(date);
    }

    //Retourne la date (String) de la date donnée
    public static String getStringDate(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }
    
    //Retourne l'heure de la date donnée
    public static String getStringTime(Date date){
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(date);
    }
    
    //Retourne une liste de date entre les deux dates données
    public static List<Date> getDatesBetween(Date date_begin, Date date_end){
    	List<Date> dates = new ArrayList<Date>();
        
        Calendar cal_begin = Calendar.getInstance();
        Calendar cal_end = Calendar.getInstance();
        
        cal_begin.setTime(date_begin);
        cal_end.setTime(date_end);
       
        while (!getStringDate(cal_begin.getTime()).equals(getStringDate(cal_end.getTime()))) {
            cal_begin.add(Calendar.DATE, 1);
            dates.add(cal_begin.getTime());
        }
        
        return dates;
    }
    
    public static Date addHours(Date time,int hour) {
        return new Date(time.getTime()+(hour*60*60*1000));
    }
    
    public static Date getDateFromTime(Time time){
        return new Date(time.getTime());
    }

    public static Date getDateFromDateTime(Date date){
    	try{
	        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	        return dateFormat.parse(dateFormat.format(date));
    	}catch(ParseException e){
    		Logger.error("Parse Date Failed", e);
    		return null;
    	}
    }
    
    //Si la date envoyée est inférieur à la date de cette instant, cela retournera une valeur négative, sinon l'inverse
	public static int compareDatesWithNow(Date date_alerte) {
        Calendar cal1 = new GregorianCalendar();
        cal1.setTime(date_alerte);
        
        Calendar cal2 = new GregorianCalendar();
        cal2.setTime(new Date());
        
        return cal1.compareTo(cal2);
	}
}