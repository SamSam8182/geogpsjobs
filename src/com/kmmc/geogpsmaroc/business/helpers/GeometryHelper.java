package com.kmmc.geogpsmaroc.business.helpers;

import java.util.ArrayList;
import java.util.List;
import com.kmmc.geogpsmaroc.data.model.*;
import com.kmmc.geogpsmaroc.business.helpers.DistanceCalculator;

/*
 * GeometryHelper permet toute les conversions de formes, et la v�rification d'appartenance d'un point � une forme geom�trique.
 * Ce Helper est principalement utilis� pour les alertes Geozone et Points d'inter�t.
 */
public class GeometryHelper {

    private static final int INDENT = 15;

    public static Polygon rectangleToPolygon(String lat_lng_ne,String lat_lng_sw){
        List<LatLng> points = new ArrayList<LatLng>();
        String[] NE = lat_lng_ne.split(",");
        String[] SW = lat_lng_sw.split(",");
        
        points.add(new LatLng(Double.parseDouble(NE[0]),Double.parseDouble(NE[1])));
        points.add(new LatLng(Double.parseDouble(SW[0]),Double.parseDouble(NE[1])));
        points.add(new LatLng(Double.parseDouble(SW[0]),Double.parseDouble(SW[1])));
        points.add(new LatLng(Double.parseDouble(NE[0]),Double.parseDouble(SW[1])));
        
        return new Polygon(points);
    }
    
	public static Polygon polylineToPolygon(Polygon aShape) {
            List<LatLng> right_line = new ArrayList<LatLng>();
            List<LatLng> left_line = new ArrayList<LatLng>();
            
            List<LatLng> points = aShape.getPoints();
            for(int i=1;i<aShape.size();i++){
                rightLine(right_line, points.get(i-1), points.get(i));
                leftLine(left_line, points.get(i-1),points.get(i));
            }
            
            List<LatLng> polygon_points = right_line;
            for(int i = left_line.size()-1; i >=0 ; i--)
                    polygon_points.add(left_line.get(i));
 
            Polygon polygon = new Polygon(polygon_points);
            
            return polygon;
	}

	private static LatLng intersectLineLine(LatLng a1,LatLng a2,LatLng b1,LatLng b2) {
            LatLng result = new LatLng(0,0);

            double ua_t = (b2.getLongitude() - b1.getLongitude()) * (a1.getLatitude() - b1.getLatitude()) - (b2.getLatitude() - b1.getLatitude()) * (a1.getLongitude() - b1.getLongitude());
            double ub_t = (a2.getLongitude() - a1.getLongitude()) * (a1.getLatitude() - b1.getLatitude()) - (a2.getLatitude() - a1.getLatitude()) * (a1.getLongitude() - b1.getLongitude());
            double u_b  = (b2.getLatitude() - b1.getLatitude()) * (a2.getLongitude() - a1.getLongitude()) - (b2.getLongitude() - b1.getLongitude()) * (a2.getLatitude() - a1.getLatitude());

            if ( u_b != 0 ) {
                double ua = ua_t / u_b;
                double ub = ub_t / u_b;

                if ( 0 <= ua && ua <= 1 && 0 <= ub && ub <= 1 ) {
                    return new LatLng(a1.getLatitude() + ua * (a2.getLatitude() - a1.getLatitude()),a1.getLongitude() + ua * (a2.getLongitude() - a1.getLongitude()));
                }
            }
            return result;	
	}

    private static List<LatLng> rightLine(List<LatLng> right_line, LatLng point1,LatLng point2){
        double dx = (point2.getLongitude() - point1.getLongitude());
        double dy = (point2.getLatitude() - point1.getLatitude());
        double length = DistanceCalculator.distance(point1,point2);
        
        double newX1 = point1.getLongitude() + INDENT*(dy/length);
        double newY1 = point1.getLatitude() - INDENT*(dx/length);
        double newX2 = point2.getLongitude() + INDENT*(dy/length);
        double newY2 = point2.getLatitude() - INDENT*(dx/length);

        LatLng pos1 = new LatLng(newY1,newX1);
        right_line.add(pos1);
        
        LatLng pos2 = new LatLng(newY2, newX2);
        right_line.add(pos2);
        
        if(right_line.size()>2)
        {
            LatLng intersect = intersectLineLine(right_line.get(right_line.size()-1),right_line.get(right_line.size()-2),right_line.get(right_line.size()-3),right_line.get(right_line.size()-4));
            if(!intersect.isNull())
            {
                    right_line.remove(right_line.size()-3);
                    right_line.set(right_line.size()-2,intersect);
            }
        }
        
        return right_line;
    }
    
    private static List<LatLng> leftLine(List<LatLng> left_line, LatLng point1,LatLng point2){
        double dx = (point2.getLongitude() - point1.getLongitude());
        double dy = (point2.getLatitude() - point1.getLatitude());
        double length = DistanceCalculator.distance(point1,point2);
        
        double newX1 = point1.getLongitude() - INDENT*(dy/length);
        double newY1 = point1.getLatitude() + INDENT*(dx/length);
        double newX2 = point2.getLongitude() - INDENT*(dy/length);
        double newY2 = point2.getLatitude() + INDENT*(dx/length);

        LatLng pos1 = new LatLng(newY1, newX1);
        left_line.add(pos1);
        
        LatLng pos2 = new LatLng(newY2, newX2);
        left_line.add(pos2);
        
        if(left_line.size()>2)
        {
            LatLng intersect = intersectLineLine(left_line.get(left_line.size()-1),left_line.get(left_line.size()-2),left_line.get(left_line.size()-3),left_line.get(left_line.size()-4));
            if(!intersect.isNull())
            {
                    left_line.remove(left_line.size()-3);
                    left_line.set(left_line.size()-2,intersect);
            }
        }
        
        return left_line;
    }
    
	public static boolean contains(List<Shape> aShape, LatLng aPoint) {
            for(Shape shape : aShape){
                if(contains(shape,aPoint))
                    return true;
            }
            return false;
	}
    
    public static boolean contains(Shape aShape, LatLng aPoint){
            if(aShape instanceof Circle)
                return contains((Circle)aShape,aPoint);
            if(aShape instanceof Polygon)
                return contains((Polygon)aShape,aPoint);
            return false;
        }
        
	public static boolean contains(Circle aShape, LatLng aPoint) {
            double distance = DistanceCalculator.distance(aPoint, aShape.getCenter());
            return (distance < aShape.getRadius()) ? true : false;
	}

	public static boolean contains(Polygon aShape, LatLng aPoint) {
            if(aShape.size() < 3 )
                return true;
            else{
                boolean contain = false;
                for( int i = 0, j = aShape.size()-1; i < aShape.size(); j = i++ ){
                    LatLng p1 = aShape.getPoints().get(i);
                    LatLng p2 = aShape.getPoints().get(j);
                    if((p1.getY() < aPoint.getY() && p2.getY() >= aPoint.getY()) || (p2.getY() < aPoint.getY() && p1.getY() >= aPoint.getY()))
                        if(p1.getX() + (aPoint.getY() - p1.getY() ) / (p2.getY() - p1.getY()) * (p2.getX() - p1.getX()) < aPoint.getX())
                            contain = !contain;
                }
                return contain;
            }
	}
}