package com.kmmc.geogpsmaroc.business.helpers;

import com.kmmc.geogpsmaroc.data.model.AgendaAlert;
import com.kmmc.geogpsmaroc.data.model.BaseAlert;
import com.kmmc.geogpsmaroc.data.model.Device;
import com.kmmc.geogpsmaroc.data.model.FuelAlert;
import com.kmmc.geogpsmaroc.data.model.Piece;
import com.kmmc.geogpsmaroc.data.model.SpeedAlert;
import com.kmmc.geogpsmaroc.data.model.TypesAlert;
import com.kmmc.geogpsmaroc.data.model.TypesPiece;
import com.kmmc.geogpsmaroc.data.model.User;

/*
 * Ce Helper, contient aussi des templates de contenu de mail et d'objet, auxquel en change des valeurs clés, pour constituer les mails qu'on souhaite.
 */
public class MailHelper {

        private static final String SUBJECT_SPEED = "Alerte Vitesse";
        private static final String SUBJECT_POI = "Alerte Points d'intérêt";
        private static final String SUBJECT_ZONE = "Alerte Geozone";
        private static final String SUBJECT_TIME = "Alerte Temps";
        private static final String SUBJECT_GASOIL = "Alerte Carburant";
        
        private static final String SUBJECT_ASSURANCE = "Alerte Assurance";
        private static final String SUBJECT_AUTORISATION_CIRCULATION = "Alerte Autorisation de circulation";
        private static final String SUBJECT_CARNET_CIRCULATION = "Alerte Carnet de circulation";
        private static final String SUBJECT_CARNET_METROLOGIQUE = "Alerte Carnet métrologique";
        private static final String SUBJECT_VIDANGE = "Alerte Vidange";
        private static final String SUBJECT_VISITE_TECHNIQUE = "Alerte Visite technique";
        
        private static final String TITLE_ASSURANCE = "Assurance";
        private static final String TITLE_AUTORISATION_CIRCULATION = "Autorisation de circulation";
        private static final String TITLE_CARNET_CIRCULATION = "Carnet de circulation";
        private static final String TITLE_CARNET_METROLOGIQUE = "Carnet métrologique";
        private static final String TITLE_VIDANGE = "Vidange";
        private static final String TITLE_VISITE_TECHNIQUE = "Visite technique";
        
        private static final String TITLE_SPEED = "vitesse";
        private static final String TITLE_POI = "points d'intérêt";
        private static final String TITLE_ZONE = "geozone";
        private static final String TITLE_TIME = "temps";
        private static final String TITLE_GASOIL = "carburant";
        
        private static final String URL_ASSURANCE = "assurance";
        private static final String URL_AUTORISATION_CIRCULATION = "autorisation_circulation";
        private static final String URL_CARNET_CIRCULATION = "carnet_circulation";
        private static final String URL_CARNET_METROLOGIQUE = "carnet_metrologie";
        private static final String URL_VIDANGE = "vidange";
        private static final String URL_VISITE_TECHNIQUE = "visite_technique";
       
        private static final String URL_GASOIL = "consommation";
        private static final String URL_SPEED = "vitesse";
        private static final String URL_POI = "poi";
        private static final String URL_ZONE = "geozone";
        private static final String URL_TIME = "temps";
        
        /** Mail prédéfinie des alertes pièces et véhicule, selon les paramétres donnée **/
        
        public static String getSubjectPiece(TypesPiece aType) {
            switch(aType){
                case ASSURANCE:
                    return SUBJECT_ASSURANCE;
                case AUTORISATION_CIRCULATION:
                    return SUBJECT_AUTORISATION_CIRCULATION;
                case CARNET_CIRCULATION:
                    return SUBJECT_CARNET_CIRCULATION;
                case CARNET_METROLOGIQUE:
                    return SUBJECT_CARNET_METROLOGIQUE;
                case VIDANGE: 
                    return SUBJECT_VIDANGE;
                case VISITE_TECHNIQUE:
                    return SUBJECT_VISITE_TECHNIQUE;
                default:
                    return "";
            }
        }

        public static String getMsgPiece(TypesPiece aType, Piece aPiece, User aUser) {
            String msg = "Bonjour "+aUser.getNom()+"\nVous avez reçus une alerte ";
            msg += getTitlePiece(aType);
            msg += " :\nVehicule : "+aPiece.getDevice().getRegistration()+" ("+aPiece.getDevice().getInternCode()+")\n";
            
            msg += getInfosPiece(aType,aPiece);
            msg += "\n";
            msg += getURLPiece(aType,aPiece);

            return msg;
        }

        public static String getMsgAlert(Device aDevice, TypesAlert type, BaseAlert alert, User aUser){
            String msg = "Bonjour "+ aUser.getNom() +"\nVous avez reçus une alerte ";
            msg += getTitleAlert(type);
            msg += " :\nVehicule : "+aDevice.getRegistration()+" ("+aDevice.getInternCode()+")\n";
            if(aDevice.getDriver() != null){
	            msg += "Chauffeur : "+aDevice.getDriver().getNom()+" "+aDevice.getDriver().getPrenom()+" ("+aDevice.getDriver().getIdChauffeur()+")\n";
	            if(aDevice.getDriver().getTel() != null && !aDevice.getDriver().getTel().equals(""))
	                    msg += "Tel du chauffeur : "+aDevice.getDriver().getTel();
            }
            msg += getPeriodAlert(type,alert,aDevice);
            msg += "\n";
            msg += getURLAlert(type,alert,aDevice);
            
            return msg;
        }
        
        public static String getSubjectAlert(TypesAlert type){
            switch(type){
                case ZONE:
                    return SUBJECT_ZONE;
                case POI:
                    return SUBJECT_POI;
                case TIME:
                    return SUBJECT_TIME;
                case SPEED:
                    return SUBJECT_SPEED;
                case GASOIL_CONSUMED: case GASOIL_ADDED:
                    return SUBJECT_GASOIL;
                default:
                    return "";
            }
        }
        
        /********* Méthodes retourne des bout de mail selon les paramétres données *******/
        
        private static String getTitlePiece(TypesPiece type){
            switch(type){
                case ASSURANCE:
                    return TITLE_ASSURANCE;
                case AUTORISATION_CIRCULATION:
                    return TITLE_AUTORISATION_CIRCULATION;
                case CARNET_CIRCULATION:
                    return TITLE_CARNET_CIRCULATION;
                case CARNET_METROLOGIQUE:
                    return TITLE_CARNET_METROLOGIQUE;
                case VIDANGE: 
                    return TITLE_VIDANGE;
                case VISITE_TECHNIQUE:
                    return TITLE_VISITE_TECHNIQUE;
                default:
                    return "";
            }
        }
        
        private static String getURLPiece(TypesPiece type, Piece piece){
            String begin = "Pour consulter l'alerte dans l'application GeoGpsMaroc, veuillez suivre ce lien :\n46.105.45.74/geogpsmaroc/";
            switch(type){
                case ASSURANCE:
                    return begin+URL_ASSURANCE+"/details/"+piece.getId();
                case AUTORISATION_CIRCULATION:
                    return begin+URL_AUTORISATION_CIRCULATION+"/details/"+piece.getId();
                case CARNET_CIRCULATION:
                    return begin+URL_CARNET_CIRCULATION+"/details/"+piece.getId();
                case CARNET_METROLOGIQUE:
                    return begin+URL_CARNET_METROLOGIQUE+"/details/"+piece.getId();
                case VIDANGE:
                    return begin+URL_VIDANGE+"/details/"+piece.getId();
                case VISITE_TECHNIQUE:
                    return begin+URL_VISITE_TECHNIQUE+"/details/"+piece.getId();
                default:
                    return "";
            }
        }
        
        private static String getInfosPiece(TypesPiece type, Piece piece){
            switch(type){
                case ASSURANCE:case AUTORISATION_CIRCULATION:case CARNET_CIRCULATION:case CARNET_METROLOGIQUE:case VISITE_TECHNIQUE:
                    return "La date d'alerte est : "+piece.getDateAlerte();
                case VIDANGE:
                    return "Le compteur d'alerte est : "+piece.getKmAlert()+" Km";
                default:
                    return "";
            }
        }
        
        private static String getTitleAlert(TypesAlert type){
            switch(type){
                case ZONE:
                    return TITLE_ZONE;
                case POI:
                    return TITLE_POI;
                case TIME:
                    return TITLE_TIME;
                case SPEED:
                    return TITLE_SPEED;
                case GASOIL_CONSUMED: case GASOIL_ADDED:
                    return TITLE_GASOIL;
                default:
                    return "";
            }
        }
        
        private static String getURLAlert(TypesAlert type, BaseAlert alert, Device device){
            String begin = "Pour consulter l'alerte dans l'application GeoGpsMaroc, veuillez suivre ce lien :\n87.98.146.234/geogpsmaroc/alerte/";
            switch(type){
                case ZONE:
                    return begin+URL_ZONE+"/"+device.getId()+"/"+DateHelper.getStringDate(alert.getDatetimeFrom());
                case POI:
                    return begin+URL_POI+"/"+device.getId()+"/"+DateHelper.getStringDate(alert.getDatetimeFrom());
                case TIME:
                    return begin+URL_TIME+"/"+device.getId()+"/"+DateHelper.getStringDate(alert.getDatetimeFrom());
                case SPEED:
                    return begin+URL_SPEED+"/"+device.getId()+"/"+DateHelper.getStringDate(alert.getDatetimeFrom());
                case GASOIL_CONSUMED:
                    return begin+URL_GASOIL+"/"+device.getId()+"/"+DateHelper.getStringDate(alert.getDatetimeFrom());
                case GASOIL_ADDED:
                    return "";
                default:
                    return "";
            }
        }
        
        private static String getPeriodAlert(TypesAlert type, BaseAlert alert, Device device){
            String period_txt = "";
            switch(type){
                case ZONE:
                    period_txt += "Du : "+DateHelper.getStringDateTime(alert.getDatetimeFrom())+"\n";
                    period_txt += "Au : "+DateHelper.getStringDateTime(alert.getDatetimeTo())+"\n";
                    break;
                case POI:
                    period_txt += "Le : "+DateHelper.getStringDate(alert.getDatetimeFrom())+"\n";
                    period_txt += "à : "+DateHelper.getStringTime(alert.getDatetimeFrom())+"\n";
                    period_txt += "Description : "+alert.getRemarque()+"\n";
                    break;
                case TIME:
                    period_txt += "Du : "+DateHelper.getStringDateTime(alert.getDatetimeFrom())+"\n";
                    period_txt += "Au : "+DateHelper.getStringDateTime(alert.getDatetimeTo())+"\n";
                    period_txt += "Distance : "+String.format("%.2f",((FuelAlert)alert).getDistance())+"\n";
                    break;
                case SPEED:
                    period_txt += "Du : "+DateHelper.getStringDateTime(alert.getDatetimeFrom())+"\n";
                    period_txt += "Au : "+DateHelper.getStringDateTime(alert.getDatetimeTo())+"\n";
                    period_txt += "Vitesse d'alerte : "+device.getAlertSpeed()+"\n";
                    period_txt += "Vitesse maximum : "+((SpeedAlert)alert).getVitesseMax()+"\n";
                    break;
                case GASOIL_CONSUMED: 
                    period_txt += "Du : "+DateHelper.getStringDateTime(alert.getDatetimeFrom())+"\n";
                    period_txt += "Au : "+DateHelper.getStringDateTime(alert.getDatetimeTo())+"\n";
                    period_txt += "Distance : "+String.format("%.2f",((FuelAlert)alert).getDistance())+"\n";
                    period_txt += "Carburant consommé : "+((FuelAlert)alert).getCarburant()+"\n";
                    break;
                case GASOIL_ADDED:
                    period_txt += "Du : "+DateHelper.getStringDateTime(alert.getDatetimeFrom())+"\n";
                    period_txt += "Au : "+DateHelper.getStringDateTime(alert.getDatetimeTo())+"\n";
                    period_txt += "Distance : "+String.format("%.2f",((FuelAlert)alert).getDistance())+"\n";
                    period_txt += "Carburant ajouté : "+((FuelAlert)alert).getCarburant()+"\n";
                    break;
            }
            return period_txt;
        }
}