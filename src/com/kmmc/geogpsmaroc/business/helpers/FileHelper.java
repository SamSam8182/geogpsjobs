package com.kmmc.geogpsmaroc.business.helpers;
import java.io.*;
import java.util.ArrayList;

public class FileHelper {
	private String fileName;

	public FileHelper(String fileName) {
		super();
		this.fileName = fileName;
	}
	
	public ArrayList<String> readFromFile(){
		ArrayList<String> readlines = new ArrayList<String>();
		// This will reference one line at a time
        String line = null;
        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(fileName);
            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while((line = bufferedReader.readLine()) != null) {
            	readlines.add(line);
            }
            // Always close files.
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.err.println("Unable to open file '" + fileName + "'");                
        }
        catch(IOException ex) {
            System.err.println("Error reading file '" + fileName + "'");
        }
		return readlines;
	}
	
	public boolean writeInFile(String line){
		try {
            // Assume default encoding.
            FileWriter fileWriter =
                new FileWriter(this.fileName,true);

            // Always wrap FileWriter in BufferedWriter.
            BufferedWriter bufferedWriter =
                new BufferedWriter(fileWriter);
				
            bufferedWriter.write(line);
            bufferedWriter.newLine();

            bufferedWriter.close();
            return true;
        }
        catch(IOException ex) {
            return false;
        }
	}
	
}
