/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmmc.geogpsmaroc.business.helpers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import com.kmmc.geogpsmaroc.data.model.Planning;

/*
 * Cette class permet les manipulitions qui concerne l'objet 'planning'.
 */
public class PlanningHelper {
    
        public static List<Planning> getPlannings(List<Planning> plannings, int day){
            List<Planning> planning_filtred  = new ArrayList<Planning>();
            
            for(Planning p : plannings){
                if(p.getDayIdx() == day)
                    planning_filtred.add(p);
            }
            
            return planning_filtred;
        }

        public static HashMap<Date,List<Planning>> getInbetweenPlanning(List<Planning> plannings, Date date_begin,Date date_end){
            HashMap<Date,List<Planning>> date_plannings = new HashMap<Date,List<Planning>>();

            List<Date> datesList = DateHelper.getDatesBetween(date_begin,date_end);
            
            for(Date date : datesList){
                List<Planning> planning_filtred = getPlannings(plannings,DateHelper.getDateIndex(date));
                date_plannings.put(date,planning_filtred);
            }
            return date_plannings;
        }
        
        public static boolean planningContainsDate(List<Planning> plannings, Date date_time){
            for(Planning planning : plannings){
                if(DateHelper.getDateIndex(date_time) == planning.getDayIdx()){
                    if(DateHelper.compareTimes(planning.getTimeFrom(),date_time)<=0 && DateHelper.compareTimes(planning.getTimeTo(),date_time)>=0)
                       return true;
                }
            }
            return false;
        }
        
}
