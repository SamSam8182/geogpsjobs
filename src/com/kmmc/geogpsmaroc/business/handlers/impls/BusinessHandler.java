package com.kmmc.geogpsmaroc.business.handlers.impls;

import com.kmmc.geogpsmaroc.core.services.Locator;
import com.kmmc.geogpsmaroc.core.services.persistance.PersistanceService;
import com.kmmc.geogpsmaroc.framework.kernel.Kernel;
import com.kmmc.geogpsmaroc.framework.persistance.dao.DAO;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryManager;

public class BusinessHandler {
    protected DAO dao;
    protected QueryManager queryManager;

    public BusinessHandler() throws Exception {
        PersistanceService persistanceService = (PersistanceService) Kernel.getInstance().getImplementation(PersistanceService.class);
        queryManager = (QueryManager) Locator.lookup(QueryManager.class);
        dao = persistanceService.getDAO();
    }
}
