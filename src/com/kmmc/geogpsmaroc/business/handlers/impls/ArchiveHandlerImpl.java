package com.kmmc.geogpsmaroc.business.handlers.impls;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.kmmc.geogpsmaroc.business.handlers.ArchiveHandler;
import com.kmmc.geogpsmaroc.business.helpers.DateHelper;
import com.kmmc.geogpsmaroc.business.helpers.FileHelper;
import com.kmmc.geogpsmaroc.business.managers.DeviceManager;
import com.kmmc.geogpsmaroc.data.model.Device;
import com.kmmc.geogpsmaroc.data.model.Driver;
import com.kmmc.geogpsmaroc.data.model.Position;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryParameters;

public class ArchiveHandlerImpl extends BusinessHandler implements ArchiveHandler {

	private static final String pathArchiveSQL = "C:\\Users\\dell\\Desktop\\";
	private static final String prefixFolderAccount = "compte_";
	private static final String prefixFileName = "\\archive_";

	public ArchiveHandlerImpl() throws Exception {
		super();
	}
	
	public boolean archivePosition(Long id_compte,String rqt){
		Date actuelle = new Date();
		DateFormat dateFormat = new SimpleDateFormat("MM_yyyy");
		String dat = dateFormat.format(actuelle);
		File fileArchive = new File(pathArchiveSQL+prefixFolderAccount+id_compte+prefixFileName+dat+".sql");
		if(!fileArchive.exists()) {
			new File(pathArchiveSQL+prefixFolderAccount+id_compte).mkdirs();
		    try {
		    	fileArchive.createNewFile();
			} catch (IOException e) {
				return false;
			}
		}
		FileHelper f=new FileHelper(pathArchiveSQL+prefixFolderAccount+id_compte+prefixFileName+dat+".sql");
		f.writeInFile(rqt);
		return true;
	}
	
	public boolean deleteOldPositions(Date dateQuota,Position position, DeviceManager deviceManager){ 
		try{
			Date date=new Date();
			int resultCompare = 0;
			Date dateChanged = position.getDevice().getDateVehiculeAppareilChanged();
			Date dateArchive = DateHelper.getDateFromDateTime(position.getDevice().getLastArchive());
			Date currentDate = DateHelper.getDateFromDateTime(date);
			Integer quota=Integer.parseInt(""+position.getDevice().getAccount().getQuota());
			if(currentDate.compareTo(dateArchive ) == 1 ){
				if(dateChanged != null && (resultCompare = currentDate.compareTo(DateHelper.addDays(DateHelper.getDateFromDateTime(dateChanged),quota+1))) <= 0){				
					QueryParameters params = new QueryParameters();
					params.addParameter("id_vehicule", Long.valueOf(position.getDevice().getId()));
					params.addParameter("date_quota", dateQuota);
					queryManager.executeUpdateNamdeQuery("query.deleteOldPositionsByIDV", params);
					if(resultCompare == 0)
					{
						Device device = position.getDevice();
						device.setDateVehiculeAppareilChanged(null);
						deviceManager.save(device);
					}
					return true;
				}else{
					QueryParameters params = new QueryParameters();
					params.addParameter("id_vehiculeAppareil", Long.valueOf(position.getIdVehiculeAppareil()));
					params.addParameter("date_quota", dateQuota);
					queryManager.executeUpdateNamdeQuery("query.deleteOldPositionsByIDVA", params);
					return true;
				}
			}
		}catch(Exception e){
			return false;
		}
		return false;
	}


}
