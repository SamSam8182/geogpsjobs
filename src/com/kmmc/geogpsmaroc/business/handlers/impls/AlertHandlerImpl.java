package com.kmmc.geogpsmaroc.business.handlers.impls;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.ejb.criteria.ParameterContainer.Helper;

import com.kmmc.geogpsmaroc.business.handlers.AlertHandler;
import com.kmmc.geogpsmaroc.business.managers.AlertProgressManager;
import com.kmmc.geogpsmaroc.business.helpers.CurveHelper;
import com.kmmc.geogpsmaroc.business.helpers.DateHelper;
import com.kmmc.geogpsmaroc.business.helpers.GeometryHelper;
import com.kmmc.geogpsmaroc.business.helpers.MailHelper;
import com.kmmc.geogpsmaroc.business.helpers.POIHelper;
import com.kmmc.geogpsmaroc.business.helpers.PlanningHelper;
import com.kmmc.geogpsmaroc.business.managers.AlertManager;
import com.kmmc.geogpsmaroc.business.managers.DeviceManager;
import com.kmmc.geogpsmaroc.business.managers.PositionManager;
import com.kmmc.geogpsmaroc.core.services.Locator;
import com.kmmc.geogpsmaroc.core.services.log.Logger;
import com.kmmc.geogpsmaroc.core.services.mail.MailService;
import com.kmmc.geogpsmaroc.data.model.AgendaAlert;
import com.kmmc.geogpsmaroc.data.model.AlertProgress;
import com.kmmc.geogpsmaroc.data.model.BaseAlert;
import com.kmmc.geogpsmaroc.data.model.Device;
import com.kmmc.geogpsmaroc.data.model.FuelAlert;
import com.kmmc.geogpsmaroc.data.model.FuelRecord;
import com.kmmc.geogpsmaroc.data.model.LatLng;
import com.kmmc.geogpsmaroc.data.model.POIAlert;
import com.kmmc.geogpsmaroc.data.model.POIElement;
import com.kmmc.geogpsmaroc.data.model.POIState;
import com.kmmc.geogpsmaroc.data.model.Piece;
import com.kmmc.geogpsmaroc.data.model.Planning;
import com.kmmc.geogpsmaroc.data.model.Position;
import com.kmmc.geogpsmaroc.data.model.SpeedAlert;
import com.kmmc.geogpsmaroc.data.model.TypesAlert;
import com.kmmc.geogpsmaroc.data.model.TypesPOI;
import com.kmmc.geogpsmaroc.data.model.TypesPiece;
import com.kmmc.geogpsmaroc.data.model.User;
import com.kmmc.geogpsmaroc.data.model.WireAlert;
import com.kmmc.geogpsmaroc.data.model.ZoneAlert;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;

public class AlertHandlerImpl extends BusinessHandler implements AlertHandler {

	private static final int POWER_SUPPLY_LEVEL = 10500;
	private static final int POWER_SUPPLY_DELTA = 500; 
	
    public AlertHandlerImpl() throws Exception {
		super();
	}

    ArrayList<User> users;
    
	public void verifyAlert(Position aPosition, ArrayList<User> aUsers, AlertManager alertManager, PositionManager positionManager, AlertProgressManager alertProgressManager, DeviceManager deviceManager) {
		Device device = aPosition.getDevice();
		users = aUsers;
		
		MailService mailService = (MailService) Locator
				.lookup(MailService.class);
		
		if(device.getAlertStates() != null){
	        for(BaseAlert base_alert : device.getAlertStates()){
	            if(base_alert instanceof POIAlert){
	                try {
						verifyPoiAlert((POIAlert)base_alert,aPosition,device,alertManager,positionManager,mailService,alertProgressManager);
					} catch (DAOException e) {
						
					}
	            }if(base_alert instanceof ZoneAlert)
	                verifyZoneAlert((ZoneAlert)base_alert,aPosition,device,alertManager,mailService,alertProgressManager);
	            if(base_alert instanceof SpeedAlert)
	                verifySpeedAlert((SpeedAlert)base_alert,aPosition,device,alertManager,mailService,alertProgressManager);
	            if(base_alert instanceof AgendaAlert)
	                verifyAgendaAlert((AgendaAlert)base_alert,aPosition,device,alertManager,mailService,alertProgressManager);
	            if(base_alert instanceof FuelAlert)
	                verifyFuelAlert((FuelAlert)base_alert,device,alertManager,mailService,alertProgressManager);
	            if(base_alert instanceof WireAlert)
	            	verifyWireAlert((WireAlert)base_alert,aPosition,device,alertManager,deviceManager);
	        }
		}
	}

	public void verifyWireAlert(WireAlert aAlert, Position aPosition, Device aDevice, AlertManager alertManager, DeviceManager deviceManager) {
		if(aPosition.getInputTrigger()!=null && aPosition.getInputTrigger().equals(29) && aPosition.getPowerSupply()!=null){
			if(aPosition.getPowerSupply() >= (POWER_SUPPLY_LEVEL+POWER_SUPPLY_DELTA)){
				aAlert.setDatetimeFrom(aPosition.getTime());
				aAlert.setTypeAlerte("WIRE_ON");
				try {
					alertManager.save((BaseAlert)aAlert);
					aDevice.setIsPowered(true);
					deviceManager.save(aDevice);
                   	Logger.info("ALERT WIRE_ON");
				} catch (DAOException e) {
					e.printStackTrace();
				}
			}else if (aPosition.getPowerSupply() <= (POWER_SUPPLY_LEVEL-POWER_SUPPLY_DELTA)){
				aAlert.setDatetimeFrom(aPosition.getTime());
				aAlert.setTypeAlerte("WIRE_OFF");
				try {
					alertManager.save((BaseAlert)aAlert);
					aDevice.setIsPowered(false);
					deviceManager.save(aDevice);
                   	Logger.info("ALERT WIRE_OFF");
				} catch (DAOException e) {
					e.printStackTrace();
				}
			}
				
		}
	}

	public void verifyPoiAlert(POIAlert aAlert, Position aPosition, Device aDevice, AlertManager alertManager, PositionManager positionManager, MailService mailService, AlertProgressManager alertProgressManager) throws DAOException {
            int day_idx = DateHelper.getDateIndex(aPosition.getTime());
            ArrayList<POIElement> poiAtteinte = POIHelper.getPOIElement(aDevice, TypesPOI.ATTEINTE, day_idx);
            ArrayList<POIElement> poiNAtteinte = POIHelper.getPOIElement(aDevice, TypesPOI.NON_ATTEINTE, day_idx);
            for(POIElement pe : poiAtteinte){
                if(!(pe.isHaveToStop() && aPosition.getSpeed() != 0)){
                    if(GeometryHelper.contains(pe.getZone(),new LatLng(aPosition.getLatitude(),aPosition.getLongitude()))){
                        if(alertManager.getNumberOfAlertPoiByType(aAlert, aPosition, pe, aDevice,ATTEINTE)==0){
                        	Logger.info("ALERT POI DE TYPE 'ATTEINTE', NAME : "+pe.getName());
                        	aAlert.setDatetimeFrom(aPosition.getTime());
                        	aAlert.setName(pe.getName());
                        	aAlert.setRemarque(ATTEINTE);
                        	aAlert.setTraiter(false);
                        	sendMailAlerte(aDevice, aAlert, TypesAlert.POI, mailService);
                        	alertManager.save((BaseAlert)aAlert);
                        }
                        continue;
                    }
                }
                if(pe.isAlertOnExit()){
                    if(alertManager.getNumberOfAlertPoiByType(aAlert, aPosition, pe, aDevice,ATTEINTE)==1){
                    	if(alertManager.getNumberOfAlertPoiByType(aAlert, aPosition, pe, aDevice,QUITTE)==0){
	                    	Logger.info("ALERT POI DE TYPE 'QUITTE', NAME : "+pe.getName());
	                    	aAlert.setDatetimeFrom(aPosition.getTime());
	                    	aAlert.setName(pe.getName());
	                    	aAlert.setRemarque(QUITTE);
                        	aAlert.setTraiter(false);
	                    	sendMailAlerte(aDevice, aAlert, TypesAlert.POI, mailService);
	                    	alertManager.save((BaseAlert)aAlert);
                        }
                    }
                }
            }
            if(!poiNAtteinte.isEmpty()){
                Position previous_position = (aDevice.getPreviousPosition()!=null)?aDevice.getPreviousPosition():positionManager.lookup(aDevice.getLastActiveRapport());
                if((previous_position!=null && DateHelper.compareDates(previous_position.getTime(),aPosition.getTime())<0)){
                    for(POIElement pe : poiNAtteinte){
                        HashMap<Date,List<Planning>> date_planning = PlanningHelper.getInbetweenPlanning(pe.getPlannings(), previous_position.getTime(), aPosition.getTime());
                        Iterator hmIt = date_planning.entrySet().iterator();
                        while(hmIt.hasNext()){
                            Map.Entry pairsIn = (Map.Entry)hmIt.next();
                            Date date = (Date) pairsIn.getKey();
                            List<Planning> plan = (List<Planning>) pairsIn.getValue();

                            for(Planning p : plan){
                                POIState poi_state = aAlert.getPoiState(DateHelper.getStringDate(date),DateHelper.getDateHour(p.getTimeFrom()),DateHelper.getDateHour(p.getTimeTo()),pe.getName());
                            	Logger.info("ALERT POI DE TYPE 'ABSENCE1', NAME : "+pe.getName()+" Date : "+DateHelper.getDateHour(p.getTimeFrom())+"-"+DateHelper.getDateHour(p.getTimeTo()));
                            	aAlert.setDatetimeFrom(aPosition.getTime());
                            	aAlert.setName(pe.getName());
                            	aAlert.setRemarque(ABSENCE);
                            	aAlert.setTrancheHoraire(p.getTimeFrom()+"-"+p.getTimeTo());
                            	aAlert.setTraiter(false);
                            	sendMailAlerte(aDevice, aAlert, TypesAlert.POI, mailService);
                            	alertManager.save((BaseAlert)aAlert);
                            	if(poi_state != null)
                            		poi_state.setAlerted(true);
                            }

                            hmIt.remove();
                        }
                    }

                    if(aAlert.getPoiStates() != null){
                    	Logger.debug("Unviewed Alert with "+aAlert.getPoiStates().size()+" POI");
	                    for (POIState elt : aAlert.getPoiStates()) {
	                        if(!elt.isAttended() && !elt.isAlerted()){
	                        	Logger.info("ALERT POI DE TYPE 'ABSENCE2', NAME : "+elt.getName()+", From/to : "+elt.getFrom()+"-"+elt.getTo());
	                        	aAlert.setDatetimeFrom(aPosition.getTime());
	                        	aAlert.setName(elt.getName());
	                        	aAlert.setRemarque(ABSENCE);
	                        	aAlert.setTrancheHoraire(elt.getFrom()+"-"+elt.getTo());
                            	aAlert.setTraiter(false);
	                        	sendMailAlerte(aDevice, aAlert, TypesAlert.POI, mailService);
	                        	alertManager.save((BaseAlert)aAlert);
	                            elt.setAlerted(true);
	                        }
	                    }     
                    }
                    List<POIState> poi_states = new ArrayList<POIState>();
                    for(POIElement poi : poiNAtteinte){
                        List<Planning> plans = PlanningHelper.getPlannings(poi.getPlannings(), day_idx);
                        for(Planning plan : plans){
                            poi_states.add(new POIState(DateHelper.getDateHour(plan.getTimeFrom()),DateHelper.getDateHour(plan.getTimeTo()),poi.getName(),DateHelper.getStringDate(aPosition.getTime()),false,false));
                        }
                    }
                    aAlert.setPoiStates(poi_states);
                }
                if(aAlert.getPoiStates() == null || aAlert.getPoiStates().isEmpty()){
                    List<POIState> poi_states = new ArrayList<POIState>();
                    for(POIElement poi : poiNAtteinte){
                        List<Planning> plans = PlanningHelper.getPlannings(poi.getPlannings(), day_idx);
                        for(Planning plan : plans){
                            poi_states.add(new POIState(DateHelper.getDateHour(plan.getTimeFrom()),DateHelper.getDateHour(plan.getTimeTo()),poi.getName(),DateHelper.getStringDate(aPosition.getTime()),false,false));
                        }
                    }
                    aAlert.setPoiStates(poi_states);
                }
            }
            for(POIElement pe : poiNAtteinte){
                List<Planning> plans = PlanningHelper.getPlannings(pe.getPlannings(),day_idx);
                for(Planning p : plans){
                    if(DateHelper.compareHour(DateHelper.getStringTime(p.getTimeFrom()), DateHelper.getDateHour(aPosition.getTime()))<0){
                        POIState poi_state = aAlert.getPoiState(DateHelper.getStringDate(aPosition.getTime()),DateHelper.getDateHour(p.getTimeFrom()),DateHelper.getDateHour(p.getTimeTo()),pe.getName());
                        if(poi_state != null && !poi_state.isAttended()){
                            if(DateHelper.compareHour(DateHelper.getStringTime(p.getTimeTo()), DateHelper.getDateHour(aPosition.getTime()))<0){
                                if(!(pe.isHaveToStop() && aPosition.getSpeed()!=0)){
                                    if(GeometryHelper.contains(pe.getZone(),new LatLng(aPosition.getLatitude(),aPosition.getLongitude()))){
                                    	Logger.info("ALERT POI DE TYPE 'RETARD', NAME : "+pe.getName()+", From/to : "+p.getTimeFrom()+"-"+p.getTimeTo());
                                    	aAlert.setDatetimeFrom(aPosition.getTime());
                                    	aAlert.setName(pe.getName());
                                    	aAlert.setRemarque(RETARD);
                                    	aAlert.setTrancheHoraire(p.getTimeFrom()+"-"+p.getTimeTo());
                                    	aAlert.setIdHistorique(pe.getIdHistorique());
                                    	sendMailAlerte(aDevice, aAlert, TypesAlert.POI, mailService);
                                    	alertManager.updatePoi(aAlert);
                                    	aAlert.setPoiState(DateHelper.getStringDate(aPosition.getTime()),DateHelper.getDateHour(p.getTimeFrom()),DateHelper.getDateHour(p.getTimeTo()),pe.getName(),true,true);
                                    }else{
                                        if(!poi_state.isAlerted()){
                                        	Logger.info("ALERT POI DE TYPE 'ABSENCE3', NAME : "+pe.getName()+", From/to : "+p.getTimeFrom()+"-"+p.getTimeTo());
                                        	aAlert.setDatetimeFrom(aPosition.getTime());
                                        	aAlert.setName(pe.getName());
                                        	aAlert.setRemarque(ABSENCE);
                                        	aAlert.setTrancheHoraire(DateHelper.getDateHour(p.getTimeFrom())+"-"+DateHelper.getDateHour(p.getTimeTo()));
                                        	sendMailAlerte(aDevice, aAlert, TypesAlert.POI, mailService);
                                        	aAlert.setTraiter(false);
                                        	alertManager.save((BaseAlert)aAlert);
                                        	aAlert.setPoiState(DateHelper.getStringDate(aPosition.getTime()),DateHelper.getDateHour(p.getTimeFrom()),DateHelper.getDateHour(p.getTimeTo()),pe.getName(),false,true);
                                        }
                                    }
                                }
                            }else{
                                if((!(pe.isHaveToStop() && aPosition.getSpeed()!=0)) && 
                                        GeometryHelper.contains(pe.getZone(),new LatLng(aPosition.getLatitude(),aPosition.getLongitude()))){
                                	aAlert.setPoiState(DateHelper.getStringDate(aPosition.getTime()),DateHelper.getDateHour(p.getTimeFrom()),DateHelper.getDateHour(p.getTimeTo()),pe.getName(),true,false);
                                }
                            }
                        }else{
                            if(pe.isAlertOnExit()){
                                if(!GeometryHelper.contains(pe.getZone(),new LatLng(aPosition.getLatitude(),aPosition.getLongitude()))){
                                   if(alertManager.getNumberOfAlertPoiByType(aAlert, aPosition, pe, aDevice,QUITTE)==0){
                                   	Logger.info("ALERT POI DE TYPE 'QUITTE', NAME : "+pe.getName());
                                	aAlert.setDatetimeFrom(aPosition.getTime());
                                	aAlert.setName(pe.getName());
                                	aAlert.setRemarque(QUITTE);
                                	aAlert.setTraiter(false);
                                   	sendMailAlerte(aDevice, aAlert, TypesAlert.POI, mailService);
                                	alertManager.save((BaseAlert)aAlert);
                                   }
                                }
                            }
                        }
                    }
                }
            }
	}

	public void verifyZoneAlert(ZoneAlert aAlert, Position aPosition, Device aDevice, AlertManager alertManager, MailService mailService, AlertProgressManager alertProgressManager) {
            if(aDevice.getZones() != null){
                if(aAlert.isInprogressAlert()){
                    aAlert.setDatetimeTo(aPosition.getTime());
                    if(GeometryHelper.contains(aDevice.getZones(),
                            new LatLng(aPosition.getLatitude(),aPosition.getLongitude()))){
                    	aAlert.setInprogressAlert(false);
                    	sendMailAlerte(aDevice, aAlert, TypesAlert.ZONE, mailService);
                       	Logger.info("ALERT ZONE");
                       	try {
                        	aAlert.setTraiter(false);
							alertManager.save((BaseAlert)aAlert);
							alertProgressManager.deleteAlertProgress(aDevice.getId(),"ZONE");
						} catch (DAOException e) {
							e.printStackTrace();
						}
                    }
                }
                else if(!GeometryHelper.contains(aDevice.getZones(),
                            new LatLng(aPosition.getLatitude(),aPosition.getLongitude()))){
                    aAlert.setDatetimeFrom(aPosition.getTime());
                    aAlert.setInprogressAlert(true);
                    
                    AlertProgress alt = new AlertProgress();
                    alt.setDateFrom(aAlert.getDatetimeFrom());
                    alt.setIdVehicule(aDevice.getId());
                    alt.setType("ZONE");
                    try {
						alertProgressManager.save(alt);
					} catch (DAOException e) {
						e.printStackTrace();
					}
                }
            }
	}

	public void verifySpeedAlert(SpeedAlert aAlert, Position aPosition, Device aDevice, AlertManager alertManager, MailService mailService, AlertProgressManager alertProgressManager) {
            if(aDevice.getAlertSpeed() > 0)
            {
                if(aAlert.isInprogressAlert()){
                    aAlert.setDatetimeTo(aPosition.getTime());
                    //DEBUG HAVE TO BE REMOVED
                    Logger.info("SPEED:INPROG,"+aDevice.getId()+","+aAlert.getDatetimeTo()+","+aPosition.getSpeed());
                    //DEBUG HAVE TO BE REMOVED
                    if (aPosition.getSpeed() > aDevice.getAlertSpeed()){
                        if(aPosition.getSpeed()>aAlert.getVitesseMax())
                            aAlert.setVitesseMax(aPosition.getSpeed().intValue());
                    }
                    else{
                    	aAlert.setInprogressAlert(false);
                    	sendMailAlerte(aDevice, aAlert, TypesAlert.SPEED, mailService);
                       	Logger.info("ALERT SPEED");
    	               	try {
                        	aAlert.setTraiter(false);
    						alertManager.save((BaseAlert)aAlert);
							alertProgressManager.deleteAlertProgress(aDevice.getId(),"SPEED");
		                    //DEBUG HAVE TO BE REMOVED
		                    Logger.info("SPEED:INPROG FALSE PROGRESS DELETED,"+aDevice.getId());
		                    //DEBUG HAVE TO BE REMOVED
    					} catch (DAOException e) {
    						e.printStackTrace();
    					}
                    }
                }
                else if (aPosition.getSpeed() > aDevice.getAlertSpeed()){
                    aAlert.setDatetimeFrom(aPosition.getTime());
                    //DEBUG HAVE TO BE REMOVED
                    Logger.info("SPEED:BEGIN INPROG,"+aDevice.getId()+","+aAlert.getDatetimeFrom()+","+aPosition.getSpeed());
                    //DEBUG HAVE TO BE REMOVED
                    aAlert.setVitesseMax(aPosition.getSpeed().intValue());
                    aAlert.setInprogressAlert(true);
                    
                    AlertProgress alt = new AlertProgress();
                    alt.setDateFrom(aAlert.getDatetimeFrom());
                    alt.setIdVehicule(aDevice.getId());
                    alt.setSpeedMax((int)aAlert.getVitesseMax());
                    alt.setType("SPEED");
                    try {
						alertProgressManager.save(alt);
	                    //DEBUG HAVE TO BE REMOVED
	                    Logger.info("SPEED:INPROG TRUE PROGRESS CREATED,"+aDevice.getId());
	                    //DEBUG HAVE TO BE REMOVED
					} catch (DAOException e) {
						e.printStackTrace();
					}
                }
            }
	}

	public void verifyAgendaAlert(AgendaAlert aAlert, Position aPosition, Device aDevice, AlertManager alertManager, MailService mailService, AlertProgressManager alertProgressManager) {
            if(aDevice.getPlannings()!=null){
                if(aAlert.isInprogressAlert()){
                    aAlert.setDatetimeTo(aPosition.getTime());
                    aAlert.setCounterTo(aDevice.getCounter());
                    if(aAlert.getCarburant() != null)
                    	aAlert.setCarburant(aAlert.getCarburant()+aPosition.getCarburantConso().intValue());
                    if (aPosition.getSpeed() == 0 || PlanningHelper.planningContainsDate(aDevice.getPlannings(),aPosition.getTime())){
                    	aAlert.setInprogressAlert(false);
	                    aAlert.setDistance(aAlert.getCounterTo()-aAlert.getCounterFrom());
                    	sendMailAlerte(aDevice, aAlert, TypesAlert.TIME, mailService);
                       	Logger.info("ALERT AGENDA");
                       	try {
                        	aAlert.setTraiter(false);
							alertManager.save((BaseAlert)aAlert);
							alertProgressManager.deleteAlertProgress(aDevice.getId(),"TIME");
						} catch (DAOException e) {
							e.printStackTrace();
						}
                    }
                }
                else if (aPosition.getSpeed() > 0 && !PlanningHelper.planningContainsDate(aDevice.getPlannings(),aPosition.getTime())){
                    aAlert.setDatetimeFrom(aPosition.getTime());
                    aAlert.setCounterFrom(aDevice.getCounter());
                    aAlert.setInprogressAlert(true);
                    aAlert.setCarburant(aPosition.getCarburantConso().intValue());
                    
                    AlertProgress alt = new AlertProgress();
                    alt.setDateFrom(aAlert.getDatetimeFrom());
                    alt.setIdVehicule(aDevice.getId());
                    alt.setCounterFrom(aAlert.getCounterFrom());
                    alt.setType("TIME");
                    try {
						alertProgressManager.save(alt);
					} catch (DAOException e) {
						e.printStackTrace();
					}
                }
            }
	}

	public void verifyFuelAlert(FuelAlert aAlert, Device aDevice, AlertManager alertManager, MailService mailService, AlertProgressManager alertProgressManager) {
		if(aDevice.isAlertEnabled()){
            List<FuelRecord> fuel_records = aDevice.getFuelRecords();
            if(fuel_records.size() >= 30 && CurveHelper.deleteFakePic(fuel_records,aDevice.getFuelLevelTolerated())){
            	//DEBUG FUEL ALERT
            	String log_debug = "FUEL:{"+aDevice.getId();
            	for(FuelRecord fuel_record : fuel_records)log_debug += ","+fuel_record.getCounter()+"|"+fuel_record.getFuelLevel()+"|"+fuel_record.getDatetime();
            	//DEBUG FUEL ALERT
            	
	            CurveHelper.traitePoints(fuel_records, aDevice.getFuelLevelTolerated());
	            
	            int idx = 0, idx_temp = 0;
	            int first_slice_average, second_slice_average, first_slice_sum = 0, second_slice_sum = 0;
	            ArrayList<Double> differences = new ArrayList<Double>();
	            
	            for (int i = 0; i < fuel_records.size() - 1; i++)
	                differences.add(Math.abs(fuel_records.get(i).getFuelLevel() - fuel_records.get(i + 1).getFuelLevel()));
	            
	            while((idx<differences.size()) && (Math.round(differences.get(idx)) == 0))
	                idx++;
	            
	            for(int i = 0; i <= idx ;i++)
	                first_slice_sum += fuel_records.get(i).getFuelLevel();
	            
	            first_slice_average = first_slice_sum/(idx+1);
	            
	            while((idx<differences.size()) && (Math.round(differences.get(idx)) != 0))
	                idx++;
	            
	            idx_temp = idx;
	            
	            while((idx<differences.size()) && (Math.round(differences.get(idx)) == 0))
	                idx++;
	            
	            for(int i = idx_temp ; i <= idx ; i++)
	                second_slice_sum += fuel_records.get(i).getFuelLevel();
	            
	            second_slice_average = second_slice_sum/(idx-idx_temp+1);
	            
	            if(Math.abs(second_slice_average-first_slice_average)>aDevice.getFuelLevelTolerated()){
                	aAlert.setInprogressAlert(false);
                	aAlert.setDatetimeFrom(fuel_records.get(0).getDatetime());
                	aAlert.setDatetimeTo(fuel_records.get(fuel_records.size()-1).getDatetime());
                	aAlert.setDistance(Math.abs(fuel_records.get(0).getCounter() - fuel_records.get(fuel_records.size()-1).getCounter()));
                	aAlert.setCarburant(Math.abs(second_slice_average-first_slice_average));
                	//DEBUG FUEL ALERT
                	log_debug += ";"+first_slice_average+","+second_slice_average+"}";
                	//DEBUG FUEL ALERT
                	String msg = (first_slice_average < second_slice_average) ? "alerte plein" : "alerte vole" ;
	               	if(first_slice_average < second_slice_average)
	               		Logger.info("ALERT PLEIN");
	               	else
	               		Logger.info("ALERT VOLE");
	               	//TODO have to persiste fuelRecords
	               	if(msg == "alerte plein"){
                    	sendMailAlerte(aDevice, aAlert, TypesAlert.GASOIL_ADDED, mailService);
	               		aAlert.setTypeAlerte("GASOIL_ADDED");
	               	}else{
                    	sendMailAlerte(aDevice, aAlert, TypesAlert.GASOIL_CONSUMED, mailService);
	               	}
	               	try {
                    	aAlert.setTraiter(false);
						alertManager.save((BaseAlert)aAlert);
					} catch (DAOException e) {
						e.printStackTrace();
					}
	                Logger.info(log_debug);
	               	aDevice.setFuelRecords(new ArrayList<FuelRecord>());
	            }
            }
        }
	}
	
	private void sendMailAlerte(Device device, BaseAlert baseAlert, TypesAlert type, MailService mailService) {
		//Envoi d'alerte par mail pour tout les utilisateurs du compte, s'il souhaite reçevoir ce mail, et y on le droit
		for(User user : users){
			if(user.isNotify_alert() && user.isRead_alert())
				mailService.send(MailHelper.getSubjectAlert(type),user.getEmail(),MailHelper.getMsgAlert(device, type, baseAlert, user));
		}
	}

}
