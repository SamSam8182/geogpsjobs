package com.kmmc.geogpsmaroc.business.handlers.impls;

import com.kmmc.geogpsmaroc.business.handlers.ReinitializeHandler;
import com.kmmc.geogpsmaroc.business.managers.AlertManager;
import com.kmmc.geogpsmaroc.business.managers.DeviceManager;
import com.kmmc.geogpsmaroc.business.managers.PieceManager;
import com.kmmc.geogpsmaroc.business.managers.PositionManager;
import com.kmmc.geogpsmaroc.business.managers.UserManager;

public class ReinitializeHandlerImpl extends BusinessHandler implements ReinitializeHandler {

    private AlertManager alertManager;
    private PieceManager pieceManager;
    private DeviceManager deviceManager;
    private UserManager userManager;
    private PositionManager positionManager;

    public ReinitializeHandlerImpl() throws Exception {
		super();
	}
    
    /*
    public Object decode(Object aMsg) {
        if(aMsg instanceof Position){
            Position position = (Position)aMsg;
            Device device = position.getDevice();
            String changed_table = dataManager.getChangedTables(device);
            String[] sections = changed_table.split(",");

            for(String section : sections){
                if(section.equals("parametre")){
                    initialize_parametre(device);
                }else if(section.equals("piece")){
                    initialize_piece(device);
                }else if(section.equals("reglage")){
                    initialize_setting(device);
                }else if(section.equals("geozone")){
                    initialize_zone(device);
                }else if(section.equals("poi")){
                    initialize_poi(device);
                }else if(section.equals("temps")){
                    initialize_agenda(device);
                }else if(section.equals("role")){
                    initialize_role(device);
                }else if(section.equals("vehicule")){
                    initialize_vehicle(device);
                }else if(section.equals("utilisateur")){
                    initialize_user(device);
                }else if(section.equals("conducteur")){
                    initialize_driver(device);
                }else if(section.equals("vehicule_conducteur")){
                    initialize_vehicle_driver(device);
                }
            }

            dataManager.clearChangedTables(device);    

            position.setDevice(device);
            return position;
        }else
            return aMsg;
    }
    private void initialize_parametre(Device device) {
    	dataManager.getSetting(device);
    }

    private void initialize_piece(Device device) {
    	dataManager.getPiece(device);
    }

    private void initialize_setting(Device device) {
    	dataManager.getTimeZone(device);
    }

    private void initialize_zone(Device device) {
    	dataManager.getZone(device);
    }

    private void initialize_poi(Device device) {
    	dataManager.getPoi(device);
    }

    private void initialize_agenda(Device device) {
    	dataManager.getAgenda(device);
    }

    private void initialize_role(Device device) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void initialize_vehicle(Device device) {
    	dataManager.getDevice(device);
    }

    private void initialize_user(Device device) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void initialize_driver(Device device) {
    	dataManager.getDriver(device);
    }

    private void initialize_vehicle_driver(Device device) {
    	dataManager.getVehicleDriver(device);
    }*/
}
