package com.kmmc.geogpsmaroc.business.handlers;

import java.util.Date;

import com.kmmc.geogpsmaroc.business.managers.DeviceManager;
import com.kmmc.geogpsmaroc.data.model.Position;



/*
 * Cette class est un handler qui s'ajoute au ChannelPipeline, c'est une �tape par lequel passe la trame.
 * Cette class permette d'archiver la trame.
 */
public interface ArchiveHandler {
	public boolean archivePosition(Long id_compte,String rqt);
	public boolean deleteOldPositions(Date dateQuota,Position position, DeviceManager deviceManager);

}