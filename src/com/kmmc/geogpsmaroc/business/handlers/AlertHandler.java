package com.kmmc.geogpsmaroc.business.handlers;

import java.util.ArrayList;

import com.kmmc.geogpsmaroc.business.managers.AlertManager;
import com.kmmc.geogpsmaroc.business.managers.AlertProgressManager;
import com.kmmc.geogpsmaroc.business.managers.DeviceManager;
import com.kmmc.geogpsmaroc.business.managers.PositionManager;
import com.kmmc.geogpsmaroc.core.services.mail.MailService;
import com.kmmc.geogpsmaroc.data.model.AgendaAlert;
import com.kmmc.geogpsmaroc.data.model.Device;
import com.kmmc.geogpsmaroc.data.model.FuelAlert;
import com.kmmc.geogpsmaroc.data.model.POIAlert;
import com.kmmc.geogpsmaroc.data.model.Position;
import com.kmmc.geogpsmaroc.data.model.SpeedAlert;
import com.kmmc.geogpsmaroc.data.model.User;
import com.kmmc.geogpsmaroc.data.model.WireAlert;
import com.kmmc.geogpsmaroc.data.model.ZoneAlert;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;

/*
 * Cette class est un handler qui s'ajoute au ChannelPipeline, c'est une �tape par lequel passe la trame.
 * Cette class analyse et v�rifie les cas d'alerte pour chaque trame, et notifie par mail dans le cas �ch�ant.
 */
public interface AlertHandler {

    public final static String ATTEINTE = "atteint la zone"  ;
    public final static String QUITTE   = "quitte la zone"   ;
    public final static String ABSENCE  = "absent de la zone";
    public final static String RETARD   = "retard à la zone" ;

	public void verifyAlert(Position aPosition, ArrayList<User> aUsers, AlertManager alertManager, PositionManager positionManager, AlertProgressManager alertProgressManager, DeviceManager deviceManager);

	public void verifyWireAlert(WireAlert aAlert, Position aPosition, Device aDevice, AlertManager alertManager, DeviceManager deviceManager);
	
	public void verifyPoiAlert(POIAlert aAlert, Position aPosition, Device aDevice, AlertManager alertManager, PositionManager positionManager, MailService mailService, AlertProgressManager alertProgressManager) throws DAOException;

	public void verifyZoneAlert(ZoneAlert aAlert, Position aPosition, Device aDevice, AlertManager alertManager, MailService mailService, AlertProgressManager alertProgressManager);
	
	public void verifySpeedAlert(SpeedAlert aAlert, Position aPosition, Device aDevice, AlertManager alertManager, MailService mailService, AlertProgressManager alertProgressManager);

	public void verifyAgendaAlert(AgendaAlert aAlert, Position aPosition, Device aDevice, AlertManager alertManager, MailService mailService, AlertProgressManager alertProgressManager);

	public void verifyFuelAlert(FuelAlert aAlert, Device aDevice, AlertManager alertManager, MailService mailService, AlertProgressManager alertProgressManager);
}