package com.kmmc.geogpsmaroc.business.handlers;

import com.kmmc.geogpsmaroc.data.model.Position;

/*
 * Cette class est un handler qui s'ajoute au ChannelPipeline, c'est une �tape par lequel passe la trame.
 * Cette �tape permet de serializer l'objet 'Device' dans un fichier XML, apr�s �a modification.
 */
public interface SerializeHandler {

	public Object decode(Object aMsg);
}