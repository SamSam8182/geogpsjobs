package com.kmmc.geogpsmaroc.business.handlers;

import com.kmmc.geogpsmaroc.business.managers.*;
import com.kmmc.geogpsmaroc.data.model.Device;
import com.kmmc.geogpsmaroc.data.model.Position;

/*
 * Cette class est un handler qui s'ajoute au ChannelPipeline, c'est une �tape par lequel passe la trame.
 * Cette class v�rifie et r�initialise les objets qui doivent �tre r�initialis�;
 * les r�initialisations sont �ffectu� aux changements de donn�es dans la BDD par l'utilisateur final;
 * (ex : changement de geozone par l'utilisateur, entraine le changement de l'objet 'Shapes')
 */
public interface ReinitializeHandler {
	
}