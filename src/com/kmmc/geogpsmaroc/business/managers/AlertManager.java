package com.kmmc.geogpsmaroc.business.managers;

import java.util.List;

import com.kmmc.geogpsmaroc.data.model.BaseAlert;
import com.kmmc.geogpsmaroc.data.model.POIAlert;
import com.kmmc.geogpsmaroc.data.model.POIElement;
import com.kmmc.geogpsmaroc.data.model.Position;
import com.kmmc.geogpsmaroc.data.model.AgendaPeriode;
import com.kmmc.geogpsmaroc.data.model.Device;
import com.kmmc.geogpsmaroc.data.model.GeoZone;
import com.kmmc.geogpsmaroc.data.model.Poi;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;

public interface AlertManager {

	public Poi lookupPoi(Long id) throws DAOException;
	public GeoZone lookupGeoZone(Long id) throws DAOException;
	public AgendaPeriode lookupAgendaPeriode(Long id) throws DAOException;
	
	public Poi save(Poi poi) throws DAOException;
	public GeoZone save(GeoZone geoZone) throws DAOException;
	public AgendaPeriode save(AgendaPeriode agendaPeriode) throws DAOException;
	public BaseAlert save(BaseAlert baseAlert) throws DAOException;
	
	public void simpleSave(Poi poi) throws DAOException;
	public void simpleSave(GeoZone geoZone) throws DAOException;
	public void simpleSave(AgendaPeriode agendaPeriode) throws DAOException;
	
	public GeoZone getZoneByDevice(Device device) throws DAOException;
	public Poi getPoiByDevice(Device device) throws DAOException;
	public List<AgendaPeriode> getAgendaPeriodeByDevice(Device device) throws DAOException;
	public Long getNumberOfAlertPoiByType(POIAlert aAlert, Position aPosition, POIElement aPoi, Device aDevice, String remarque) throws DAOException;
	public void updatePoi(POIAlert aAlert) throws DAOException;
}
