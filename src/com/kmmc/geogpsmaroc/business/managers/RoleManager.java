package com.kmmc.geogpsmaroc.business.managers;

import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;

public interface RoleManager {
	public String getAccessPrivileges(String idModule, Long idRole) throws DAOException;
}
