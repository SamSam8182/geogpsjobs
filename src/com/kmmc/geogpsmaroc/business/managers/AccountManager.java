package com.kmmc.geogpsmaroc.business.managers;

import com.kmmc.geogpsmaroc.data.model.Account;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;

/**
 * Interface de gestion de la classe Formulaire.
 * 
 * @author FBenhayoun
 */
public interface AccountManager {
	public Account lookup(Long id) throws DAOException;
	public Account save(Account account) throws DAOException;
	public void simpleSave(Account account) throws DAOException;
	
}
