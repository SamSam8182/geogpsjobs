package com.kmmc.geogpsmaroc.business.managers.impls;

import java.util.List;

import com.kmmc.geogpsmaroc.business.helpers.DateHelper;
import com.kmmc.geogpsmaroc.business.managers.AlertManager;
import com.kmmc.geogpsmaroc.core.services.Locator;
import com.kmmc.geogpsmaroc.data.model.AgendaPeriode;
import com.kmmc.geogpsmaroc.data.model.AgendaTravail;
import com.kmmc.geogpsmaroc.data.model.BaseAlert;
import com.kmmc.geogpsmaroc.data.model.Device;
import com.kmmc.geogpsmaroc.data.model.POIAlert;
import com.kmmc.geogpsmaroc.data.model.POIElement;
import com.kmmc.geogpsmaroc.data.model.Poi;
import com.kmmc.geogpsmaroc.data.model.GeoZone;
import com.kmmc.geogpsmaroc.data.model.Position;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryManager;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryParameters;

public class AlertManagerImpl extends BusinessManager implements AlertManager {

	public AlertManagerImpl() throws Exception {
		super();
	}

	public Poi lookupPoi(Long id) throws DAOException {
		dao.initialize();

		Poi poi = new Poi();
		poi.setId(id);
		poi = (Poi) dao.lookup(poi);
		dao.cleanup();

		return poi;
	}

	public GeoZone lookupGeoZone(Long id) throws DAOException {
		dao.initialize();

		GeoZone geoZone = new GeoZone();
		geoZone.setId(id);
		geoZone = (GeoZone) dao.lookup(geoZone);
		dao.cleanup();

		return geoZone;
	}

	public AgendaPeriode lookupAgendaPeriode(Long id) throws DAOException {
		dao.initialize();

		AgendaPeriode agendaPeriode = new AgendaPeriode();
		agendaPeriode.setId(id);
		agendaPeriode = (AgendaPeriode) dao.lookup(agendaPeriode);
		dao.cleanup();

		return agendaPeriode;
	}

	public AgendaTravail lookupAgendaTravail(Long id) throws DAOException {
		dao.initialize();

		AgendaTravail agendaTravail = new AgendaTravail();
		agendaTravail.setId(id);
		agendaTravail = (AgendaTravail) dao.lookup(agendaTravail);
		dao.cleanup();

		return agendaTravail;
	}
	
	public Poi save(Poi poi) throws DAOException {
		dao.initialize();
		dao.beginTransaction();

		try {
			// Sauvegarde d'un User

			poi = (Poi) dao.save(poi);
			dao.commitTransaction();

		} catch (Exception e) {
			poi = null;
			dao.rollbackTransaction();
			throw new DAOException(e);
		} finally {
			dao.cleanup();
		}

		return poi;
	}

	public GeoZone save(GeoZone geoZone) throws DAOException {
		dao.initialize();
		dao.beginTransaction();

		try {
			// Sauvegarde d'un User

			geoZone = (GeoZone) dao.save(geoZone);
			dao.commitTransaction();

		} catch (Exception e) {
			geoZone = null;
			dao.rollbackTransaction();
			throw new DAOException(e);
		} finally {
			dao.cleanup();
		}

		return geoZone;
	}

	public BaseAlert save(BaseAlert baseAlert) throws DAOException {
		dao.initialize();
		dao.beginTransaction();

		try {
			// Sauvegarde d'un User

			baseAlert = (BaseAlert) dao.save(baseAlert);
			dao.commitTransaction();

		} catch (Exception e) {
			baseAlert = null;
			dao.rollbackTransaction();
			throw new DAOException(e);
		} finally {
			dao.cleanup();
		}

		return baseAlert;
	}

	public AgendaPeriode save(AgendaPeriode agendaPeriode) throws DAOException {
		dao.initialize();
		dao.beginTransaction();

		try {
			// Sauvegarde d'un User

			agendaPeriode = (AgendaPeriode) dao.save(agendaPeriode);
			dao.commitTransaction();

		} catch (Exception e) {
			agendaPeriode = null;
			dao.rollbackTransaction();
			throw new DAOException(e);
		} finally {
			dao.cleanup();
		}

		return agendaPeriode;
	}

	public void simpleSave(Poi poi) throws DAOException {
		dao.initialize();
		dao.beginTransaction();
		poi = (Poi) dao.save(poi);
		dao.commitTransaction();
		dao.cleanup();
	}

	public void simpleSave(GeoZone geoZone) throws DAOException {
		dao.initialize();
		dao.beginTransaction();
		geoZone = (GeoZone) dao.save(geoZone);
		dao.commitTransaction();
		dao.cleanup();
	}

	public void simpleSave(AgendaPeriode agendaPeriode) throws DAOException {
		dao.initialize();
		dao.beginTransaction();
		agendaPeriode = (AgendaPeriode) dao.save(agendaPeriode);
		dao.commitTransaction();
		dao.cleanup();
	}

	public GeoZone getZoneByDevice(Device device) throws DAOException{

		/*QueryManager queryManager = (QueryManager) Locator
				.lookup(QueryManager.class);*/
		QueryParameters params = new QueryParameters();

		params.addParameter("idDevice", device.getId());
		
		List listZone = queryManager.executeNamedQuery("query.getZoneByDevice", params);

		try{
			return (GeoZone) listZone.get(0);
		}catch(IndexOutOfBoundsException npe){
			return null;
		}

	}
	
	public Poi getPoiByDevice(Device device) throws DAOException{

		/*QueryManager queryManager = (QueryManager) Locator
				.lookup(QueryManager.class);*/
		QueryParameters params = new QueryParameters();

		params.addParameter("idDevice", device.getId());
		
		List listPoi = queryManager.executeNamedQuery("query.getPoiByDevice", params);

		try{
			return (Poi) listPoi.get(0);
		}catch(IndexOutOfBoundsException npe){
			return null;
		}
	}
	
	public List<AgendaPeriode> getAgendaPeriodeByDevice(Device device) throws DAOException{

		/*QueryManager queryManager = (QueryManager) Locator
				.lookup(QueryManager.class);*/
		QueryParameters params = new QueryParameters();
		
		params.addParameter("idDevice", device.getId());
		
		List<AgendaPeriode> listAgenda = queryManager.executeNamedQuery("query.getAgendaPeriodeByDevice", params);

		AgendaTravail agendaTravail = null;
		if(!listAgenda.isEmpty())
			agendaTravail = lookupAgendaTravail(listAgenda.get(0).getIdAgendaTravail());
		
		for(AgendaPeriode agendaP : listAgenda)
			agendaP.setIdHistoric(agendaTravail.getIdTempsAlerte());
		
		return listAgenda;
	}

	public Long getNumberOfAlertPoiByType(POIAlert aAlert, Position aPosition,
			POIElement aPoi, Device aDevice, String remarque)
			throws DAOException {
		/*QueryManager queryManager = (QueryManager) Locator
		.lookup(QueryManager.class);*/
		QueryParameters params = new QueryParameters();

		params.addParameter("idVehicule", aDevice.getId());
		params.addParameter("time", aPosition.getTime());
		params.addParameter("name", aPoi.getName());
		params.addParameter("remarque", remarque);
		
		List<Long> listAlerts = queryManager.executeNamedQuery("query.getNumberOfAlertPoiByType", params);

		try{
			return listAlerts.get(0);
		}catch(IndexOutOfBoundsException npe){
			return (long) 0;
		}
	}
	
	public void updatePoi(POIAlert aAlert)
			throws DAOException {
		QueryParameters params = new QueryParameters();

		params.addParameter("remarque",aAlert.getRemarque());
		params.addParameter("id_vehicule", aAlert.getIdVehicule());
		params.addParameter("id_chauffeur", aAlert.getIdChauffeur());
		params.addParameter("name", aAlert.getName());
		params.addParameter("date", aAlert.getDatetimeFrom());
		params.addParameter("horaire", aAlert.getTrancheHoraire());
		
		//TODO à refaire, ne doit pas être de cette manière
		if(aAlert.getIdHistorique() != null){
			params.addParameter("idPoiAlerte", "="+aAlert.getIdHistorique());
			queryManager.executeUpdateNamdeQuery("query.updatePoi", params);
		}else{
			queryManager.executeUpdateNamdeQuery("query.updatePoiNullIdHistoric", params);
		}
	}

}
