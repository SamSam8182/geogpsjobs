package com.kmmc.geogpsmaroc.business.managers.impls;

import java.util.HashMap;
import java.util.List;

import com.kmmc.geogpsmaroc.business.constants.DBConstants;
import com.kmmc.geogpsmaroc.business.managers.AlertProgressManager;
import com.kmmc.geogpsmaroc.data.model.AgendaAlert;
import com.kmmc.geogpsmaroc.data.model.AlertProgress;
import com.kmmc.geogpsmaroc.data.model.BaseAlert;
import com.kmmc.geogpsmaroc.data.model.SpeedAlert;
import com.kmmc.geogpsmaroc.data.model.TypesAlert;
import com.kmmc.geogpsmaroc.data.model.ZoneAlert;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryParameters;

public class AlertProgressManagerImpl extends BusinessManager implements
		AlertProgressManager {

	public AlertProgressManagerImpl() throws Exception {
		super();
	}


	public AlertProgress lookup(Long id) throws DAOException {
		dao.initialize();

		AlertProgress alert = new AlertProgress();
		alert.setId(id);
		alert = (AlertProgress) dao.lookup(alert);
		dao.cleanup();

		return alert;
	}

	public AlertProgress save(AlertProgress alert) throws DAOException {
		dao.initialize();
		dao.beginTransaction();

		try {
			// Sauvegarde d'un Driver

			alert = (AlertProgress) dao.save(alert);
			dao.commitTransaction();

		} catch (Exception e) {
			alert = null;
			dao.rollbackTransaction();
			throw new DAOException(e);
		} finally {
			dao.cleanup();
		}

		return alert;
	}

	public void simpleSave(AlertProgress alert) throws DAOException {
		dao.initialize();
		dao.beginTransaction();
		alert = (AlertProgress) dao.save(alert);
		dao.commitTransaction();
		dao.cleanup();
	}
	
	public HashMap<TypesAlert,BaseAlert> getAlertProgressByDevice(Long id) throws DAOException {
		QueryParameters params = new QueryParameters();

		params.addParameter("idVehicule", id);
		
		List<AlertProgress> listAlert = queryManager.executeNamedQuery("query.getAlertProgressByDevice", params);

		try{
			HashMap<TypesAlert,BaseAlert> listAlertDevice = new HashMap<TypesAlert,BaseAlert>();
			for(AlertProgress alertP : listAlert){
				BaseAlert alertObj = null;
				TypesAlert type = null;
				
				if(alertP.getType().equals(DBConstants.TYPE_ALERTE_SPEED)){
					alertObj = new SpeedAlert();
					type = TypesAlert.SPEED;
				}else if(alertP.getType().equals(DBConstants.TYPE_ALERTE_TIME)){
					alertObj = new AgendaAlert();
					type = TypesAlert.TIME;
				}else if(alertP.getType().equals(DBConstants.TYPE_ALERTE_ZONE)){
					alertObj = new ZoneAlert();
					type = TypesAlert.ZONE;
				}
	
				alertObj.setDatetimeFrom(alertP.getDateFrom());
				alertObj.setVitesseMax(alertP.getSpeedMax());
				alertObj.setTypeAlerte(alertP.getType());
				alertObj.setIdVehicule(id);
				alertObj.setInprogressAlert(true);
				alertObj.setFuelFrom(alertP.getFuelFrom());
				alertObj.setCounterFrom(alertP.getCounterFrom());
				
				listAlertDevice.put(type,alertObj);
			}
			return listAlertDevice;
		}catch(Exception e){
			return null;
		}
	}

	public void deleteAlertProgress(Long id, String type) throws DAOException {
		QueryParameters params = new QueryParameters();

		params.addParameter("idVehicule", id);
		params.addParameter("Type", type);
		
		queryManager.executeUpdateNamdeQuery("query.deleteAlertProgress", params);
	}
}
