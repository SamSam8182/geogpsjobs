package com.kmmc.geogpsmaroc.business.managers.impls;

import java.util.List;

import com.kmmc.geogpsmaroc.business.managers.DriverManager;
import com.kmmc.geogpsmaroc.core.services.Locator;
import com.kmmc.geogpsmaroc.data.model.Device;
import com.kmmc.geogpsmaroc.data.model.Driver;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryManager;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryParameters;

public class DriverManagerImpl extends BusinessManager implements DriverManager {

	public DriverManagerImpl() throws Exception {
		super();
	}

	public Driver lookup(Long id) throws DAOException {
		dao.initialize();

		Driver driver = new Driver();
		driver.setId(id);
		driver = (Driver) dao.lookup(driver);
		dao.cleanup();

		return driver;
	}

	public Driver save(Driver driver) throws DAOException {
		dao.initialize();
		dao.beginTransaction();

		try {
			// Sauvegarde d'un Driver

			driver = (Driver) dao.save(driver);
			dao.commitTransaction();

		} catch (Exception e) {
			driver = null;
			dao.rollbackTransaction();
			throw new DAOException(e);
		} finally {
			dao.cleanup();
		}

		return driver;
	}

	public void simpleSave(Driver driver) throws DAOException {
		dao.initialize();
		dao.beginTransaction();
		driver = (Driver) dao.save(driver);
		dao.commitTransaction();
		dao.cleanup();
	}

	public Driver getDriverByDevice(Device device) throws DAOException {

		/*QueryManager queryManager = (QueryManager) Locator
				.lookup(QueryManager.class);*/
		QueryParameters params = new QueryParameters();
		
		params.addParameter("idDevice", device.getId());
		
		List listDriver = queryManager.executeNamedQuery("query.getDriverByDevice", params);
		
		try{
			return (Driver) listDriver.get(0);
		}catch (NullPointerException npe){
			return null;
		}catch (IndexOutOfBoundsException ioobe){
			return null;
		}
	}

}
