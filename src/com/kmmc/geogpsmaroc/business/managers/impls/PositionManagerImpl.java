package com.kmmc.geogpsmaroc.business.managers.impls;

import java.util.List;

import com.kmmc.geogpsmaroc.business.managers.PositionManager;
import com.kmmc.geogpsmaroc.core.services.Locator;
import com.kmmc.geogpsmaroc.data.model.Position;
import com.kmmc.geogpsmaroc.data.model.User;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryManager;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryParameters;

public class PositionManagerImpl extends BusinessManager implements
		PositionManager {

	public PositionManagerImpl() throws Exception {
		super();
	}

	public Position lookup(Long id) throws DAOException {
		dao.initialize();

		Position position = new Position();
		position.setId(id);
		position = (Position) dao.lookup(position);
		dao.cleanup();

		return position;
	}

	public Position save(Position position) throws DAOException {
		dao.initialize();
		dao.beginTransaction();

		try {
			// Sauvegarde d'un User

			position = (Position) dao.save(position);
			dao.commitTransaction();

		} catch (Exception e) {
			position = null;
			dao.rollbackTransaction();
			throw new DAOException(e);
		} finally {
			dao.cleanup();
		}

		return position;
	}

	public void simpleSave(Position position) throws DAOException {
		dao.initialize();
		dao.beginTransaction();
		position = (Position) dao.save(position);
		dao.commitTransaction();
		dao.cleanup();
	}

	public List getPositionsUnTreated() throws DAOException {
		QueryParameters params = new QueryParameters();
		
		List listTrameUntreated = queryManager.executeNamedQuery(
				"query.getPositionsUnTreated", params);
		return listTrameUntreated;
	}
	
	public String insertToString(Position position) throws DAOException {
		return "INSERT INTO `rapport` values("
							+position.getId()+","
							+position.getIdVehiculeAppareil()+","
							+position.getIdChauffeur()+","
							+"'"+position.getDate()+"',"
							+"'"+position.getHeure()+"',"
							+position.getLatitude()+","
							+position.getLongitude()+","
							+position.getAltitude()+","
							+position.getSpeed() +","
							+position.getCompteur() +","
							+position.getCompteurBoitier()+ ","
							+position.getGsmLevel()+","
							+position.getCourse()+ ","
							+"'"+position.getSensDeviation() +"',"
							+"'"+position.getEtatMoteur()+"',"
							+position.getSatellite() +","
							+position.getTemperature() +", "
							+position.getFuel() +","
							+position.getCarburantConso() +","
							+position.getFuelCanbus() +","
							+position.getEngine() +","
							+position.getTreated() +","
							+position.getInputTrigger()+","
							+position.getPowerSupply()+")";
	}
}
