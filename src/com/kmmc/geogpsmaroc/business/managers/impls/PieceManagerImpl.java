package com.kmmc.geogpsmaroc.business.managers.impls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.kmmc.geogpsmaroc.business.managers.PieceManager;
import com.kmmc.geogpsmaroc.core.services.Locator;
import com.kmmc.geogpsmaroc.data.model.Device;
import com.kmmc.geogpsmaroc.data.model.Piece;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryManager;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryParameters;

public class PieceManagerImpl extends BusinessManager implements PieceManager {

	public PieceManagerImpl() throws Exception {
		super();
	}

	public Piece lookup(Long id) throws DAOException {
		dao.initialize();

		Piece piece = new Piece();
		piece.setId(id);
		piece = (Piece) dao.lookup(piece);
		dao.cleanup();

		return piece;
	}

	public Piece save(Piece piece) throws DAOException {
		dao.initialize();
		dao.beginTransaction();

		try {
			// Sauvegarde d'un Piece

			piece = (Piece) dao.save(piece);
			dao.commitTransaction();

		} catch (Exception e) {
			piece = null;
			dao.rollbackTransaction();
			throw new DAOException(e);
		} finally {
			dao.cleanup();
		}

		return piece;
	}

	public void simpleSave(Piece piece) throws DAOException {
		dao.initialize();
		dao.beginTransaction();
		piece = (Piece) dao.save(piece);
		dao.commitTransaction();
		dao.cleanup();
	}

	public Piece getPieceByTypeAndDevice(Device device, String typePiece) throws DAOException {

		QueryManager queryManager = (QueryManager) Locator
				.lookup(QueryManager.class);
		QueryParameters params = new QueryParameters();

		params.addParameter("idDevice", device.getId());
		params.addParameter("typePiece", typePiece);
		
		List listPiece = queryManager.executeNamedQuery("query.getPieceByTypeAndDevice", params);
		
		try {
			return (Piece) listPiece.get(0);	
		} catch( IndexOutOfBoundsException npe ){
			return null;
		}

	}
	
	public List<Piece> getPieceUnalerted() throws DAOException {

		QueryManager queryManager = (QueryManager) Locator
				.lookup(QueryManager.class);
		QueryParameters params = new QueryParameters();

		List<Piece> listPiece = queryManager.executeNamedQuery("query.getPieceUnalerted", params);
		List<Piece> finalListPiece = new ArrayList<Piece>();
		
		HashMap<String, ArrayList<Piece>> pair = new HashMap<String,ArrayList<Piece>>();
		
		for(Piece piece : listPiece){
			String idV = piece.getIdVehicule()+""+piece.getTypePiece();
			if(pair.get(idV) != null && !pair.get(idV).isEmpty()){
				pair.get(idV).add(piece);
			}else{
				ArrayList<Piece> lpiece = new ArrayList<Piece>();
				lpiece.add(piece);
				pair.put(idV,lpiece);
			}
		}
		
		for(Entry<String, ArrayList<Piece>> entry : pair.entrySet()){
			if(entry.getValue() != null && entry.getValue().size()>1){
				ArrayList<Piece> listDuplicatePiece = entry.getValue(); 
				Piece savedPiece = listDuplicatePiece.get(0);
				for(Piece duplicatePiece : listDuplicatePiece) {
					if(duplicatePiece.getId() > savedPiece.getId())
						savedPiece = duplicatePiece;
				}
				
				finalListPiece.add(savedPiece);
			}else{
				if(entry.getValue() != null)
					finalListPiece.add(entry.getValue().get(0));
			}
		}
		
		
		return (List<Piece>) finalListPiece;	
	}
}
