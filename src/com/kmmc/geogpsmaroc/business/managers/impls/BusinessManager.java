package com.kmmc.geogpsmaroc.business.managers.impls;

import com.kmmc.geogpsmaroc.core.services.Locator;
import com.kmmc.geogpsmaroc.core.services.persistance.PersistanceService;
import com.kmmc.geogpsmaroc.framework.kernel.Kernel;
import com.kmmc.geogpsmaroc.framework.persistance.dao.DAO;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryManager;


/**
 * Classe Metier pour la gestion des objets persistents.
 * @author EBE
 *
 */
public class BusinessManager {
    protected DAO dao;
    protected QueryManager queryManager;

    public BusinessManager() throws Exception {
        PersistanceService persistanceService = (PersistanceService) Kernel.getInstance().getImplementation(PersistanceService.class);
        queryManager = (QueryManager) Locator.lookup(QueryManager.class);
        dao = persistanceService.getDAO();
    }
}
