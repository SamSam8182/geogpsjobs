package com.kmmc.geogpsmaroc.business.managers.impls;

import java.util.List;

import com.kmmc.geogpsmaroc.business.managers.DeviceManager;
import com.kmmc.geogpsmaroc.core.services.Locator;
import com.kmmc.geogpsmaroc.data.model.Device;
import com.kmmc.geogpsmaroc.data.model.GeoZone;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryManager;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryParameters;

public class DeviceManagerImpl  extends BusinessManager implements DeviceManager {

	public DeviceManagerImpl() throws Exception {
		super();
	}

	public Device lookup(Long id) throws DAOException {
		dao.initialize();

		Device device = new Device();
		device.setId(id);
		device = (Device) dao.lookup(device);
		dao.cleanup();

		return device;
	}

	public Device save(Device device) throws DAOException {
		dao.initialize();
		dao.beginTransaction();

		try {
			// Sauvegarde d'un Device

			device = (Device) dao.save(device);
			dao.commitTransaction();

		} catch (Exception e) {
			device = null;
			dao.rollbackTransaction();
			throw new DAOException(e);
		} finally {
			dao.cleanup();
		}

		return device;
	}

	public void simpleSave(Device device) throws DAOException {
		dao.initialize();
		dao.beginTransaction();
		device = (Device) dao.save(device);
		dao.commitTransaction();
		dao.cleanup();
	}
	
	public Device getDeviceByIdDeviceAppareil(Long id) throws DAOException {
		QueryParameters params = new QueryParameters();

		params.addParameter("idDeviceAppareil", id);
		
		List<Device> listDevice = queryManager.executeNamedQuery("query.getDeviceByIdDeviceAppareil", params);

		try{
			return (Device) listDevice.get(0);
		}catch(IndexOutOfBoundsException npe){
			return null;
		}
	}

}
