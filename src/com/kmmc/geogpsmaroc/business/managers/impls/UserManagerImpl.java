package com.kmmc.geogpsmaroc.business.managers.impls;

import java.util.ArrayList;
import java.util.List;

import com.kmmc.geogpsmaroc.business.helpers.JSONHelper;
import com.kmmc.geogpsmaroc.business.jsonmodel.SettingJSON;
import com.kmmc.geogpsmaroc.business.managers.RoleManager;
import com.kmmc.geogpsmaroc.business.managers.UserManager;
import com.kmmc.geogpsmaroc.core.services.Locator;
import com.kmmc.geogpsmaroc.data.model.User;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryParameters;

/**
 * Impl�mentation de gestion des formulaires
 * 
 * @author ETTOUHAMI
 */
public class UserManagerImpl extends BusinessManager implements UserManager {
	public UserManagerImpl() throws Exception {
		super();
	}

	public User save(User user) throws DAOException {
		dao.initialize();
		dao.beginTransaction();

		try {
			// Sauvegarde d'un User

			user = (User) dao.save(user);
			dao.commitTransaction();

		} catch (Exception e) {
			user = null;
			dao.rollbackTransaction();
			throw new DAOException(e);
		} finally {
			dao.cleanup();
		}

		return user;
	}


	public void simpleSave(User user) throws DAOException {
		dao.initialize();
		dao.beginTransaction();
		user = (User) dao.save(user);
		dao.commitTransaction();
		dao.cleanup();
	}

	public User lookup(Long id) throws DAOException {
		dao.initialize();

		User user = new User();
		user.setId(id);
		user = (User) dao.lookup(user);
		dao.cleanup();

		return user;
	}

	public ArrayList<User> getUserByIdAccount(Long id) throws DAOException {
		RoleManager roleManager = (RoleManager) Locator
				.lookup(RoleManager.class);
		
		QueryParameters params = new QueryParameters();

		params.addParameter("idCompte", id);
		
		List<User> listUser = queryManager.executeNamedQuery("query.getUserByIdAccount", params);
		
		for(User user : listUser){
			SettingJSON settingJson = (SettingJSON) JSONHelper.getObject(SettingJSON.class, user.getCommentaire());
            settingJson.setSettings(user);

            if(user.getIdRole() == null){
            	user.setRead_alert(true);
            	user.setRead_piece(true);
            }else{
            	String droitPieceUnparsed = roleManager.getAccessPrivileges("Piece", user.getIdRole());
    			user.setRead_piece((droitPieceUnparsed.charAt(1)=='1')?true:false);

    			String droitAlertUnparsed = roleManager.getAccessPrivileges("Alerte", user.getIdRole());
    			user.setRead_alert((droitAlertUnparsed.charAt(0)=='1')?true:false);
    		}	
		}

		return (ArrayList<User>) listUser;
	}

}
