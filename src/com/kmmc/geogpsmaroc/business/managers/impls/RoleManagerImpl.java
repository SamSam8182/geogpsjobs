package com.kmmc.geogpsmaroc.business.managers.impls;

import java.util.List;

import com.kmmc.geogpsmaroc.business.managers.RoleManager;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;
import com.kmmc.geogpsmaroc.framework.persistance.query.QueryParameters;

public class RoleManagerImpl extends BusinessManager implements RoleManager {

	public RoleManagerImpl() throws Exception {
		super();
	}

	public String getAccessPrivileges(String idModule, Long idRole) throws DAOException {
		QueryParameters params = new QueryParameters();

		params.addParameter("idModule", idModule);
		params.addParameter("idRole", idRole);
		
		List<String> droit = queryManager.executeNamedQuery("query.getAccessPrivileges", params);

		if(droit.size()>0)
			return droit.get(0);
		else
			return null;
	}
}
