package com.kmmc.geogpsmaroc.business.managers.impls;

import com.kmmc.geogpsmaroc.business.managers.AccountManager;
import com.kmmc.geogpsmaroc.data.model.Account;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;

/**
 * Impl�mentation de gestion des formulaires
 * 
 * @author ETTOUHAMI
 */
public class AccountManagerImpl extends BusinessManager implements AccountManager {
	public AccountManagerImpl() throws Exception {
		super();
	}

	public Account save(Account account) throws DAOException {
		dao.initialize();
		dao.beginTransaction();

		try {
			// Sauvegarde d'un User

			account = (Account) dao.save(account);
			dao.commitTransaction();

		} catch (Exception e) {
			account = null;
			dao.rollbackTransaction();
			throw new DAOException(e);
		} finally {
			dao.cleanup();
		}

		return account;
	}


	public void simpleSave(Account account) throws DAOException {
		dao.initialize();
		dao.beginTransaction();
		account = (Account) dao.save(account);
		dao.commitTransaction();
		dao.cleanup();
	}

	public Account lookup(Long id) throws DAOException {
		dao.initialize();

		Account account = new Account();
		account.setId(id);
		account = (Account) dao.lookup(account);
		dao.cleanup();

		return account;
	}
}
