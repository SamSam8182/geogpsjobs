package com.kmmc.geogpsmaroc.business.managers;

import java.util.HashMap;

import com.kmmc.geogpsmaroc.data.model.AlertProgress;
import com.kmmc.geogpsmaroc.data.model.BaseAlert;
import com.kmmc.geogpsmaroc.data.model.TypesAlert;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;

public interface AlertProgressManager {
	public AlertProgress lookup(Long id) throws DAOException;
	public AlertProgress save(AlertProgress alert) throws DAOException;
	public void simpleSave(AlertProgress alert) throws DAOException;
	
	public HashMap<TypesAlert,BaseAlert> getAlertProgressByDevice(Long id) throws DAOException;
	public void deleteAlertProgress(Long id, String type) throws DAOException;
}
