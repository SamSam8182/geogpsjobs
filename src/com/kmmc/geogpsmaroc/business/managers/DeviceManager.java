package com.kmmc.geogpsmaroc.business.managers;

import java.util.List;

import com.kmmc.geogpsmaroc.data.model.Device;
import com.kmmc.geogpsmaroc.data.model.User;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;

public interface DeviceManager {

	public Device lookup(Long id) throws DAOException;
	public Device save(Device device) throws DAOException;
	public void simpleSave(Device device) throws DAOException;
	
	public Device getDeviceByIdDeviceAppareil(Long id) throws DAOException;
}
