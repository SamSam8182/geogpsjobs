package com.kmmc.geogpsmaroc.business.managers;

import com.kmmc.geogpsmaroc.data.model.Device;
import com.kmmc.geogpsmaroc.data.model.Driver;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;

public interface DriverManager {

	public Driver lookup(Long id) throws DAOException;
	public Driver save(Driver driver) throws DAOException;
	public void simpleSave(Driver driver) throws DAOException;
	public Driver getDriverByDevice(Device device) throws DAOException;
}
