package com.kmmc.geogpsmaroc.business.managers;

import java.util.List;

import com.kmmc.geogpsmaroc.data.model.Device;
import com.kmmc.geogpsmaroc.data.model.Piece;
import com.kmmc.geogpsmaroc.data.model.User;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;

public interface PieceManager {

	public Piece lookup(Long id) throws DAOException;
	public Piece save(Piece piece) throws DAOException;
	public void simpleSave(Piece piece) throws DAOException;
	public Piece getPieceByTypeAndDevice(Device device, String typePiece) throws DAOException;
	public List<Piece> getPieceUnalerted() throws DAOException;
}
