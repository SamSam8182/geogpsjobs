package com.kmmc.geogpsmaroc.business.managers;

import java.util.List;

import com.kmmc.geogpsmaroc.data.model.Position;
import com.kmmc.geogpsmaroc.data.model.User;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;

public interface PositionManager {

	public Position lookup(Long id) throws DAOException;
	public Position save(Position position) throws DAOException;
	public void simpleSave(Position position) throws DAOException;
	public List getPositionsUnTreated() throws DAOException;
	public String insertToString(Position position) throws DAOException;
}
