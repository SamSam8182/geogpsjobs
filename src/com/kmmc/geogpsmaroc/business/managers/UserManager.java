package com.kmmc.geogpsmaroc.business.managers;

import java.util.ArrayList;
import java.util.List;

import com.kmmc.geogpsmaroc.data.model.User;
import com.kmmc.geogpsmaroc.framework.persistance.dao.exception.DAOException;

/**
 * Interface de gestion de la classe Formulaire.
 * 
 * @author FBenhayoun
 */
public interface UserManager {
	

	public User lookup(Long id) throws DAOException;
	public User save(User user) throws DAOException;
	public void simpleSave(User user) throws DAOException;

	public ArrayList<User> getUserByIdAccount(Long id) throws DAOException;
}
