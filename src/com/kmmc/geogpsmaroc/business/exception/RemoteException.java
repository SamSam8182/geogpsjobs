package com.kmmc.geogpsmaroc.business.exception;

/**
 * Classe d'exception des services distants
 * @author EBE
 *
 */
public class RemoteException extends Exception {
    
    public RemoteException() {
        super();
    }

    public RemoteException(String message) {
        super(" Probl�me dans l'appel des services Remote TVA : "+message);
    }
}
