package com.kmmc.geogpsmaroc.common.exception;

public class SimplException extends Exception {
    /**
     *
     * @param innerException
     */
    public SimplException() {
    }

    /**
     *
     * @param innerException
     * @param code
     * @param severity
     * @param arguments
     */
    public SimplException(String message) {
        super(message);
    }
}
