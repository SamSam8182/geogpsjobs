package com.kmmc.geogpsmaroc.servlet.startup;

import javax.servlet.ServletException;

import com.kmmc.geogpsmaroc.core.startup.StartupManager;
import com.kmmc.geogpsmaroc.framework.kernel.Kernel;


/**
 * Servlet principal de d�marrage de l'application.
 * @author h.benmessaoud On 1 d�c. 07
 */
public class StartupServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
    public void init() throws ServletException {
        try {
            //D�marrage du noyau
            Kernel.getInstance().init();
            StartupManager.lunch();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e);
        }
    }
}
